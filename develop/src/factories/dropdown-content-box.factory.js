import ContentBox from '../components/filter-manager/dropdown/content-box';
export default dropdown => {
  let contentBox,
    dropdownConfig = dropdown.config;

  contentBox = dropdown.attachChild(ContentBox, 'contentBox');
  contentBox.configure({
    contentVisibility: dropdownConfig.contentVisibility,
    items: dropdownConfig.dropDownInfo.newItems,
    extContentStyle: (dropdownConfig.dropDownStyle && dropdownConfig.dropDownStyle.item) || {}
  });
};
