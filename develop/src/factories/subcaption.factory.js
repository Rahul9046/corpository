import { componentFactory } from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/lib';
import SubCaption from '../components/sub-caption';

export default chart => {
  let chartAttrs = chart.getFromEnv('chart-attrib'),
    companyDetails = chartAttrs.companydetails || {};
  if (companyDetails.cin || companyDetails.status || companyDetails.state || companyDetails.city) {
    componentFactory(
      chart,
      SubCaption,
      'subCaption',
      1,
      [{
        subCaptionStyle: {},
        cin: companyDetails.cin,
        status: companyDetails.status,
        statusFlag: companyDetails.statusflag,
        state: companyDetails.state,
        city: companyDetails.city
      }]
    );
  }
};
