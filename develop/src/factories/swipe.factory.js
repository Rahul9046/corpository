let dragStartHandler = function (canvas) {
    return function () {
      startX = 0;
      canvas.getChildren('mouseTracker')[0].deactivate();
    };
  },
  dragMoveHandler = function (canvas) {
    return function (e) {
      let chart = canvas.getLinkedParent(),
        scale = chart.config.focusScaleX,
        // The total domain is wrt to the time navigator scale
        totalDomain = chart.config.contextScaleX.getDomain(),
        // Getting the range in pixel wrt to the scale attached to the drawing canvas
        [currStartRange, currEndRange] = scale.getRange(),
        data = e.originalEvent.data,
        offset = data[0] - startX,
        startDomainValue,
        endDomainValue;
      // Changing the Range wrt to the number of pixel moved in drag move
      // Pan is possbile only when current Start date in canvas is more than the start date of time navigator
      // Pan is possbile only when current End date in canvas is less than the end date of time navigator
      if (offset < 0 ? scale.getDomainValue(currEndRange) < totalDomain[1] :
        scale.getDomainValue(currStartRange) > totalDomain[0]) {
        currEndRange -= offset;
        currStartRange -= offset;
      }
      startDomainValue = scale.getDomainValue(currStartRange);
      endDomainValue = scale.getDomainValue(currEndRange);
      // Re-setting the domain
      chart.setFocusLimit([startDomainValue, endDomainValue]);
      startX = data[0];
    };
  },
  dragEndHandler = function (canvas) {
    return function () {
      canvas.getChildren('mouseTracker')[0].activate();
    };
  },
  startX;

export default function (canvas) {
  let config = canvas.config;
  // Adding wheel event only once for the main canvas
  if (config.enableInteraction && !config.swipeEventAdded) {
    canvas.addEventListener('fc-dragstart', dragStartHandler(canvas));
    canvas.addEventListener('fc-dragmove', dragMoveHandler(canvas));
    canvas.addEventListener('fc-dragend', dragEndHandler(canvas));
    config.swipeEventAdded = true;
  }
}
