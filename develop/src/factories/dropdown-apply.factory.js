import Apply from '../components/filter-manager/dropdown/apply';
export default dropdown => {
  let apply;

  apply = dropdown.attachChild(Apply, 'apply');
  apply.configure({
    display: dropdown.config.contentVisibility
  });
};
