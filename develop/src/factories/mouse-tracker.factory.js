import MouseTracker2 from '../components/mouse-tracker2';
export default function (canvas) {
  let mouseTracker;
  if (canvas.config.enableMouseTracking) {
    mouseTracker = canvas.attachChild(MouseTracker2, 'mouseTracker');
    canvas.addToEnv('mouseTracker', mouseTracker);
    // if mouse tracker events are already added then do not add the events
    if (!mouseTracker.config.mouseTrackerEventAdded) {
      mouseTracker.addEvents();
      mouseTracker.config.mouseTrackerEventAdded = true;
    }
  }
}
