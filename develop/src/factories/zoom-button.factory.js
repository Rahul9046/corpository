import { componentFactory } from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/lib';
import ZoomButton from '../components/zoom-button';

const DURATION_DAY = 86400000;

export default chart => {
  let zoominButton,
    zoomoutButton,
    focusLimit = chart.config.focusScaleX.getDomain(),
    contextLimit = chart.config.contextScaleX.getDomain();
  componentFactory(
    chart,
    ZoomButton,
    'zoominButton',
    1,
    [{
      name: 'Zoom In',
      type: 'zoomIn',
      enabled: ((focusLimit[1] - focusLimit[0]) >= 5.5 * DURATION_DAY)
    }]
  );
  zoominButton = chart.getChildren('zoominButton')[0];
  zoominButton.addExtEventListener('focusLimitChanged', () => {
    focusLimit = chart.config.focusScaleX.getDomain();
    zoominButton.setData({
      enabled: ((focusLimit[1] - focusLimit[0]) >= 5.5 * DURATION_DAY)
    }, true);
  }, chart);
  componentFactory(
    chart,
    ZoomButton,
    'zoomoutButton',
    1,
    [{
      name: 'Zoom Out',
      type: 'zoomOut',
      enabled: ((focusLimit[1] - focusLimit[0]) !== (contextLimit[1] - contextLimit[0]))
    }]
  );
  zoomoutButton = chart.getChildren('zoomoutButton')[0];
  zoomoutButton.addExtEventListener('focusLimitChanged', () => {
    focusLimit = chart.config.focusScaleX.getDomain();
    contextLimit = chart.config.contextScaleX.getDomain();
    zoomoutButton.setData({
      enabled: ((focusLimit[1] - focusLimit[0]) !== (contextLimit[1] - contextLimit[0]))
    }, true);
  }, chart);
};
