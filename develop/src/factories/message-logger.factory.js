import MessageLogger from '../components/message-logger';
import {
  componentFactory
} from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/lib';

export default chart => {
  let chartWidth = chart.getFromEnv('chartWidth'),
    chartHeight = chart.getFromEnv('chartHeight');
  componentFactory(
    chart,
    MessageLogger,
    'messageLogger',
    1, [{
      x: 0,
      y: 0,
      width: chartWidth,
      height: chartHeight
    }]
  );
};
