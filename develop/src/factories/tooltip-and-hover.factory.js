import MouseAction from '../components/tooltip-and-hover2';
export default function (canvas) {
  if (canvas.config.enableMouseTracking) {
    canvas.attachChild(MouseAction, 'tooltipHover').configure();
  }
};
