import Header from '../components/filter-manager/dropdown/header';
export default dropdown => {
  let dropdownConfig = dropdown.config,
    header;

  header = dropdown.attachChild(Header, 'header');
  header.configure({
    headerName: dropdownConfig.dropDownInfo.header,
    extHeaderStyle: (dropdownConfig.dropDownStyle && dropdownConfig.dropDownStyle.header) || {}
  });
};
