import { componentFactory } from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/lib';
import Caption from '../components/caption';

export default chart => {
  let chartAttrs = chart.getFromEnv('chart-attrib'),
    companyDetails = chartAttrs.companydetails || {};
  if (companyDetails.name) {
    componentFactory(
      chart,
      Caption,
      'caption',
      1,
      [{
        captionStyle: {},
        companyName: companyDetails.name
      }]
    );
  }
};
