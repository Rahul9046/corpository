import {
  componentFactory,
  pluck
} from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/lib';
import FilterManager from '../components/filter-manager';

const DROPDOWN_GAP = 10;
export default chart => {
  let chartConfig = chart.config,
    dataSource = chart.getFromEnv('dataSource'),
    filterObj = dataSource.filter || {},
    filterNameMap = filterObj.filternamemap || {},
    dropDownStyle = filterObj.style || {},
    uniqueEventCategories = chartConfig.uniqueEventCategories,
    uniqueEventDescription = chartConfig.uniqueEventDescription,
    uniqueEventFlags = chartConfig.uniqueEventFlags,
    measureDatas = chartConfig.measureDatas,
    chartWidth = chart.getFromEnv('chartWidth'),
    chartAttrs = chart.getFromEnv('chart-attrib'),
    chartStyle = chartAttrs.style || {},
    resetButtonStyle = chartStyle.resetbutton || {},
    buttonStyle = resetButtonStyle.button || {},
    textStyle = resetButtonStyle.text || {},
    dropDownWidth,
    filterHeaderNames = {
      category: pluck(filterNameMap.category, 'Category of events'),
      description: pluck(filterNameMap.description, 'Description of events'),
      flag: pluck(filterNameMap.flag, 'Flags'),
      measure: pluck(filterNameMap.measure, 'Measures')
    };

  // accomodate whole canvas width
  dropDownWidth = Math.min((chartWidth * 0.8 - 3 * DROPDOWN_GAP) / 4, 190);
  componentFactory(
    chart,
    FilterManager,
    'filterManager',
    1, [{
      dropDownInfo: [{
        header: filterHeaderNames.category,
        maxSelect: uniqueEventCategories.length,
        type: 'eventCategories',
        items: uniqueEventCategories.map((item) => {
          return {
            contentName: item
          };
        })
      },
      {
        header: filterHeaderNames.description,
        maxSelect: uniqueEventDescription.length,
        type: 'eventDescriptions',
        items: uniqueEventDescription.map((item) => {
          return {
            contentName: item
          };
        })
      },
      {
        header: filterHeaderNames.flag,
        maxSelect: uniqueEventFlags.length,
        type: 'flags',
        items: uniqueEventFlags.map((item, index) => {
          return {
            contentName: item,
            index
          };
        })
      },
      {
        header: filterHeaderNames.measure,
        maxSelect: measureDatas.length,
        type: 'measures',
        items: measureDatas.map((item, index) => {
          return {
            contentName: item.measureName,
            index
          };
        })
      }],
      dropDownWidth,
      dropDownStyle,
      buttonStyle,
      textStyle,
      filterHeaderNames
    }]
  );
};
