import { getMouseCoordinate } from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/lib';
let wheelHandler = function (canvas) {
  return function (e) {
    let chart = canvas.getLinkedParent(),
      scale = chart.config.focusScaleX,
      // The total domain is wrt to the time navigator scale
      totalDomain = chart.config.contextScaleX.getDomain(),
      // Getting the range in pixel wrt to the scale attached to the drawing canvas
      [currStartRange, currEndRange] = scale.getRange(),
      originalEvent = e.originalEvent,
      direction,
      offset,
      startDomainValue,
      coordinate,
      chartX,
      r1,
      r2,
      sum,
      endDomainValue;

    if (Math.abs(originalEvent.deltaY) > Math.abs(originalEvent.deltaX)) {
      direction = 'vertical';
      offset = originalEvent.deltaY;
    } else {
      direction = 'horizontal';
      offset = originalEvent.deltaX;
    }

    if (direction === 'vertical') {
      coordinate = getMouseCoordinate(canvas.getFromEnv('chart-container'), e.originalEvent,
        chart);
      chartX = coordinate.chartX;
      // Finding the ratio in which the zooming has to be done such that zooming is done keeping the
      // category hovered at the center
      r1 = chartX - currStartRange;
      r2 = currEndRange - chartX;
      sum = r1 + r2;
      // Changing the Range wrt to the wheel value such that the gap changes (zoom)
      currStartRange -= 2 * offset * r1 / sum;
      currEndRange += 2 * offset * r2 / sum;
    } else { // Changing the Range wrt to the wheel value such that the gap remains the same (pan)
      // Pan is possbile only when current Start date in canvas is more than the start date of time navigator
      // Pan is possbile only when current End date in canvas is less than the end date of time navigator
      if (offset > 0 ? scale.getDomainValue(currEndRange) < totalDomain[1] :
        scale.getDomainValue(currStartRange) > totalDomain[0]) {
        currEndRange += offset;
        currStartRange += offset;
      }
    }
    startDomainValue = scale.getDomainValue(currStartRange);
    endDomainValue = scale.getDomainValue(currEndRange);
    // Re-setting the domain
    chart.setFocusLimit([startDomainValue, endDomainValue]);
  };
};

export default function (canvas) {
  let config = canvas.config;
  // Adding wheel event only once for the main canvas
  if (config.enableInteraction && !config.wheelEventAdded) {
    canvas.addEventListener('fc-wheel', wheelHandler(canvas));
    config.wheelEventAdded = true;
  }
}
