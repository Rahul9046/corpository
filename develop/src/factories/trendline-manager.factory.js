import TrendlineManager from '../components/trendline-manager';
import { pluck } from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/lib';

export default canvas => {
  let canvasConfig = canvas.config,
    dataSource = canvas.getFromEnv('dataSource'),
    chartConfig = canvas.getFromEnv('chartConfig'),
    chartAttrs = canvas.getFromEnv('chart-attrib'),
    paletteColors = dataSource.trendline && dataSource.trendline.palette,
    message = pluck(chartAttrs.nomeasurestodisplaymessage, 'NO MEASURES TO DISPLAY'),
    trendlineManager;

  if (canvasConfig.enableTrendlines) {
    trendlineManager = canvas.attachChild(TrendlineManager, 'trendlineManager');
    trendlineManager.configure({
      measureConfig: canvasConfig.measures,
      measureFormat: chartConfig.measureFormat,
      paletteColors,
      message
    });
  }
};
