import { componentFactory, pluck, pluckNumber } from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/lib';
import Canvas from '../components/canvas2';

const COLOR_D6D6D6 = '#d6d6d6',
  COLOR_FAFAFC = '#fafafc',
  NONE = 'none';
export default chart => {
  let chartConfig = chart.config,
    dataSource = chart.getFromEnv('dataSource'),
    getStyleDef = chart.getFromEnv('getStyleDef'),
    markerStyle = (dataSource.eventmarker && dataSource.eventmarker.marker && dataSource.eventmarker.marker.style) || {},
    trendlineStyle = (dataSource.trendline && dataSource.trendline.style) || {},
    markerBGStyle = getStyleDef(markerStyle.background),
    trendlineBgStyle = getStyleDef(trendlineStyle.background);

  componentFactory(
    chart,
    Canvas,
    'canvas_marker',
    1,
    [{
      enableGridLines: false,
      enableMouseTracking: 1,
      enableMarkers: 0,
      enableInteraction: 1,
      ownCompanyEvents: chartConfig.ownCompanyEvents,
      groupCompanyEvents: chartConfig.groupCompanyEvents,
      enableEventMarkers: true,
      leftBorder: true,
      rightBorder: true,
      topBorder: true,
      bottomBorder: true,
      style: {
        fill: pluck(markerBGStyle.fill, COLOR_FAFAFC),
        'fill-opacity': pluckNumber(markerBGStyle['fill-opacity'], 1),
        'stroke-opacity': pluckNumber(markerBGStyle['stroke-opacity'], 1),
        'stroke-dasharray': pluck(markerBGStyle['stroke-dasharray'], NONE),
        'stroke-width': pluckNumber(markerBGStyle['stroke-width'], 1),
        leftBorderStroke: pluck(markerBGStyle.stroke, COLOR_D6D6D6),
        rightBorderStroke: pluck(markerBGStyle.stroke, COLOR_D6D6D6),
        topBorderStroke: pluck(markerBGStyle.stroke, NONE),
        bottomBorderStroke: pluck(markerBGStyle.stroke, COLOR_D6D6D6)
      }
    }]
  );
  componentFactory(
    chart,
    Canvas,
    'canvas_trendline',
    1,
    [{
      measures: chartConfig.measures,
      enableGridLines: false,
      enableMouseTracking: 1,
      enableMarkers: 0,
      enableInteraction: 1,
      enableTrendlines: true,
      leftBorder: true,
      rightBorder: true,
      topBorder: true,
      bottomBorder: true,
      style: {
        fill: pluck(trendlineBgStyle.fill, COLOR_FAFAFC),
        'fill-opacity': pluckNumber(trendlineBgStyle['fill-opacity'], 1),
        'stroke-opacity': pluckNumber(trendlineBgStyle['stroke-opacity'], 1),
        'stroke-dasharray': pluck(trendlineBgStyle['stroke-dasharray'], NONE),
        'stroke-width': pluckNumber(trendlineBgStyle['stroke-width'], 1),
        leftBorderStroke: pluck(trendlineBgStyle.stroke, COLOR_D6D6D6),
        rightBorderStroke: pluck(trendlineBgStyle.stroke, COLOR_D6D6D6),
        topBorderStroke: pluck(trendlineBgStyle.stroke, COLOR_D6D6D6),
        bottomBorderStroke: pluck(trendlineBgStyle.stroke, COLOR_D6D6D6)
      }
    }]
  );
};
