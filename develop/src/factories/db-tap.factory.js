let dbTapHandler = function (canvas) {
    return function (e) {
      let chart = canvas.getLinkedParent(),
        scale = chart.config.focusScaleX,
        // Getting the range in pixel wrt to the scale attached to the drawing canvas
        [currStartRange, currEndRange] = scale.getRange(),
        startDomainValue,
        endDomainValue;

      // Changing the Range wrt to the wheel value such that the gap changes (zoom)
      currStartRange += offset;
      currEndRange -= offset;

      startDomainValue = scale.getDomainValue(currStartRange);
      endDomainValue = scale.getDomainValue(currEndRange);
      // Re-setting the domain
      chart.setFocusLimit([startDomainValue, endDomainValue]);
    };
  },
  // Flag to control the dbTap magnification
  offset = 20;

export default function (canvas) {
  let config = canvas.config;
  // Adding wheel event only once for the main canvas
  if (config.enableInteraction && !config.dbTapEventAdded) {
    canvas.addEventListener('fc-dbclick', dbTapHandler(canvas));
    config.dbTapEventAdded = true;
  }
}
