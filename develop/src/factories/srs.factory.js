import StandardRangeSelector from '../components/srs';
import { componentFactory } from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/lib';

export default chart => {
  let standardRangeSelector,
    extensions = chart.getFromEnv('dataSource').extensions,
    enabled = chart.config.enableStandardRangeSelector,
    chartConfig = chart.config;

  if (enabled) {
    componentFactory(chart, StandardRangeSelector, 'standardRangeSelector', 1);
    standardRangeSelector = chart.getChildren('standardRangeSelector')[0];
    standardRangeSelector.configure({
      'currentDomain': chartConfig.focusScaleX.getDomain(),
      'totalDomain': chartConfig.contextScaleX.getDomain(),
      'style': (extensions && extensions.standardrangeselector && extensions.standardrangeselector.style) || {}
    });
  } else {
    componentFactory(chart, StandardRangeSelector, 'standardRangeSelector', 0);
  }
};
