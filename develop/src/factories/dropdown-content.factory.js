import Content from '../components/filter-manager/dropdown/content';
export default contentBox => {
  let contentBoxConfig = contentBox.config,
    items = contentBoxConfig.items,
    content,
    i,
    len;

  for (i = 0, len = items.length; i < len; i++) {
    content = contentBox.attachChild(Content, 'content');
    content.configure(Object.assign({}, items[i], { extContentStyle: contentBoxConfig.extContentStyle }));
  }
};
