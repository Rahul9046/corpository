import { getMouseCoordinate } from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/lib';

let pinchStart = function (e) {
    startX = e.originalEvent.data.distanceX;
  },
  pinchMove = function (canvas) {
    return function (e) {
      let chart = canvas.getLinkedParent(),
        scale = chart.config.focusScaleX,
        // The total domain is wrt to the time navigator scale
        // Getting the range in pixel wrt to the scale attached to the drawing canvas
        [currStartRange, currEndRange] = scale.getRange(),
        data = e.originalEvent.data,
        distanceX = data.distanceX,
        startDomainValue,
        // Finding the difference between the distance of 2 fingers in the previous and current position
        diff = distanceX - startX,
        endDomainValue,
        coordinate = getMouseCoordinate(canvas.getFromEnv('chart-container'), e.originalEvent,
          chart),
        translationObj = canvas.getTranslation(),
        translationX = translationObj ? translationObj.x : 0,
        chartX = coordinate.chartX - translationX,
        // Finding the ratio in which the zooming has to be done such that zooming is done keeping the
        // category hovered at the center
        r1 = chartX - currStartRange,
        r2 = currEndRange - chartX,
        sum = r1 + r2;
      // Storing the current distance as the startX
      startX = distanceX;
      // If diff is positive then we are zooming in else zooming out
      currStartRange += 2 * diff * r1 / sum;
      currEndRange -= 2 * diff * r2 / sum;
      startDomainValue = scale.getDomainValue(currStartRange);
      endDomainValue = scale.getDomainValue(currEndRange);
      // Re-setting the domain
      chart.setFocusLimit([startDomainValue, endDomainValue]);
    };
  },
  // Flag to control the dbTap magnification
  startX;

export default function (canvas) {
  let config = canvas.config;
  // Adding wheel event only once for the main canvas
  if (config.enableInteraction && !config.pinchEventAdded) {
    canvas.addEventListener('fc-pinchstart', pinchStart);
    canvas.addEventListener('fc-pinchmove', pinchMove(canvas));
    config.pinchEventAdded = true;
  }
}
