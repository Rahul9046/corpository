import { componentFactory } from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/lib';
import CustomRangeSelector from '../components/crs';

export default chart => {
  let dataSource = chart.getFromEnv('dataSource'),
    extensions = dataSource.extensions || {},
    isEnabled = chart.config.enableCustomRangeSelector,
    focusScale = chart.config.focusScaleX;

  componentFactory(
    chart,
    CustomRangeSelector,
    'customRangeSelector',
    +isEnabled,
    [{
      domain: focusScale.getDomain(),
      style: (extensions.customrangeselector && extensions.customrangeselector.style) || {}
    }]
  );
};
