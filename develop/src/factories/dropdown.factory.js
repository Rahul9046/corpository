import Dropdown from '../components/filter-manager/dropdown';

export default manager => {
  let managerConfig = manager.config,
    filterHeaderNames = managerConfig.filterHeaderNames,
    dropDownInfo = managerConfig.dropDownInfo,
    dropdown,
    chartConfig = manager.getFromEnv('chartConfig'),
    selectedCategories = chartConfig.eventCategories,
    eventCategoryMap = chartConfig.eventCategoryMap,
    eventFlagMap = chartConfig.eventFlagMap,
    currDescription,
    currFlag,
    i,
    j,
    itemLen,
    selectionArr,
    allItems,
    arrangedItems,
    currItems,
    filteredDescriptionNames = [],
    filteredFlagsNames = [],
    type,
    len;
  for (i = 0, len = dropDownInfo.length; i < len; i++) {
    currItems = [];
    dropdown = manager.attachChild(Dropdown, 'dropdown');
    type = dropDownInfo[i].type;
    selectionArr = chartConfig[type].slice(0);
    allItems = dropDownInfo[i].items.slice(0);
    if (dropDownInfo[i].header === filterHeaderNames.description && selectedCategories.length) {
      for (j = 0, itemLen = allItems.length; j < itemLen; j++) {
        currDescription = allItems[j].contentName;
        // eslint-disable-next-line
        selectedCategories.forEach((selectedCategory) => {
          eventCategoryMap[selectedCategory] && eventCategoryMap[selectedCategory].forEach((eventDescription) => {
            if ((currDescription.toLowerCase() === eventDescription) &&
              !filteredDescriptionNames.includes(currDescription)) {
              currItems.push(allItems[j]);
              filteredDescriptionNames.push(currDescription);
            }
          });
        });
      }
      if (!currItems.length) {
        currItems = allItems;
      }
    } else if (dropDownInfo[i].header === filterHeaderNames.flag && selectedCategories.length) {
      for (j = 0, itemLen = allItems.length; j < itemLen; j++) {
        currFlag = allItems[j].contentName;
        // eslint-disable-next-line
        selectedCategories.forEach((selectedCategory) => {
          eventFlagMap[selectedCategory] && eventFlagMap[selectedCategory].forEach((eventFlag) => {
            if ((currFlag.toLowerCase() === eventFlag) &&
              !filteredFlagsNames.includes(currFlag)) {
              currItems.push(allItems[j]);
              filteredFlagsNames.push(currFlag);
            }
          });
        });
      }
      if (!currItems.length) {
        currItems = allItems;
      }
    } else {
      currItems = allItems;
    }
    if (selectionArr.length) {
      arrangedItems = [];
      // first push the selected items at thge top of the heirarchy so that selected items appear at top.
      arrangedItems.push(...(currItems.filter(item => selectionArr.includes(item.contentName.toLowerCase())))); // eslint-disable-line
      // then push the rest of the items.
      arrangedItems.push(...(currItems.filter(item => !selectionArr.includes(item.contentName.toLowerCase())))); // eslint-disable-line
      dropDownInfo[i].newItems = arrangedItems.slice(0);
    } else {
      dropDownInfo[i].newItems = currItems.slice(0);
    }
    dropdown.configure({
      dropDownInfo: dropDownInfo[i],
      selectionArr,
      width: managerConfig.dropDownWidth,
      selectCount: selectionArr.length,
      dropDownStyle: managerConfig.dropDownStyle
    });
  }
};
