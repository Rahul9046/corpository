import Trendlines from '../components/trendlines';

export default manager => {
  let managerConfig = manager.config,
    trendline,
    chart = manager.getFromEnv('chart'),
    chartConfig = chart.config,
    trendLineDatasets = managerConfig.trendLineDatasets,
    i,
    len;

  for (i = 0, len = trendLineDatasets.length; i < len; i++) {
    trendline = manager.attachChild(Trendlines, 'trendline');
    trendline.configure({
      datasetInfo: trendLineDatasets[i],
      xScale: chartConfig.focusScaleX,
      yScale: chartConfig.trendlineScaleY,
      index: i,
      trendLabelStyle: managerConfig.trendLabelStyle,
      showTrendLabel: chartConfig.showTrendLabels
    });
    // eslint-disable-next-line no-loop-func
    trendline.addExtEventListener('focusLimitChanged', ((_trendline) => {
      return () => {
        _trendline.setData({}, true);
      };
    })(trendline), chart);
  }
};
