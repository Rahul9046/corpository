import { componentFactory } from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/lib';
import Axis from '../components/axis';

export default chart => {
  let chartConfig = chart.config,
    scale = chartConfig.focusScaleX,
    getStyleDef = chart.getFromEnv('getStyleDef'),
    dataSource = chart.getFromEnv('dataSource'),
    axis,
    lineStyle,
    tickStyle,
    majorTickStyle,
    minorTickStyle,
    labelStyle,
    majorLabelStyle,
    minorLabelStyle;
  lineStyle = getStyleDef(dataSource.axis && dataSource.axis.line && dataSource.axis.line.style);
  tickStyle = (dataSource.axis && dataSource.axis.tick && dataSource.axis.tick.style) || {};
  labelStyle = (dataSource.axis && dataSource.axis.label && dataSource.axis.label.style) || {};
  majorTickStyle = getStyleDef(tickStyle.major);
  minorTickStyle = getStyleDef(tickStyle.minor);
  majorLabelStyle = getStyleDef(labelStyle.major);
  // reject provided font-size
  delete majorLabelStyle['font-size'];
  minorLabelStyle = getStyleDef(labelStyle.minor);
  // reject provided font-size
  delete minorLabelStyle['font-size'];
  componentFactory(
    chart,
    Axis,
    'axis',
    1,
    [{
      scale,
      majorTickStyle,
      minorTickStyle,
      majorLabelStyle,
      minorLabelStyle,
      lineStyle
    }]
  );
  axis = chart.getChildren('axis')[0];
  axis.addExtEventListener('focusLimitChanged', () => {
    axis.asyncDraw();
  }, chart);
};
