import Background from '../vendors/fusioncharts-xt/develop/src/vendors/fc-timeseries/src/_internal/components/background';
import { componentFactory, pluck, pluckNumber } from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/lib';

const COLOR_FFFFFF = '#ffffff',
  COLOR_D6D6D6 = '#d6d6d6';
export default chart => {
  let getStyleDef = chart.getFromEnv('getStyleDef'),
    chartAttrs = chart.getFromEnv('chart-attrib'),
    backgroundStyle = getStyleDef(chartAttrs.style && chartAttrs.style.background);
  componentFactory(chart, Background, 'background', 1, [{
    backgroundCss: {
      'fill': pluck(backgroundStyle.fill, COLOR_FFFFFF),
      'stroke': pluck(backgroundStyle.stroke, COLOR_D6D6D6),
      'stroke-width': pluckNumber(backgroundStyle['stroke-width'], 4),
      'stroke-opacity': pluckNumber(backgroundStyle['stroke-opacity'], 1),
      'fill-opacity': pluckNumber(backgroundStyle['fill-opacity'], 1)
    }
  }]);
};
