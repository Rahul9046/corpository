import EventMarker from '../components/event-marker';

const BLANKSTRING = '';
export default canvas => {
  let canvasConfig = canvas.config,
    chart = canvas.getFromEnv('chart'),
    chartConfig = canvas.getFromEnv('chartConfig'),
    getStyleDef = canvas.getFromEnv('getStyleDef'),
    dataSource = canvas.getFromEnv('dataSource'),
    markerObj = (dataSource.eventmarker && dataSource.eventmarker.marker) || {},
    eventBucketObj = (dataSource.eventbucket && dataSource.eventbucket) || {},
    laneTextHAlign = (eventBucketObj && eventBucketObj.text && eventBucketObj.text.halign) || BLANKSTRING,
    laneTextVAlign = (eventBucketObj && eventBucketObj.text && eventBucketObj.text.valign) || BLANKSTRING,
    textColorMap = (markerObj && markerObj.textcolormap) || {},
    flagColorMap = (markerObj && markerObj.flagcolormap) || {},
    eventMarkerStyle = (markerObj && markerObj.style) || {},
    eventLaneTextStyle = getStyleDef(eventBucketObj.text && eventBucketObj.text.style),
    eventLaneBucketStyle = getStyleDef(eventBucketObj.lane && eventBucketObj.lane.style),
    palette = eventBucketObj.lane && eventBucketObj.lane.palette,
    ownCompanyEventMarker,
    groupCompanyEventMarker,
    textStyle,
    flagStyle;

  textStyle = getStyleDef(eventMarkerStyle.text);
  delete textStyle['font-size'];
  delete eventLaneTextStyle['font-size'];
  flagStyle = getStyleDef(eventMarkerStyle.flag);
  if (canvasConfig.enableEventMarkers) {
    ownCompanyEventMarker = canvas.attachChild(EventMarker, 'ownCompanyEventMarker');
    ownCompanyEventMarker.configure({
      type: 'ownCompany',
      markerConfigurationArr: canvasConfig.ownCompanyEvents,
      xScale: chartConfig.focusScaleX,
      yScale: chartConfig.ownCompanyScale,
      textColorMap,
      flagColorMap,
      textStyle,
      flagStyle
    });
    ownCompanyEventMarker.addExtEventListener('focusLimitChanged', () => {
      ownCompanyEventMarker.setData({}, true);
    }, chart);
    groupCompanyEventMarker = canvas.attachChild(EventMarker, 'groupCompanyEventMarker');
    groupCompanyEventMarker.configure({
      type: 'groupCompany',
      markerConfigurationArr: canvasConfig.groupCompanyEvents,
      xScale: chartConfig.focusScaleX,
      yScale: chartConfig.groupCompanyScale,
      textColorMap,
      flagColorMap,
      textStyle,
      flagStyle,
      laneTextHAlign,
      laneTextVAlign,
      eventLaneTextStyle,
      eventLaneBucketStyle,
      palette
    });
    groupCompanyEventMarker.addExtEventListener('focusLimitChanged', () => {
      groupCompanyEventMarker.setData({}, true);
    }, chart);
  }
};
