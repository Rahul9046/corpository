import { componentFactory, pluckNumber } from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/lib';
import Reload from '../components/reload';

export default chart => {
  let chartAttrs = chart.getFromEnv('chart-attrib'),
    dataURL = chartAttrs.dataurl,
    chartStyle = chartAttrs.style || {},
    reloadButtonStyle = chartStyle.reloadbutton || {},
    buttonStyle = reloadButtonStyle.button || {},
    textStyle = reloadButtonStyle.text || {},
    drawLoader = pluckNumber(chartAttrs.showdefaultloader, 1);
  componentFactory(
    chart,
    Reload,
    'reload',
    1,
    [{
      dataURL,
      drawLoader,
      buttonStyle,
      textStyle
    }]
  );
};
