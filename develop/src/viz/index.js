import TimeSeries from '../vendors/fusioncharts-xt/develop/src/viz/timeseries';
import canvasFactory from '../factories/canvas.factory';
import axisFactory from '../factories/axis.factory';
import backgroundFactory from '../factories/background.factory';
import ScaleTime from '../vendors/fusioncharts-xt/develop/src/vendors/fc-utils/src/scales/time-bin';
import ScaleUTCTime from '../vendors/fusioncharts-xt/develop/src/vendors/fc-utils/src/scales/utc';
import Category from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/axis/scales/category';
import Linear from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/axis/scales/linear';
import BinDecider from '../vendors/fusioncharts-xt/develop/src/vendors/fc-utils/src/bin-decider';
import standardBins from '../vendors/fusioncharts-xt/develop/src/vendors/fc-utils/src/bin-decider/standard-bins';
import srsFactory from '../factories/srs.factory';
import crsFactory from '../factories/crs.factory';
import filterManagerFactory from '../factories/filter-manager-factory';
import toolbarFactory from '../vendors/fusioncharts-xt/develop/src/vendors/fc-timeseries/src/_internal/factories/toolbar-factory';
import toolbarManagerFactory from '../vendors/fusioncharts-xt/develop/src/vendors/fc-timeseries/src/viz/timeseries/factories/toolbar-manager-factory';
import NumberFormatter from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/number-formatter';
import messageLoggerFactory from '../factories/message-logger.factory';
import captionFactory from '../factories/caption.factory';
import subCaptionFactory from '../factories/subcaption.factory';
import zoomButtonFactory from '../factories/zoom-button.factory';
import reloadFactory from '../factories/reload.factory';
import jss from 'jss';
import TimeConverter from '../vendors/fusioncharts-xt/develop/src/vendors/fc-utils/src/time-converter';

import {
  pluckNumber,
  pluck,
  convertColor,
  UNDEF
} from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/lib';

import { timeMillisecond, timeSecond, timeMinute, timeHour, timeDay, timeWeek, timeMonth, timeYear } from '../vendors/fusioncharts-xt/develop/src/vendors/fc-utils/src/time-intervals';
import { utcMillisecond, utcSecond, utcMinute, utcHour, utcDay, utcWeek, utcMonth, utcYear } from '../vendors/fusioncharts-xt/develop/src/vendors/fc-utils/src/time-intervals/utc';

jss.createStyleSheet({
  /* eslint-disable */
  '@font-face': {
    'font-family': 'Source Sans Pro',
    'font-style': 'normal',
    'font-weight': 600,
    'src': "local('Source Sans Pro SemiBold'), local('SourceSansPro-SemiBold'), url(https://fonts.gstatic.com/s/sourcesanspro/v11/6xKydSBYKcSV-LCoeQqfX1RYOo3i54rwlxdu3cOWxw.woff2) format('woff2')",
    'unicode-range': "U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;"
  }
  /* eslint-enable */
}).attach();
jss.createStyleSheet({
  /* eslint-disable */
 '@font-face': {
    'font-family': 'Source Sans Pro',
    'font-style': 'normal',
    'font-weight': '700',
    'src': "local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v12/6xKydSBYKcSV-LCoeQqfX1RYOo3ig4vwlxdu3cOWxw.woff2) format('woff2')",
    'unicode-range': "U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD"
  }
  /* eslint-enable */
}).attach();

const DEFAULT_FT_FONT = 'Source Sans Pro',
  DURATION_DAY = 86400000,
  SUBCAPTION_HEIGHT = 17;
// Function to get the index of a particular column from the schema
let getIndexFromSchema = (schema, name) => {
    return schema.findIndex(ele => ele.name.toLowerCase() === name);
  },
  addEiMethods = (chartInstance, eiMethods) => {
    let methodName;

    for (methodName in eiMethods) {
      if (eiMethods.hasOwnProperty(methodName)) {
        chartInstance[methodName] = eiMethods[methodName];
      }
    }
  },
  // Function to check for valid string.
  isValidString = str => str !== '' && str !== UNDEF && str !== null,
  isValidNumber = x => x !== '' && x !== null && typeof +x === 'number' && !isNaN(+x) && x !== Infinity && x !== -Infinity,
  sanitizeData = (attrs) => {
    attrs.chart = attrs.chart || {};
    attrs.xaxis = attrs.xaxis || {};
  };
class Corpository extends TimeSeries {
  constructor () {
    super();
    let chart = this;
    chart.deregisterFactory('caption');
    chart.deregisterFactory('legend');
    chart.deregisterFactory('timeNavigator');
    chart.deregisterFactory('toolbarManager');
    chart.deregisterFactory('standardRangeSelector');
    chart.deregisterFactory('selectorToolbar');
    chart.deregisterFactory('customRangeSelector');
    chart.deregisterFactory('panel');
    chart.deregisterFactory('background');
    chart.deregisterFactory('multicanvasCrosslineManager');

    chart.registerFactory('chartBackground', backgroundFactory);
    chart.registerFactory('axis', axisFactory);
    chart.registerFactory('canvas', canvasFactory);
    chart.registerFactory('srs', srsFactory);
    chart.registerFactory('crs', crsFactory);
    chart.registerFactory('filterManagerFactory', filterManagerFactory);
    chart.registerFactory('toolbarFactory', toolbarFactory, ['canvas']);
    chart.registerFactory('toolbarManagerFactory', toolbarManagerFactory, ['srs', 'crs']);
    chart.registerFactory('messageLoggerFactory', messageLoggerFactory);
    chart.registerFactory('captionFactory', captionFactory);
    chart.registerFactory('subCaptionFactory', subCaptionFactory);
    chart.registerFactory('zoomButtonFactory', zoomButtonFactory);
    chart.registerFactory('reloadFactory', reloadFactory);
    chart.eiMethods = {
      updateData: function (dataJSON, schemaJSON) {
        if (!dataJSON) {
          return;
        }
        let FC = this,
          chartInstance = FC.apiInstance,
          dataTable = chartInstance.config.dataTable,
          schema = schemaJSON || dataTable.getData().schema,
          dataStore = dataTable.getDataStore(),
          jsonData = Object.assign({}, FC.getJSONData()),
          DataStoreDef = dataStore.constructor,
          newDataStore;
        newDataStore = new DataStoreDef(dataJSON, schema);
        jsonData.data = newDataStore.getDataTable();
        FC.setJSONData(jsonData);
      },
      getCurrentSelection: function () {
        let chartInstance = this.apiInstance,
          focusLimit = chartInstance.config.focusScaleX.getDomain();
        return {
          start: focusLimit[0],
          end: focusLimit[1]
        };
      },
      getTotalSelection: function () {
        let chartInstance = this.apiInstance,
          contextimit = chartInstance.config.contextScaleX.getDomain();
        return {
          start: contextimit[0],
          end: contextimit[1]
        };
      }
    };
  }
  __setDefaultConfig () {
    super.__setDefaultConfig();
    let config = this.config;
    config.enableStandardRangeSelector = 1;
    config.enableCustomRangeSelector = 1;
    config.allCanvasTop = 0;
    config.allCanvasLeft = 0;
    config.allCanvasWidth = 0;
    config.allCanvasHeight = 0;
    config.ownCompanyEvents = [];
    config.groupCompanyEvents = [];
    config.groupCompanyCatMap = [];
    config.dataMeasuresMap = {};
    config.eventCategoryMap = {};
    config.eventFlagMap = {};
    config.measureDatas = [];
    config.uniqueEventCategories = [];
    config.uniqueEventDescription = [];
    config.uniqueEventFlags = [];

    config.measures = [];
    config.flags = [];
    config.eventCategories = [];
    config.eventDescriptions = [];
  }
  /**
   * Method that validates that atleast two events, each for group company and own companies are present.
   */
  _checkForEnoughData () {
    let dataTable = this.getFromEnv('dataSource').data,
      dataObj = dataTable.getData(),
      schema = dataObj.schema,
      allData = dataObj.data,
      dateColumnIndex = getIndexFromSchema(schema, 'date'),
      eventCategoryIndex = getIndexFromSchema(schema, 'category of event'),
      eventDescpIndex = getIndexFromSchema(schema, 'description of event'),
      eventAbbrIndex = getIndexFromSchema(schema, 'abbreviation'),
      groupCompanyIndex = getIndexFromSchema(schema, 'group company filter'),
      validEvents = allData.filter(data => data[dateColumnIndex] && data[eventCategoryIndex] && data[eventDescpIndex] && data[eventAbbrIndex]),
      groupCompanyEvents = [],
      ownCompanyEvents = [];
    validEvents.forEach((eventObj) => {
      if (eventObj[groupCompanyIndex]) {
        groupCompanyEvents.push(eventObj);
      } else {
        ownCompanyEvents.push(eventObj);
      }
    });
    return !((groupCompanyEvents.length >= 1) || (ownCompanyEvents.length >= 1));
  }
  createMeasuresConfiguration () {
    let chart = this,
      config = chart.config,
      dataTable = config.dataTable,
      schema = dataTable.getData().schema,
      dataMeasuresMap = config.dataMeasuresMap,
      measuresData = config.measureDatas = [],
      i,
      len;
    for (i = 0, len = schema.length; i < len; i++) {
      if (schema[i].type.toLowerCase() === 'number') {
        dataMeasuresMap[schema[i].name] = i;
        measuresData.push({
          measureName: schema[i].name,
          measureData: [],
          maxValue: -Infinity,
          minValue: +Infinity
        });
      }
    }
  }
  /**
   * Function to check if the chart has the proper data or not
   * @return {boolean} if JSON data is valid or not
   */
  _checkInvalidData () {
    const dataObj = this.getFromEnv('dataSource'),
      chartInstance = this.getFromEnv('chartInstance');

    // Checking if the chart has proper data
    if (!dataObj.data) {
      chartInstance.__state.dataReady = false;
      chartInstance.jsVars.hasNativeMessage = true;
      chartInstance.jsVars.drawCount += 1;

      return true;
    } else {
      return this._checkForEnoughData();
    }
  }
  createMeasureData () {
    let chart = this,
      config = chart.config,
      dataTable = config.dataTable,
      dateColumnIndex = config.dateColumnIndex,
      dataArr = dataTable.getData().data,
      currData,
      dataMeasuresMap = config.dataMeasuresMap,
      measuresData = config.measureDatas,
      i,
      j,
      dataLen,
      measuresLen,
      currMeasure,
      value;
    for (i = 0, dataLen = dataArr.length; i < dataLen; i++) {
      currData = dataArr[i];
      for (j = 0, measuresLen = measuresData.length; j < measuresLen; j++) {
        currMeasure = measuresData[j];
        value = currData[dataMeasuresMap[currMeasure.measureName]];
        if (isValidNumber(value)) {
          currMeasure.measureData.push({
            date: currData[dateColumnIndex],
            value
          });
          (value > currMeasure.maxValue) && (currMeasure.maxValue = value);
          (value < currMeasure.minValue) && (currMeasure.minValue = value);
        }
      }
    }
  }
  sanitizeData (attrs) {
    sanitizeData.call(this, attrs);
  }
  // This API creates map of unique events descriptions of each category
  createEventCategoryMap () {
    let chart = this,
      config = chart.config,
      eventCategoryMap = config.eventCategoryMap,
      dataTable = config.dataTable,
      dataObj = dataTable.getData().data,
      eventCategoryIndex = config.eventCategoryIndex,
      eventDescpIndex = config.eventDescpIndex,
      i,
      len,
      currData,
      currCategoryName,
      currDescription;
    for (i = 0, len = dataObj.length; i < len; i++) {
      currData = dataObj[i];
      if (currData[eventCategoryIndex]) {
        currCategoryName = currData[eventCategoryIndex].toLowerCase();
        if (!eventCategoryMap[currCategoryName]) {
          eventCategoryMap[currCategoryName] = [];
        }
        currDescription = currData[eventDescpIndex].toLowerCase();
        if (currDescription && !eventCategoryMap[currCategoryName].includes(currDescription)) {
          eventCategoryMap[currCategoryName].push(currDescription);
        }
      }
    }
  }
  // This API creates map of unique events flags of each category
  createEventFlagMap () {
    let chart = this,
      config = chart.config,
      eventCategoryMap = config.eventFlagMap,
      dataTable = config.dataTable,
      dataObj = dataTable.getData().data,
      eventCategoryIndex = config.eventCategoryIndex,
      eventFlagIndex = config.eventFlagIndex,
      i,
      len,
      currData,
      currCategoryName,
      currFlag;
    for (i = 0, len = dataObj.length; i < len; i++) {
      currData = dataObj[i];
      if (currData[eventCategoryIndex]) {
        currCategoryName = currData[eventCategoryIndex].toLowerCase();
        if (!eventCategoryMap[currCategoryName]) {
          eventCategoryMap[currCategoryName] = [];
        }
        currFlag = currData[eventFlagIndex].toLowerCase();
        if (currFlag && !eventCategoryMap[currCategoryName].includes(currFlag)) {
          eventCategoryMap[currCategoryName].push(currFlag);
        }
      }
    }
  }
  configureAttributes (attrs) {
    let chart = this,
      config = chart.config,
      dataObj,
      dataTable,
      dateColumnIndex,
      initialZoom,
      initialFilterSelection,
      dateParser,
      chartAttrs = attrs.chart || {},
      dataSource = chart.getFromEnv('dataSource'),
      getStyleDef = chart.getFromEnv('getStyleDef'),
      chartStyle = (dataSource.chart && dataSource.chart.style) || {},
      baseTextStyle = getStyleDef(chartStyle.text),
      tooltipStyle = getStyleDef(chartStyle.tooltip),
      schema;
    // config.dataTable = dataTable = attrs.data;
    chart.createScales();
    chart.createBaseComponent();
    chart.getFromEnv('animationManager').setAnimationState(
      chart._firstConfigure ? 'initial' : 'update'
    );
    if (!chart.getFromEnv('chart-attrib')) {
      chartAttrs = attrs.chart || {};
      chart.addToEnv('chart-attrib', chartAttrs);
    } else {
      chartAttrs = chart.getFromEnv('chart-attrib');
    }
    config.measureFormat = chartAttrs.measureformat || {};
    dataTable = attrs.data ? (config.dataTable = attrs.data) : config.dataTable;
    chart.addToEnv('chartConfig', config);
    chart.addToEnv('chart-attrib', chartAttrs);
    // reject provided font-size
    delete baseTextStyle['font-size'];
    // for svg elements
    if (baseTextStyle.fill) {
      baseTextStyle.color = baseTextStyle.fill;
    }
    if (!baseTextStyle['font-family']) {
      baseTextStyle['font-family'] = 'Source Sans Pro';
    }
    chart.addToEnv('baseTextStyle', baseTextStyle);
    chart.addToEnv('tooltipStyle', Object.assign({}, baseTextStyle, tooltipStyle));
    config.showTrendLabels = pluckNumber(chartAttrs.showtrendlabels, 1);

    if (initialFilterSelection = chartAttrs.initialfilterselection) {
      initialFilterSelection.measures && (typeof initialFilterSelection.measures === 'object') &&
      (config.measures = initialFilterSelection.measures.map(item => item.toLowerCase()));

      initialFilterSelection.flags && (typeof initialFilterSelection.flags === 'object') &&
       (config.flags = initialFilterSelection.flags.map(item => item.toLowerCase()));

      initialFilterSelection.eventdescriptions && (typeof initialFilterSelection.eventdescriptions === 'object') &&
      (config.eventDescriptions = initialFilterSelection.eventdescriptions.map(item => item.toLowerCase()));

      initialFilterSelection.eventcategories && (typeof initialFilterSelection.eventcategories === 'object') &&
       (config.eventCategories = initialFilterSelection.eventcategories.map(item => item.toLowerCase()));
    }

    chart.addToEnv('dateAPI', (date, param, utc) => date['get' + (utc ? 'UTC' : '') + param]());
    dataObj = dataTable.getData();
    schema = dataObj.schema;
    config.dateColumnIndex = dateColumnIndex = getIndexFromSchema(schema, 'date');
    config.eventCategoryIndex = getIndexFromSchema(schema, 'category of event');
    config.eventDescpIndex = getIndexFromSchema(schema, 'description of event');
    config.groupCompanyIndex = getIndexFromSchema(schema, 'group company filter');
    config.eventAbbrIndex = getIndexFromSchema(schema, 'abbreviation');
    config.eventTooltipIndex = getIndexFromSchema(schema, 'tooltip');
    config.eventFlagIndex = getIndexFromSchema(schema, 'flag');
    config.dateFormat = schema[dateColumnIndex].format;
    if (initialZoom = chartAttrs.initialzoom) {
      dateParser = TimeConverter.parser(config.dateFormat);
      if (initialZoom.from) {
        config.startDate = dateParser.parse(initialZoom.from);
      }
      if (initialZoom.to) {
        config.endDate = dateParser.parse(initialZoom.to);
      }
    }
    chart.createEventCategoryMap();
    chart.createEventFlagMap();
    chart.createMarkerMapAndConfigurations();
    chart.createMeasuresConfiguration();
    chart.createMeasureData();
    chart.setTooltipStyle();
    chart.setScaleLimit();
  }
  setScaleLimit () {
    let chart = this,
      config = chart.config,
      startDate = config.startDate,
      endDate = config.endDate,
      dateColumnIndex = config.dateColumnIndex,
      dataTable = config.dataTable,
      dataArr = dataTable.getData().data,
      minFocusLimit,
      maxFocusLimit,
      dataLen = dataArr.length,
      totalDataStartDate = dataArr[0][dateColumnIndex],
      totalDataEndDate = dataArr[dataLen - 1][dateColumnIndex];
    minFocusLimit = startDate ? Math.max(totalDataStartDate, startDate) : totalDataStartDate;
    maxFocusLimit = endDate ? Math.min(totalDataEndDate, endDate) : totalDataEndDate;
    chart.setScaleDomain('contextScaleX', [totalDataStartDate, totalDataEndDate]);
    chart.setScaleDomain('focusScaleX', [minFocusLimit, maxFocusLimit]);
  }
  /**
   * Method to get all valid event objects
   * @param {Object} dataObj data object
   */
  getValidEvents (dataObj) {
    let chart = this,
      config = chart.config,
      dateColumnIndex = config.dateColumnIndex,
      eventCategoryIndex = config.eventCategoryIndex,
      eventDescpIndex = config.eventDescpIndex,
      eventAbbrIndex = config.eventAbbrIndex,
      validEvents = dataObj.filter(data => data[dateColumnIndex] && data[eventCategoryIndex] && data[eventDescpIndex] && data[eventAbbrIndex]);
    return validEvents;
  }
  makeScaleBins () {
    let chart = this,
      config = this.config,
      focusScaleX = config.focusScaleX,
      contextScaleX = config.contextScaleX,
      isUTC = chart.getFromEnv('isUTC'),
      bin = isUTC ? new BinDecider(standardBins(utcYear, utcMonth, utcWeek, utcDay, utcHour, utcMinute, utcSecond, utcMillisecond)) :
        new BinDecider(standardBins(timeYear, timeMonth, timeWeek, timeDay, timeHour, timeMinute, timeSecond, timeMillisecond));
    focusScaleX.setThresholdIntervals(bin.standardBins);
    contextScaleX.setThresholdIntervals(bin.standardBins);
    focusScaleX.setRangeThreshold(bin.standardBins[42]);
  }
  /**
   * Method to create all the event marker configurations and group event category map.
   */
  createMarkerMapAndConfigurations () {
    let chart = this,
      config = chart.config,
      dataTable = config.dataTable,
      dataObj = dataTable.getData(),
      data = dataObj.data,
      ownCompanyEvents = config.ownCompanyEvents = [],
      groupCompanyEvents = config.groupCompanyEvents = [],
      groupCompanyCatMap = config.groupCompanyCatMap,
      eventCategoryIndex = config.eventCategoryIndex,
      eventDescpIndex = config.eventDescpIndex,
      eventFlagIndex = config.eventFlagIndex,
      groupCompanyIndex = config.groupCompanyIndex,
      uniqueEventCategories = config.uniqueEventCategories,
      uniqueEventDescription = config.uniqueEventDescription,
      uniqueEventFlags = config.uniqueEventFlags,
      groupCompanyName,
      i,
      len,
      currData,
      currEventCategory,
      currEventDesc,
      currEventFlag,
      validEvents = chart.getValidEvents(data);
    for (i = 0, len = validEvents.length; i < len; i++) {
      currData = validEvents[i];
      currEventCategory = currData[eventCategoryIndex];
      currEventDesc = currData[eventDescpIndex];
      currEventFlag = currData[eventFlagIndex];
      if (isValidString(currEventCategory) && isValidString(currEventDesc)) {
        if (currEventCategory && !uniqueEventCategories.includes(currEventCategory)) {
          uniqueEventCategories.push(currEventCategory);
        }
        if (currEventDesc && !uniqueEventDescription.includes(currEventDesc)) {
          uniqueEventDescription.push(currEventDesc);
        }
        if (currEventFlag && !uniqueEventFlags.includes(currEventFlag)) {
          uniqueEventFlags.push(currEventFlag);
        }
        if (!currData[groupCompanyIndex]) { // own company event
          // store event object
          ownCompanyEvents.push(currData);
        } else if (isValidString(groupCompanyName = currData[groupCompanyIndex])) { // group company event
          // store event object
          groupCompanyEvents.push(currData);
          // store category
          !groupCompanyCatMap.includes(groupCompanyName) && groupCompanyCatMap.push(groupCompanyName);
        }
      }
    }
    // set the domain of the group company scale
    chart.setScaleDomain('groupCompanyScale', [-0.5, groupCompanyCatMap.length - 0.5]);
  }

  /**
   * Method to set the range of scale
   * @param {String} scaleType type of scale
   * @param {Array} range range of scale
   */
  setScaleRange (scaleType, range) {
    let scale = this.config[scaleType];
    scale.setRange(range);
  }

  /**
   * Method to set the domain of scale
   * @param {String} scaleType type of scale
   * @param {Array} range range of scale
   */
  setScaleDomain (scaleType, range) {
    let scale = this.config[scaleType];
    scale.setDomain(range);
  }
  /**
   * function to preconfigure chart API
   * @param {Object} dataObj input json data
   */
  preConfigure (dataObj) {
    if (!this.getFromEnv('number-formatter')) {
      this.addToEnv('number-formatter', new NumberFormatter(this, {
        numberscaleunit: 'K,M,B',
        numberscalevalue: '1000,1000,1000',
        decimals: 2
      }));
    }
    addEiMethods(this.getFromEnv('chartInstance'), this.eiMethods);
    this.fireEvent('preconfigure', dataObj);
  }
  /**
   * Method to create all the scales required by the chart
   */
  createScales () {
    let chart = this,
      config = chart.config,
      isUTC = chart.getFromEnv('UTC');
    config.contextScaleX = isUTC ? new ScaleUTCTime() : new ScaleTime();
    config.focusScaleX = isUTC ? new ScaleUTCTime() : new ScaleTime();
    config.ownCompanyScale = new Category();
    config.groupCompanyScale = new Category();
    config.trendlineScaleY = new Linear();
    chart.addToEnv('focusScalesX', [config.focusScale]);
    chart.addToEnv('contextScalesX', [config.contextScaleX]);
  }
  // API that validates current domain with respect to its total domain and previous domain.
  domainValidator (currentDomain, totalDomain, prevDomain = []) {
    let chart = this,
      mode,
      currentDomainStartValue = currentDomain[0],
      currentDomainEndValue = currentDomain[1],
      totalDomainStartValue = totalDomain[0],
      totalDomainEndValue = totalDomain[1],
      currentDomainStartValuems = +currentDomain[0],
      currentDomainEndValuems = +currentDomain[1],
      totalDomainStartValuems = +totalDomain[0],
      totalDomainEndValuems = +totalDomain[1],
      validatedDomainDiff;

    // if previous domain is defined, then derive the mode of transformation.
    if (prevDomain.length) {
      mode =
          Math.abs(
            currentDomainEndValuems -
            currentDomainStartValuems -
            (+prevDomain[1] - +prevDomain[0])
          ) > 1 ?
            'squeeze' :
            'drag';
    }
    // if current brush start domain value is less than total domain start value
    if (currentDomainStartValuems < totalDomainStartValuems) {
      currentDomainStartValue = totalDomainStartValue;
      currentDomainEndValue =
          mode === 'drag' ?
            new Date(
              currentDomainEndValuems +
            (totalDomainStartValuems - currentDomainStartValuems)
            ) :
            currentDomainEndValue;
    }
    // if current brush end domain value is greater than total domain end value
    if (currentDomainEndValuems > totalDomainEndValuems) {
      currentDomainEndValue = totalDomainEndValue;
      currentDomainStartValue =
          mode === 'drag' ?
            new Date(
              currentDomainStartValuems -
            (currentDomainEndValuems - totalDomainEndValuems)
            ) :
            currentDomainStartValue;
    }

    // if current brush end domain value is less than total domain start value. possible only if the
    // right handle is dragged across the left handle
    if (currentDomainEndValuems < totalDomainStartValuems) {
      currentDomainEndValue = totalDomainStartValue;
    }
    // if current brush start domain value is greater than total domain end value. possible only if the
    // left handle is dragged across the right handle
    if (currentDomainStartValuems > totalDomainEndValuems) {
      currentDomainStartValue = totalDomainEndValue;
    }

    // if current brush start domain value is greater than the current domain end value.
    if (currentDomainStartValuems > currentDomainEndValuems) {
      // swap the domains
      [currentDomainStartValue, currentDomainEndValue] = [
        currentDomainEndValue,
        currentDomainStartValue
      ];
    }
    validatedDomainDiff = currentDomainEndValue - currentDomainStartValue;
    // Ensures that the chart, at most, zooms in so that three plots may be
    // visible if the data is at regular intervals
    if (validatedDomainDiff >= 5 * DURATION_DAY) {
      // Raising event for domainvalidator which can be used by extensions
      chart.fireEvent('domainValidated');
      // fire public event
      chart.fireChartInstanceEvent('focusLimitChanged', {
        prevDomain: prevDomain,
        newDomain: [new Date(+currentDomainStartValue), new Date(+currentDomainEndValue)]
      });
      // return validated domain
      return [+currentDomainStartValue, +currentDomainEndValue];
    } else {
      return prevDomain;
    }
  }
  setFocusLimit (limit = []) {
    const chart = this,
      config = chart.config,
      contextScaleXDomain = config.contextScaleX.getDomain(),
      focusScaleXDomain = config.focusScaleX.getDomain(),
      dataLimit = this.domainValidator(limit, contextScaleXDomain, focusScaleXDomain);

    chart.setScaleDomain('focusScaleX', dataLimit);
    chart.fireEvent('focusLimitChanged');
  }
  // Method that sets style for tooltip
  setTooltipStyle () {
    let tooltipStyle = this.getFromEnv('tooltipStyle') || {},
      toolTipController = this.getFromEnv('toolTipController');
    toolTipController.setStyle({
      'background': pluck(tooltipStyle.background, ''),
      'bgColor': convertColor((tooltipStyle['background-color'] || 'FFF'), pluckNumber(tooltipStyle['background-opacity'] * 100, 100)),
      'rawBgColor': (tooltipStyle['background-color'] || 'FFF').replace(/\s+/g, '').replace(/^#?([a-f0-9]+)/ig, '#$1'),
      'fontColor': tooltipStyle.color || '#5f5f5f',
      'borderColor': convertColor((tooltipStyle['border-color'] || 'DCDDEE')),
      'rawBorderColor': (tooltipStyle['border-color'] || 'DCDDEE').replace(/\s+/g, '').replace(/^#?([a-f0-9]+)/ig, '#$1'),
      'bgAlpha': pluckNumber(tooltipStyle['background-opacity'] * 100, 100),
      'border': pluck(tooltipStyle.border, ''),
      'borderThickness': pluckNumber(tooltipStyle['border-width'], 1),
      'showTooltipShadow': pluckNumber(tooltipStyle['background-shadow'], 1),
      'borderRadius': pluckNumber(tooltipStyle['border-radius'], 3),
      'font-size': pluckNumber(tooltipStyle['font-size'], 11),
      'font-family': tooltipStyle['font-family'] || DEFAULT_FT_FONT,
      'font-weight': tooltipStyle['font-weight'] || 'normal',
      'padding': pluckNumber(tooltipStyle.padding, 3),
      'borderAlpha': pluckNumber(tooltipStyle['background-opacity'], 90),
      'showToolTipShadow': 1
    });
  }
  /**
   * Space manager of chart. All the child components are provided with
   * available width, available height and translation
   */
  manageSpace () {
    let chart = this,
      config = chart.config,
      markerCanvas = chart.getChildren('canvas_marker')[0],
      trendlineCanvas = chart.getChildren('canvas_trendline')[0],
      background = chart.getChildren('background')[0],
      axis = chart.getChildren('axis')[0],
      caption = chart.getChildren('caption') && chart.getChildren('caption')[0],
      reload = chart.getChildren('reload') && chart.getChildren('reload')[0],
      zoominButton = chart.getChildren('zoominButton')[0],
      zoomoutButton = chart.getChildren('zoomoutButton')[0],
      captionDim,
      subCaption = chart.getChildren('subCaption') && chart.getChildren('subCaption')[0],
      filterManager = chart.getChildren('filterManager')[0],
      // dropDownWidth = filterManager.config.dropDownWidth,
      selectorToolBar = chart.getFromEnv('selectorToolbar'),
      buttonDimension,
      reloadDimension,
      width = +chart.getFromEnv('chartWidth'),
      height = +chart.getFromEnv('chartHeight'),
      availableHeight,
      left = 0,
      top = 0,
      padding = 10,
      // dropDownGap = 8, // static gap between two dropdowns
      toolDimension,
      toolHeight,
      canvasWidth,
      canvasHeight,
      canvasLeft,
      canvasTop,
      axisLeft,
      axisTop,
      axisWidth,
      axisHeight,
      buttonLeft,
      buttonTop;
    chart.makeScaleBins();

    // manage background space
    background.setDimension({
      height,
      width
    });
    background.setTranslation(left, top);
    canvasWidth = 0.8 * width;
    canvasLeft = 0.1 * width;
    // if (reload) {
    //   reloadDimension = reload.getDimension();
    //   reload.setTranslation({
    //     left: (left + width) - (reloadDimension.width / 2 + padding),
    //     top: top + (reloadDimension.height / 2 + padding)
    //   });
    // }
    top += 0.06 * height;
    // manage caption space
    if (caption) {
      caption.setTranslation({
        top,
        left: canvasLeft + 2
      });
      captionDim = caption.getDimension();
      top += captionDim.height - padding / 2;
    }
    // manage sub caption space
    if (subCaption) {
      subCaption.setTranslation({
        top,
        left: canvasLeft + 2
      });
      top += SUBCAPTION_HEIGHT + padding;
    }
    if (reload) {
      reloadDimension = reload.getDimension();
      reload.setTranslation({
        left: (canvasLeft + canvasWidth) - (reloadDimension.width / 2),
        top: top + (reloadDimension.height + padding) / 2
      });
    }
    // manage filters space.
    // filterManager.setTranslation({
    //   x: (canvasLeft + canvasWidth / 2) - (2 * dropDownWidth + 1.5 * dropDownGap), // taking the center of canvas and displace left by 2 dropdowns and 1.5 gap
    //   y: top
    // });
    filterManager.setTranslation({
      x: canvasLeft, // taking the center of canvas and displace left by 2 dropdowns and 1.5 gap
      y: top
    });
    filterManager.setDimension({
      width: canvasWidth - 0.2 * canvasWidth,
      height: 0.05 * height
    });

    // manage tools space
    top += 0.05 * height + 2 * padding;
    toolDimension = selectorToolBar.getLogicalSpace();
    toolHeight = toolDimension.height;
    selectorToolBar.setDimension({
      x: canvasLeft,
      y: top,
      width: canvasWidth
    });
    selectorToolBar.manageSpace();

    top += toolHeight + padding;
    buttonDimension = zoominButton.getDimension(); // both zoomin and zoomout dimensions are same
    buttonLeft = (canvasLeft + canvasWidth) - (2 * buttonDimension.width + padding / 2);
    buttonTop = top + buttonDimension.height / 2 + padding;
    zoominButton.setTranslation({
      left: buttonLeft,
      top: buttonTop
    });
    buttonLeft += buttonDimension.width + padding / 2;
    zoomoutButton.setTranslation({
      left: buttonLeft,
      top: buttonTop
    });
    // manage trendlines space.
    availableHeight = height - top;
    config.allCanvasLeft = canvasLeft;
    config.allCanvasTop = top;
    config.allCanvasWidth = canvasWidth;
    config.allCanvasHeight += 0.3 * availableHeight;
    trendlineCanvas.setTranslation(canvasLeft, top);
    trendlineCanvas.setDimension({
      width: canvasWidth,
      height: 0.3 * availableHeight
    });
    chart.setScaleRange('trendlineScaleY', [top + 0.3 * availableHeight - 2 * padding, buttonTop + buttonDimension.height + padding]);

    // manage canvas space
    top += 0.3 * availableHeight;
    availableHeight = height - top;

    canvasHeight = 0.9 * availableHeight;
    canvasTop = top;

    config.allCanvasHeight += canvasHeight;
    markerCanvas.setDimension({
      height: canvasHeight,
      width: canvasWidth
    });
    markerCanvas.setTranslation(canvasLeft, canvasTop);

    // set range of own company Y scale
    chart.setScaleRange('ownCompanyScale', [canvasTop + 0.15 * canvasHeight, canvasTop + padding]);

    axisLeft = canvasLeft + 0.1 * canvasWidth;
    axisTop = canvasTop + 0.25 * canvasHeight;
    axisWidth = 0.8 * canvasWidth;
    axisHeight = 0.1 * canvasHeight;
    axis.setTranslation({ x: axisLeft, y: axisTop });
    axis.setDimension({ width: axisWidth, height: axisHeight });

    // set range of group company Y scale
    chart.setScaleRange('groupCompanyScale', [axisTop + axisHeight + 2 * padding, canvasTop + canvasHeight - padding]);

    // set range of focus scale
    chart.setScaleRange('focusScaleX', [axisLeft, axisLeft + axisWidth]);

    // set range of context scale
    chart.setScaleRange('contextScaleX', [axisLeft, axisLeft + axisWidth]);
  }
  displayMessageLogger (config) {
    let chart = this,
      messageLogger = chart.getChildren('messageLogger')[0];
    messageLogger.setData(config, true);
  }
  redrawCanvasComponents () {
    let chart = this,
      config = chart.config,
      eventCanvas = chart.getChildren('canvas_marker')[0],
      trendlineCanvas = chart.getChildren('canvas_trendline')[0];
    eventCanvas.setData({}, true);
    trendlineCanvas.setData({ measures: config.measures }, true);
  }
  static getName () {
    return 'corpository';
  }

  getName () {
    return 'corpository';
  }
}
export default Corpository;
