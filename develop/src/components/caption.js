import { SmartRenderer } from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/component-interface';

const COLOR_686980 = '#686980';
class Caption extends SmartRenderer {
  constructor () {
    super();
    this._translation = {};
  }
  __setDefaultConfig () {
    let config = this.config;
    config.captionDefaultStyle = {
      'font-family': 'Source Sans Pro',
      'font-weight': 'bold',
      'font-size': '16px',
      'fill': COLOR_686980,
      'fill-opacity': '1',
      'font-style': 'normal'
    };
  }
  configureAttributes (obj) {
    let caption = this,
      config = caption.config;
    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        config[key] = obj[key];
      }
    }
  }
  getDimension () {
    let caption = this,
      config = caption.config,
      captionDefaultStyle = config.captionDefaultStyle,
      captionStyle = config.captionStyle,
      baseTextStyle = caption.getFromEnv('baseTextStyle'),
      mergedStyle,
      companyName = config.companyName,
      smartLabel = caption.getFromEnv('smartLabel');
    mergedStyle = Object.assign({}, captionDefaultStyle, baseTextStyle, captionStyle);
    smartLabel.setStyle(mergedStyle);
    return smartLabel.getOriSize(companyName);
  }
  setTranslation (obj) {
    this._translation.x = obj.left;
    this._translation.y = obj.top;
  }
  getTranslation () {
    return this._translation;
  }
  draw () {
    let caption = this,
      config = caption.config,
      captionDefaultStyle = config.captionDefaultStyle,
      captionStyle = config.captionStyle,
      translation = caption.getTranslation(),
      baseTextStyle = caption.getFromEnv('baseTextStyle'),
      mergedStyle;
    mergedStyle = Object.assign({}, captionDefaultStyle, baseTextStyle, captionStyle);
    caption.addGraphicalElement({
      el: 'text',
      attr: {
        x: translation.x,
        y: translation.y,
        text: config.companyName.toUpperCase(),
        fill: mergedStyle.fill,
        'fill-opacity': mergedStyle['fill-opacity'],
        'font-style': mergedStyle['font-style'],
        'font-family': mergedStyle['font-family'],
        'font-weight': mergedStyle['font-weight'],
        'font-size': mergedStyle['font-size']
      },
      container: {
        id: 'meso',
        label: 'meso',
        isParent: true
      },
      id: 'caption',
      label: 'caption'
    });
  }
}

export default Caption;
