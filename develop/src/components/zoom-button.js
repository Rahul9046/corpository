import { SmartRenderer } from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/component-interface';
import { getDep } from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/dependency-manager';
import { TRACKER_FILL } from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/lib';

const COLOR_WHITE = '#ffffff',
  Raphael = getDep('redraphael', 'plugin'),
  mathCos = Math.cos,
  mathSin = Math.sin,
  POINTER = 'pointer',
  NOT_ALLOWED = 'not-allowed',
  ZOOMIN = 'zoomIn',
  zoomOutIcon = (x, y, radius) => {
    var
      icoX = x - radius * 0.2,
      icoY = y - radius * 0.2,
      rad = radius * 0.8,
      startAngle = Raphael.rad(43),
      endAngle = Raphael.rad(48), // to prevent cos and sin of start and end from becoming equal on 360 arcs
      startX = icoX + rad * mathCos(startAngle),
      startY = icoY + rad * mathSin(startAngle),
      endX = icoX + rad * mathCos(endAngle),
      endY = icoY + rad * mathSin(endAngle),
      handleHeight = radius, // the height of the handle
      handAngle = Raphael.rad(45),
      handX1 = startX + handleHeight * mathCos(handAngle),
      handY1 = startY + handleHeight * mathSin(handAngle),
      handX2 = endX + handleHeight * mathCos(handAngle),
      handY2 = endY + handleHeight * mathSin(handAngle),
      semiW = 2;

    return ['M', startX, startY,
      'A', rad, rad, 0, 1, 0, endX, endY, 'Z', 'M', startX + 1, startY + 1, 'L',
      handX1, handY1, handX2, handY2, endX + 1,
      endY + 1, 'Z', 'M', icoX - semiW, icoY, 'L', icoX + semiW,
      icoY, 'Z'];
  },

  zoomInIcon = (x, y, radius) => {
    var icoX = x - radius * 0.2,
      icoY = y - radius * 0.2,
      rad = radius * 0.8,
      startAngle = Raphael.rad(43),
      // to prevent cos and sin of start and end from becoming equal on 360 arcs
      endAngle = Raphael.rad(48),
      startX = icoX + rad * mathCos(startAngle),
      startY = icoY + rad * mathSin(startAngle),
      endX = icoX + rad * mathCos(endAngle),
      endY = icoY + rad * mathSin(endAngle),
      handleHeight = radius, // the height of the handle
      handAngle = Raphael.rad(45),
      handX1 = startX + handleHeight * mathCos(handAngle),
      handY1 = startY + handleHeight * mathSin(handAngle),
      handX2 = endX + handleHeight * mathCos(handAngle) - 1,
      handY2 = endY + handleHeight * mathSin(handAngle) - 1,
      semiW = 2;

    return ['M', startX, startY,
      'A', rad, rad, 0, 1, 0, endX, endY, 'Z', 'M', startX + 1, startY + 1, 'L',
      handX1, handY1, handX2, handY2, endX + 1,
      endY + 1, 'Z', 'M', icoX - semiW, icoY, 'L', icoX + semiW,
      icoY, 'Z', 'M', icoX, icoY - semiW, 'L', icoX,
      icoY + semiW, 'Z'];
  },
  getPathArr = (type, x, y, radius) => {
    let pathArr = [];
    switch (type.toLowerCase()) {
      case 'zoomin':
        return zoomInIcon(x, y, radius);
      case 'zoomout':
        return zoomOutIcon(x, y, radius);
    }
    return pathArr;
  };
class ZoomButton extends SmartRenderer {
  constructor () {
    super();
    this._handlers = {};
    this._translation = {};
  }
  __setDefaultConfig () {
    super.__setDefaultConfig();
    let config = this.config;
    config.buttonRadius = 5.5;
    config.width = 17;
    config.height = 17;
    config.enabledColor = '#9e9e9e';
    config.disabledColor = '#e3e3e3';

    config.boxCosmetics = {
      'stroke-width': '1',
      'fill-opacity': '1',
      'stroke-opacity': '1',
      'fill': TRACKER_FILL
    };
    config.iconCosmetics = {
      'stroke-width': '1'
    };
  }
  attachHandlers () {
    let zoomButton = this,
      config = zoomButton.config,
      handlers = zoomButton._handlers,
      chart = zoomButton.getFromEnv('chart'),
      focusScale,
      type = config.type,
      offset = 30,
      multiplier = type === ZOOMIN ? 1 : -1;
    zoomButton.addEventListener('fc-click', handlers.clickHandler || (handlers.clickHandler = () => {
      if (config.enabled) {
        focusScale = chart.config.focusScaleX;
        let [currStartRange, currEndRange] = focusScale.getRange(),
          startDomainValue,
          endDomainValue;
        currStartRange += multiplier * offset;
        currEndRange -= multiplier * offset;
        startDomainValue = focusScale.getDomainValue(currStartRange);
        endDomainValue = focusScale.getDomainValue(currEndRange);
        // Re-setting the domain
        chart.setFocusLimit([startDomainValue, endDomainValue]);
      }
    }));
  }
  configureAttributes (obj) {
    let config = this.config;
    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        config[key] = obj[key];
      }
    }
    this.attachHandlers();
  }
  setTranslation (obj) {
    let translation = this._translation;
    translation.x = obj.left;
    translation.y = obj.top;
  }
  getTranslation () {
    return this._translation;
  }
  getDimension () {
    let config = this.config;
    return {
      width: config.width,
      height: config.height
    };
  }
  draw () {
    let button = this,
      config = button.config,
      boxCosmetics = config.boxCosmetics,
      iconCosmetics = config.iconCosmetics,
      buttonRadius = config.buttonRadius,
      width = config.width,
      height = config.height,
      pathArr,
      centerX,
      centerY,
      translation = button.getTranslation();
    centerX = translation.x;
    centerY = translation.y;
    pathArr = getPathArr(config.type, centerX, centerY, buttonRadius);
    // button icon
    button.addGraphicalElement({
      el: 'path',
      attr: {
        path: pathArr,
        fill: COLOR_WHITE,
        stroke: config.enabled ? config.enabledColor : config.disabledColor,
        'stroke-width': iconCosmetics['stroke-width']
      },
      component: button,
      id: config.name + '_button_icon',
      label: config.name + '_button_icon',
      container: {
        id: 'meso',
        label: 'meso',
        isParent: true
      }
    }, true);
    // button box
    button.addGraphicalElement({
      el: 'rect',
      attr: {
        x: centerX - width / 2,
        y: centerY - height / 2,
        width,
        height,
        r: 2.3,
        stroke: config.enabled ? config.enabledColor : config.disabledColor,
        fill: boxCosmetics.fill,
        'stroke-width': boxCosmetics['stroke-width'],
        cursor: config.enabled ? POINTER : NOT_ALLOWED
      },
      component: button,
      id: config.name + '_button_box',
      label: config.name + '_button_box',
      container: {
        id: 'meso',
        label: 'meso',
        isParent: true
      }
    });
  }
}
export default ZoomButton;
