import { SmartRenderer } from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/component-interface';
import { pluckNumber } from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/lib';

const MOUSEOVER = 'fc-mouseover',
  MOUSEDOWN = 'fc-mousedown',
  MOUSEUP = 'fc-mouseup',
  MOUSEMOVE = 'fc-mousemove',
  MOUSEOUT = 'fc-mouseout',
  CLICK = 'fc-click';
/**
 * This class is responsible for the tooltip and hover effect of dataplots and datamarkers
 */
class MouseAction extends SmartRenderer {
  configure () {
    super.configure();
    let mouseAction = this,
      config = mouseAction.config,
      dataSourceChart = mouseAction.getFromEnv('chart-attrib'),
      mouseTracker = mouseAction.getFromEnv('mouseTracker');
    config.showTooltip = pluckNumber(dataSourceChart.showtooltip, 1);
    // Attaching external event listener on mouseTracker for showing tooltip
    !config.eventAdded && mouseAction.addExtEventListener('canvasHovered', (function (_mouseAction) {
      config.eventAdded = true;
      return function () {
        switch (arguments[1].hoveredInfo && arguments[1].hoveredInfo.component && arguments[1].hoveredInfo.component.getType()) {
          case 'trendlineManager':
            _mouseAction._triggerTrendlineMouseActions(...arguments);
            break;
          default:
            _mouseAction._triggerMarkerMouseActions(...arguments);
        }
      };
    })(mouseAction), mouseTracker);
  }
  getMouseEvents (e, categoryIndex, markerIndex, componentType) {
    var mouseAction = this,
      config = mouseAction.config,
      lastCategoryIndex = config._lastCategoryIndex,
      lastMarkerIndex = config._lastMarkerIndex,
      lastComponentType = config._lastComponentHovered && config._lastComponentHovered.config.type,
      eventType = e.type,
      derivedEvensInfo = {
        fireOut: false,
        events: []
      };
    switch (eventType) {
      case CLICK:
        derivedEvensInfo.events.push(CLICK);
        break;
      case MOUSEMOVE:
        if (lastComponentType === componentType && lastCategoryIndex === categoryIndex &&
            lastMarkerIndex === markerIndex) {
          // clear mouseout timer(if any) if mousemove happens over the same plot
          clearTimeout(config.mouseoutTimer);
          derivedEvensInfo.events.push(MOUSEMOVE);
        } else {
          derivedEvensInfo.events.push(MOUSEOVER);
          derivedEvensInfo.fireOut = true;
        }

        break;
      case MOUSEDOWN:
        if (lastComponentType !== componentType || lastCategoryIndex !== categoryIndex ||
            lastMarkerIndex !== markerIndex) {
          derivedEvensInfo.fireOut = true;
          derivedEvensInfo.events.push(MOUSEOVER);
        }
        derivedEvensInfo.events.push(MOUSEDOWN);
        break;

      case MOUSEOVER:
        if (lastComponentType === componentType && lastCategoryIndex === categoryIndex &&
            lastMarkerIndex === markerIndex) {
          // clear mouseout timer(if any) if mouseover happens over the same plot
          clearTimeout(config.mouseoutTimer);
        } else {
          //  when hovering over an element that lies over the last hovered element the fireout the last hovered element
          derivedEvensInfo.fireOut = true;
          derivedEvensInfo.events.push(MOUSEOVER);
        }
        break;

      case MOUSEUP:
        derivedEvensInfo.events.push(MOUSEUP);
        // @todo: implement clcik from mouseend as well
        break;

      case MOUSEOUT :
        derivedEvensInfo.fireOut = true;
    }
    return derivedEvensInfo;
  }
  mouseoutHandler (e, lastComponentHovered) {
    var mouseAction = this,
      config = mouseAction.config;
    if (lastComponentHovered) {
      lastComponentHovered.setHoverOutEffect();
      lastComponentHovered._firePlotEvent && lastComponentHovered._firePlotEvent(MOUSEOUT, e, config._lastCategoryIndex, config._lastMarkerIndex);
    }
    // delete stored last component details
    delete config._lastCategoryIndex;
    delete config._lastMarkerIndex;
  }
  /**
   * Call back function of canvasHovered event responsible for hover effect and tooltip of event markers
   * @param {Object} customObj The event object of canvasHovered events
   */
  _triggerMarkerMouseActions (customObj) {
    let mouseAction = this,
      config = mouseAction.config,
      data = customObj.data,
      e = data.e,
      currEvent = e.eventType,
      toolTipController = mouseAction.getFromEnv('toolTipController'),
      chartX = data.chartX,
      chartY = data.chartY,
      parent = mouseAction.getLinkedParent(),
      hoveredInfo = data.hoveredInfo,
      hovered = hoveredInfo.hovered,
      showTooltip = config.showTooltip,
      _lastCategoryIndex = config._lastCategoryIndex,
      derivedEventsInfo,
      eventMarker = hoveredInfo.component,
      catIndex = hoveredInfo.hoveredCategoryIndex,
      markerIndex = hoveredInfo.hoveredMarkerIndex,
      tooltext = hoveredInfo.hovered && eventMarker.config.duplicateMarkerArr[catIndex].markers[markerIndex].toolText;
    if (hovered) {
      derivedEventsInfo = mouseAction.getMouseEvents(e, catIndex, markerIndex, eventMarker.config.type);
      if (currEvent !== MOUSEOUT) {
        showTooltip && (config.currentToolTip = toolTipController.drawAt(chartX, chartY, tooltext, config.currentToolTip, parent));
      }
      if (derivedEventsInfo.fireOut && typeof _lastCategoryIndex !== 'undefined') {
        if (!derivedEventsInfo.events.length) {
          config.mouseoutTimer = setTimeout(function () {
            mouseAction.mouseoutHandler(e, config._lastComponentHovered);
          }, 20);
        } else {
          mouseAction.mouseoutHandler(e, config._lastComponentHovered);
          clearTimeout(config.mouseoutTimer);
        }
      }
      if (derivedEventsInfo.events.length) {
        config._lastCategoryIndex = catIndex;
        config._lastMarkerIndex = markerIndex;
        eventMarker.setHoverInEffect(catIndex, markerIndex);
        derivedEventsInfo.events.forEach((eventType) => {
          eventMarker._firePlotEvent(eventType, e, catIndex, markerIndex);
        });
        config._lastComponentHovered = eventMarker;
      }
    } else {
      showTooltip && toolTipController.hide(config.currentToolTip);
      if (typeof _lastCategoryIndex !== 'undefined') {
        mouseAction.mouseoutHandler(e, config._lastComponentHovered);
      }
      // delete stored last component details
      delete config._lastCategoryIndex;
      delete config._lastMarkerIndex;
    }
  }
  /**
   * Call back function of canvasHovered event responsible for hover effect and tooltip of trendlines
   * @param {Object} customObj The event object of canvasHovered events
   */
  _triggerTrendlineMouseActions (customObj) {
    let mouseAction = this,
      config = mouseAction.config,
      data = customObj.data,
      toolTipController = mouseAction.getFromEnv('toolTipController'),
      chartX = data.chartX,
      chartY = data.chartY,
      parent = mouseAction.getLinkedParent(),
      hoveredInfo = data.hoveredInfo,
      hovered = hoveredInfo.hovered,
      hoveredData = hoveredInfo.hoveredData,
      showTooltip = config.showTooltip,
      trendlineManager = hoveredInfo.component,
      tooltext = hoveredInfo.hovered && hoveredData.tooltext;
    if (hovered) {
      showTooltip && (config.currentToolTip = toolTipController.drawAt(chartX, chartY, tooltext, config.currentToolTip, parent));
      config._lastComponentHovered = trendlineManager;
    } else {
      showTooltip && toolTipController.hide(config.currentToolTip);
    }
  }
}

export default MouseAction;
