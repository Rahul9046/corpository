import MouseTracker from '../vendors/fusioncharts-xt/develop/src/vendors/fc-timeseries/src/_internal/components/mouse-tracker';
import { getMouseCoordinate } from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/lib';

class MouseTracker2 extends MouseTracker {
  /**
   * Callback function for the mouseTracker
   * @param {Object} e The mouse Event
   */
  mouseEvtHandler (e) {
    if (!this.active) {
      return;
    }
    let mouseTracker = this,
      oriEvent = e.originalEvent,
      parent = mouseTracker.getLinkedParent(),
      chart = mouseTracker.getFromEnv('chart'),
      axis = chart.getChildren('axis')[0],
      axisTranslation = axis.getTranslation(),
      ownCompanyEventMarker = parent.getChildren('ownCompanyEventMarker') && parent.getChildren('ownCompanyEventMarker')[0],
      groupCompanyEventMarker = parent.getChildren('groupCompanyEventMarker') && parent.getChildren('groupCompanyEventMarker')[0],
      trendlineManager = parent.getChildren('trendlineManager') && parent.getChildren('trendlineManager')[0],
      coordinate,
      chartX,
      chartY,
      hoveredInfo = {};
    coordinate = getMouseCoordinate(mouseTracker.getFromEnv('chart-container'), oriEvent,
      mouseTracker.getFromEnv('chart'));
    chartX = coordinate.chartX;
    chartY = coordinate.chartY;
    if (ownCompanyEventMarker && groupCompanyEventMarker) {
      if (axisTranslation.y >= chartY) {
        hoveredInfo = ownCompanyEventMarker.checkPointerOverMarker(e, chartX, chartY);
      } else {
        hoveredInfo = groupCompanyEventMarker.checkPointerOverMarker(e, chartX, chartY);
      }
    } else if (trendlineManager) {
      hoveredInfo = trendlineManager.checkPointerOverTrendline(chartX, chartY);
    }
    // Raising an event even when no event marker is hovered
    mouseTracker.fireEvent('canvasHovered', { hoveredInfo, e, chartX, chartY });
  }
}

export default MouseTracker2;
