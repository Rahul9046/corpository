import { SmartRenderer } from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/component-interface';

const MAJOR = 'major',
  MINOR = 'minor',
  DEFAULT_MAJOR_TICK_LEN = 14,
  DEFAULT_MINOR_TICK_LEN = 11,
  COLOR_A7A6BD = '#a7a6bd',
  COLOR_686980 = '#686980',
  DEFAULT_FONT_FAMILY = 'Source Sans Pro',
  M = 'M',
  v = 'v',
  h = 'h',
  getTickPathArr = (x, y, type) => {
    if (type === MAJOR) {
      return [
        M, x, y,
        v, DEFAULT_MAJOR_TICK_LEN
      ];
    } else {
      return [
        M, x, y,
        v, DEFAULT_MINOR_TICK_LEN
      ];
    }
  },
  /**
   * Returns the time formatter based on tick type
   *
   * @param {Array} timeFormat list of formatters
   * @param {Array} tickType list of ticks
   * @param {Number} index index for which tick is to be calculated
   * @returns {Instance} formatter
   */
  getFormatter = (timeFormat, tickType, index) => {
    if (timeFormat && tickType) {
      return timeFormat[tickType[index]];
    }
  };
class Axis extends SmartRenderer {
  __setDefaultConfig () {
    let config = this.config;
    config.majorTicks = [];
    config.minorTicks = [];
    config.majorTickDefaultTextStyle = {
      'font-family': DEFAULT_FONT_FAMILY,
      'font-size': '14px',
      'font-weight': '600',
      'text-anchor': 'middle',
      'vertical-align': 'top',
      fill: COLOR_686980
    };
    config.minorTickDefaultTextStyle = {
      'font-family': DEFAULT_FONT_FAMILY,
      'font-size': '12px',
      'text-anchor': 'middle',
      'vertical-align': 'top',
      'font-weight': '400',
      fill: COLOR_686980
    };
    config.defaultLineStyle = {
      'stroke-width': '1',
      'stroke': COLOR_A7A6BD,
      'stroke-opacity': '1',
      'stroke-dasharray': 'none'
    };
    config.majorTickMarkDefaultStyle = {
      'stroke-width': '0.7',
      'stroke': COLOR_A7A6BD,
      'stroke-dasharray': [2, 2],
      'stroke-opacity': 1
    };
    config.minorTickMarkDefaultStyle = {
      'stroke-width': '0.7',
      'stroke': COLOR_A7A6BD,
      'stroke-dasharray': [2, 2],
      'stroke-opacity': 1
    };
    this._translation = {};
    this._dimensions = {};
  }
  configureAttributes (obj) {
    let axis = this,
      config = axis.config;
    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        config[key] = obj[key];
      }
    }
  }
  setDimension (dim) {
    let dimension = this._dimensions;
    dimension.width = dim.width;
    dimension.height = dim.height;
  }
  setTranslation (pos) {
    let translation = this._translation;
    translation.x = pos.x;
    translation.y = pos.y;
  }
  getDimension () {
    return this._dimensions;
  }
  getTranslation () {
    return this._translation;
  }
  createGroups () {
    let axis = this;
    // axis main container
    axis.addGraphicalElement({
      el: 'group',
      container: {
        id: 'strato',
        label: 'group',
        isParent: true
      },
      component: axis,
      label: 'axis-strato-group',
      attr: {
        name: 'axis-strato'
      },
      id: 'strato'
    }, true);
    // major ticks group
    axis.addGraphicalElement({
      el: 'group',
      container: {
        id: 'strato',
        label: 'axis-strato-group',
        isParent: false
      },
      component: axis,
      label: 'major-ticks',
      attr: {
        name: 'major-ticks'
      },
      id: 'major-ticks'
    }, true);
    // minor ticks group
    axis.addGraphicalElement({
      el: 'group',
      container: {
        id: 'strato',
        label: 'axis-strato-group',
        isParent: false
      },
      component: axis,
      label: 'minor-ticks',
      attr: {
        name: 'minor-ticks'
      },
      id: 'minor-ticks'
    }, true);
  }
  createTicks () {
    let axis = this,
      config = axis.config,
      scale = config.scale,
      allTicks = scale.ticks(),
      majorTicks = config.majorTicks = [],
      minorTicks = config.minorTicks = [],
      tickTypeArr = scale._tickType,
      currTickType,
      format = scale.tickFormat(),
      formatter,
      text;

    allTicks.forEach((tick, index) => {
      formatter = getFormatter(scale._timeFormat, tickTypeArr, index);
      text = format(tick, formatter, tickTypeArr[index]);
      currTickType = tickTypeArr[index];
      if (currTickType === MAJOR) {
        majorTicks.push({
          tickValue: tick,
          text
        });
      } else if (currTickType === MINOR) {
        minorTicks.push({
          tickValue: tick,
          text
        });
      }
    });
  }
  drawTickMarks () {
    let axis = this,
      config = axis.config,
      scale = config.scale,
      majorTicks,
      minorTicks,
      minorTickLen,
      majorTickLen,
      translation = axis.getTranslation(),
      defaultMajorTickMarkStyle = config.majorTickMarkDefaultStyle,
      defaultMinorTickMarkStyle = config.minorTickMarkDefaultStyle,
      majorTickStyle = config.majorTickStyle,
      minorTickStyle = config.minorTickStyle,
      increments = axis.getTickMarkIncrement(),
      mergedMajorTickStyle,
      mergedMinorTickStyle,
      pathArr,
      scaleDomain = scale.getDomain(),
      currTickValue,
      xPos,
      yPos,
      i;
    majorTicks = config.majorTicks;
    minorTicks = config.minorTicks;
    mergedMajorTickStyle = Object.assign({}, defaultMajorTickMarkStyle, majorTickStyle);
    mergedMinorTickStyle = Object.assign({}, defaultMinorTickMarkStyle, minorTickStyle);
    // draw major tick marks
    for (i = 0, majorTickLen = majorTicks.length; i < majorTickLen; i += increments.majorTickLabelIncrement) {
      currTickValue = majorTicks[i].tickValue;
      if (+currTickValue >= +scaleDomain[0] && +currTickValue <= +scaleDomain[1]) {
        xPos = scale.getRangeValue(currTickValue);
        yPos = translation.y;
        pathArr = getTickPathArr(xPos, yPos, 'major');
        axis.addGraphicalElement({
          el: 'path',
          component: axis,
          container: {
            id: 'major-ticks',
            label: 'group',
            isParent: false
          },
          attr: {
            path: pathArr,
            'stroke-width': mergedMajorTickStyle['stroke-width'],
            'stroke': mergedMajorTickStyle.stroke,
            'stroke-dasharray': mergedMajorTickStyle['stroke-dasharray'],
            'stroke-opacity': mergedMajorTickStyle['stroke-opacity']
          },
          label: 'majorTickMark',
          id: 'axis_major_tick_mark_' + i
        });
      }
    }
    // draw minor tick marks
    for (i = 0, minorTickLen = minorTicks.length; i < minorTickLen; i += increments.minorTickLabelIncrement) {
      currTickValue = minorTicks[i].tickValue;
      if (+currTickValue >= +scaleDomain[0] && +currTickValue <= +scaleDomain[1]) {
        xPos = scale.getRangeValue(currTickValue);
        yPos = translation.y;
        pathArr = getTickPathArr(xPos, yPos, 'minor');
        axis.addGraphicalElement({
          el: 'path',
          component: axis,
          container: {
            id: 'minor-ticks',
            label: 'group',
            isParent: false
          },
          attr: {
            path: pathArr,
            'stroke-width': mergedMinorTickStyle['stroke-width'],
            'stroke': mergedMinorTickStyle.stroke,
            'stroke-dasharray': mergedMinorTickStyle['stroke-dasharray'],
            'stroke-opacity': mergedMinorTickStyle['stroke-opacity']
          },
          label: 'minorTickMark',
          id: 'axis_minor_tick_mark_' + i
        });
      }
    }
  }
  getTickMarkIncrement () {
    let axis = this,
      config = axis.config,
      majorTicks = config.majorTicks,
      minorTicks = config.minorTicks,
      scale = config.scale,
      scaleDomain = scale.getDomain(),
      smartLabel = axis.getFromEnv('smartLabel'),
      majorTickDefaultTextStyle = config.majorTickDefaultTextStyle,
      minorTickDefaultTextStyle = config.minorTickDefaultTextStyle,
      baseTextStyle = axis.getFromEnv('baseTextStyle'),
      majorLabelStyle = config.majorLabelStyle,
      minorLabelStyle = config.minorLabelStyle,
      majorTickMergedTextStyle,
      minorTickMergedTextStyle,
      majorTickLabelIncrement = 1,
      minorTickLabelIncrement = 1,
      smartText,
      currTickValue,
      currTickLabel,
      majorFirstTextDim,
      minorFirstTextDim,
      xPos,
      i,
      len;
    majorTickMergedTextStyle = Object.assign({}, majorTickDefaultTextStyle, baseTextStyle, majorLabelStyle);
    // ignore application of line-height
    delete majorTickMergedTextStyle['line-height'];
    minorTickMergedTextStyle = Object.assign({}, minorTickDefaultTextStyle, baseTextStyle, minorLabelStyle);
    // ignore application of line-height
    delete minorTickMergedTextStyle['line-height'];
    smartLabel.setStyle(majorTickMergedTextStyle);
    // calculate label increment for major ticks
    for (i = 0, len = majorTicks.length; i < len; i++) {
      currTickValue = majorTicks[i].tickValue;
      currTickLabel = majorTicks[i].text;
      if (+currTickValue >= +scaleDomain[0] && +currTickValue <= +scaleDomain[1]) {
        xPos = scale.getRangeValue(currTickValue);
        smartText = smartLabel.getOriSize(currTickLabel);
        if (!majorFirstTextDim) {
          majorFirstTextDim = {
            x: xPos,
            width: smartText.width
          };
        } else {
          smartText = smartLabel.getOriSize(currTickLabel);
          // minimum 2px gap between two labels
          if (((xPos - smartText.width / 2) - (majorFirstTextDim.x + majorFirstTextDim.width / 2)) >= 2) {
            break;
          } else {
            majorTickLabelIncrement++;
          }
        }
      }
    }
    smartLabel.setStyle(minorTickMergedTextStyle);
    // calculate label increment for major ticks
    for (i = 0, len = minorTicks.length; i < len; i++) {
      currTickValue = minorTicks[i].tickValue;
      currTickLabel = minorTicks[i].text;
      if (+currTickValue >= +scaleDomain[0] && +currTickValue <= +scaleDomain[1]) {
        xPos = scale.getRangeValue(currTickValue);
        smartText = smartLabel.getOriSize(currTickLabel);
        if (!minorFirstTextDim) {
          minorFirstTextDim = {
            x: xPos,
            width: smartText.width
          };
        } else {
          smartText = smartLabel.getOriSize(currTickLabel);
          if ((minorFirstTextDim.x + minorFirstTextDim.width / 2) < (xPos - smartText.width / 2)) {
            break;
          } else {
            minorTickLabelIncrement++;
          }
        }
      }
    }
    return {
      majorTickLabelIncrement,
      minorTickLabelIncrement
    };
  }
  drawTickLabels () {
    let axis = this,
      config = axis.config,
      scale = config.scale,
      majorTicks,
      minorTicks,
      minorTickLen,
      majorTickLen,
      currTickLabel,
      translation = axis.getTranslation(),
      scaleDomain = scale.getDomain(),
      majorTickDefaultTextStyle = config.majorTickDefaultTextStyle,
      minorTickDefaultTextStyle = config.minorTickDefaultTextStyle,
      majorLabelStyle = config.majorLabelStyle,
      minorLabelStyle = config.minorLabelStyle,
      baseTextStyle = axis.getFromEnv('baseTextStyle'),
      majorTickMergedTextStyle,
      minorTickMergedTextStyle,
      currTickValue,
      increments,
      xPos,
      yPos,
      i;
    majorTickMergedTextStyle = Object.assign({}, majorTickDefaultTextStyle, baseTextStyle, majorLabelStyle);
    // ignore application of line-height
    delete majorTickMergedTextStyle['line-height'];
    minorTickMergedTextStyle = Object.assign({}, minorTickDefaultTextStyle, baseTextStyle, minorLabelStyle);
    // ignore application of line-height
    delete minorTickMergedTextStyle['line-height'];
    majorTicks = config.majorTicks;
    minorTicks = config.minorTicks;
    increments = axis.getTickMarkIncrement();
    // draw major tick labels
    for (i = 0, majorTickLen = majorTicks.length; i < majorTickLen; i += increments.majorTickLabelIncrement) {
      currTickValue = majorTicks[i].tickValue;
      currTickLabel = majorTicks[i].text;
      if (+currTickValue >= +scaleDomain[0] && +currTickValue <= +scaleDomain[1]) {
        xPos = scale.getRangeValue(currTickValue);
        yPos = translation.y;
        axis.addGraphicalElement({
          el: 'text',
          component: axis,
          container: {
            id: 'major-ticks',
            label: 'group',
            isParent: false
          },
          attr: {
            text: currTickLabel,
            x: xPos,
            y: yPos + DEFAULT_MAJOR_TICK_LEN,
            fill: majorTickMergedTextStyle.fill,
            opacity: majorTickMergedTextStyle.opacity,
            'font-family': majorTickMergedTextStyle['font-family'],
            'font-style': majorTickMergedTextStyle['font-style'],
            'font-weight': majorTickMergedTextStyle['font-weight'],
            'font-size': majorTickMergedTextStyle['font-size'],
            'text-anchor': majorTickMergedTextStyle['text-anchor'],
            'vertical-align': majorTickMergedTextStyle['vertical-align']
          },
          label: 'majorTickLabel',
          id: 'axis_major_tick_label_' + i
        });
      }
    }
    // draw minor tick labels
    for (i = 0, minorTickLen = minorTicks.length; i < minorTickLen; i += increments.minorTickLabelIncrement) {
      currTickValue = minorTicks[i].tickValue;
      currTickLabel = minorTicks[i].text;
      if (+currTickValue >= +scaleDomain[0] && +currTickValue <= +scaleDomain[1]) {
        xPos = scale.getRangeValue(currTickValue);
        yPos = translation.y;
        axis.addGraphicalElement({
          el: 'text',
          component: axis,
          container: {
            id: 'major-ticks',
            label: 'group',
            isParent: false
          },
          attr: {
            text: currTickLabel,
            x: xPos,
            y: yPos + DEFAULT_MINOR_TICK_LEN,
            fill: minorTickMergedTextStyle.fill,
            opacity: minorTickMergedTextStyle.opacity,
            'font-family': minorTickMergedTextStyle['font-family'],
            'font-style': minorTickMergedTextStyle['font-style'],
            'font-weight': minorTickMergedTextStyle['font-weight'],
            'font-size': minorTickMergedTextStyle['font-size'],
            'text-anchor': minorTickMergedTextStyle['text-anchor'],
            'vertical-align': minorTickMergedTextStyle['vertical-align']
          },
          label: 'minorTickLabel',
          id: 'axis_minor_tick_label_' + i
        });
      }
    }
  }
  draw () {
    let axis = this,
      config = axis.config,
      defaultLineStyle = config.defaultLineStyle,
      lineStyle = config.lineStyle,
      mergedLineStyle,
      translation = axis.getTranslation(),
      dimension = axis.getDimension();
    mergedLineStyle = Object.assign({}, defaultLineStyle, lineStyle);
    axis.createGroups();
    axis.createTicks();
    // draw axis line
    axis.addGraphicalElement({
      el: 'path',
      component: axis,
      container: {
        id: 'strato',
        label: 'axis-strato-group',
        isParent: false
      },
      attr: {
        path: [
          M, translation.x, translation.y,
          h, dimension.width
        ],
        'stroke-width': mergedLineStyle['stroke-width'],
        'stroke': mergedLineStyle.stroke,
        'stroke-opacity': mergedLineStyle['stroke-opacity'],
        'stroke-dasharray': mergedLineStyle['stroke-dasharray']
      },
      label: 'axisLine',
      id: 'axis_line'
    });
    // draw tick marks
    axis.drawTickMarks();
    // draw tick labels
    axis.drawTickLabels();
  }
}

export default Axis;
