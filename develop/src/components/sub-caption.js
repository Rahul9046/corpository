import { SmartRenderer } from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/component-interface';

const COLOR_8C8C97 = '#8c8c97',
  COLOR_BLACK = '#000000';
class Caption extends SmartRenderer {
  constructor () {
    super();
    this._translation = {};
  }
  __setDefaultConfig () {
    let config = this.config;
    config.statusFlag = COLOR_BLACK;
    config.subCaptionDefaultStyle = {
      'font-family': 'Source Sans Pro',
      'font-weight': '600',
      'font-size': '13px',
      'fill': COLOR_8C8C97,
      'fill-opacity': '1',
      'font-style': 'normal'
    };
  }
  configureAttributes (obj) {
    let caption = this,
      config = caption.config;
    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        config[key] = obj[key];
      }
    }
  }
  setTranslation (obj) {
    this._translation.x = obj.left;
    this._translation.y = obj.top;
  }
  getTranslation () {
    return this._translation;
  }
  createGroup () {
    let subcaption = this,
      config = subcaption.config,
      translation = subcaption.getTranslation(),
      subCaptionDefaultStyle = config.subCaptionDefaultStyle,
      subCaptionStyle = config.subCaptionStyle,
      baseTextStyle = subcaption.getFromEnv('baseTextStyle'),
      mergedStyle = Object.assign({}, subCaptionDefaultStyle, baseTextStyle, subCaptionStyle);
    subcaption.addGraphicalElement({
      el: 'html',
      attr: {
        x: translation.x,
        y: translation.y,
        type: 'div'
      },
      css: {
        'user-select': 'none',
        '-ms-user-select': 'none',
        '-moz-user-select': 'none',
        '-webkit-user-select': 'none',
        '-webkit-touch-callout': 'none',
        'z-index': '0',
        color: mergedStyle.fill,
        'font-family': mergedStyle['font-family'],
        'font-weight': mergedStyle['font-weight'],
        'font-style': mergedStyle['font-style'],
        'font-size': mergedStyle['font-size'],
        'opacity': mergedStyle['fill-opacity'],
        width: '100%'
      },
      component: subcaption,
      label: 'subcaption-container',
      id: 'subcaption-container'
    }, true);
  }
  draw () {
    let subcaption = this,
      config = subcaption.config,
      cin = config.cin,
      status = config.status,
      state = config.state,
      city = config.city,
      subCaptionDefaultStyle = config.subCaptionDefaultStyle,
      subCaptionStyle = config.subCaptionStyle,
      baseTextStyle = subcaption.getFromEnv('baseTextStyle'),
      smartLabel = subcaption.getFromEnv('smartLabel'),
      cinText,
      cityNameText,
      stateText,
      mergedStyle;
    mergedStyle = Object.assign({}, subCaptionDefaultStyle, baseTextStyle, subCaptionStyle);
    subcaption.createGroup();
    smartLabel.setStyle(mergedStyle);
    // company CIN number
    if (cin) {
      cinText = 'CIN ' + cin.toUpperCase();
      subcaption.addGraphicalElement({
        el: 'html',
        attr: {
          type: 'div',
          innerHTML: cinText
        },
        container: {
          label: 'subcaption-container',
          id: 'subcaption-container',
          isParent: false
        },
        css: {
          'position': 'relative',
          float: 'left',
          'margin-right': '5px'
        },
        id: 'subcaption_company_cin',
        label: 'subcaption_company_cin'
      });
    }
    // company status
    if (status) {
      subcaption.addGraphicalElement({
        el: 'html',
        attr: {
          type: 'div',
          innerHTML: status
        },
        container: {
          label: 'subcaption-container',
          id: 'subcaption-container',
          isParent: false
        },
        css: {
          color: config.statusFlag,
          'position': 'relative',
          float: 'left',
          'margin-right': '5px'
        },
        id: 'subcaption_company_status',
        label: 'subcaption_company_status'
      });
    }
    // company city name
    if (city) {
      cityNameText = city + (state ? ', ' : '');
      subcaption.addGraphicalElement({
        el: 'html',
        attr: {
          type: 'div',
          innerHTML: cityNameText
        },
        container: {
          label: 'subcaption-container',
          id: 'subcaption-container',
          isParent: false
        },
        css: {
          'position': 'relative',
          float: 'left',
          'margin-right': '5px'
        },
        id: 'subcaption_company_city',
        label: 'subcaption_company_city'
      });
    }
    // company state name
    if (state) {
      stateText = state;
      subcaption.addGraphicalElement({
        el: 'html',
        attr: {
          type: 'div',
          innerHTML: stateText
        },
        container: {
          label: 'subcaption-container',
          id: 'subcaption-container',
          isParent: false
        },
        css: {
          'position': 'relative',
          float: 'left',
          'margin-right': '5px'
        },
        id: 'subcaption_company_state',
        label: 'subcaption_company_state'
      });
    }
  }
}

export default Caption;
