import Canvas from '../vendors/fusioncharts-xt/develop/src/vendors/fc-timeseries/src/_internal/components/canvas';
import eventMarkerFactory from '../factories/event-marker-factory';
import mouseTrackerFactory from '../factories/mouse-tracker.factory';
import mouseActionFactory from '../factories/tooltip-and-hover.factory';
import wheelFactory from '../factories/wheel.factory';
import swipeFactory from '../factories/swipe.factory';
import doubleTapFactory from '../factories/db-tap.factory';
import pinchZoomFactory from '../factories/pinch-zoom.factory';
import trendlineManagerFactory from '../factories/trendline-manager.factory';

const M = 'M',
  V = 'V',
  H = 'H',
  h = 'h',
  v = 'v',
  Z = 'Z';

class Canvas2 extends Canvas {
  constructor () {
    super();
    // de-register old factories
    this.deregisterFactory('dataset');
    this.deregisterFactory('timeMarker');
    this.deregisterFactory('referenceLine');
    this.deregisterFactory('verticalCrossline');
    this.deregisterFactory('horizontalCrossline');
    this.deregisterFactory('mouseTracker');
    this.deregisterFactory('tooltip');
    this.deregisterFactory('wheel');
    this.deregisterFactory('dbTap');
    this.deregisterFactory('swipe');
    this.deregisterFactory('pinchZoom');

    // register new factories
    this.registerFactory('eventMarker', eventMarkerFactory);
    this.registerFactory('mouseTracker2', mouseTrackerFactory);
    this.registerFactory('mouseAction', mouseActionFactory);
    this.registerFactory('wheelInteraction', wheelFactory);
    this.registerFactory('swipeInteraction', swipeFactory);
    this.registerFactory('doubleTapInteraction', doubleTapFactory);
    this.registerFactory('pinchZoomInteraction', pinchZoomFactory);
    this.registerFactory('trendlineManager', trendlineManagerFactory);
  }
  configureAttributes (obj) {
    super.configureAttributes(obj);
    let canvas = this,
      canvasConfig = canvas.config,
      borderConfig = canvasConfig.borderConfig;
    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        canvasConfig[key] = obj[key];
      }
    }
    // Setting the canvas border thickness
    ['leftBorder', 'topBorder', 'rightBorder', 'bottomBorder'].forEach(border => {
      // Parsing canvasBorder thickness only if provided by the chart
      obj[border] && (borderConfig[border] = obj.style['stroke-width']);
    });
  }
  /**
   * Fucntion to draw the canavs
   */
  _drawCanvas () {
    var canvas = this,
      config = canvas.config,
      borderConfig = config.borderConfig,
      canvasStyle = config.style,
      translateX = config._translateX,
      translateY = config._translateY,
      canvasTop = config.canvasTop + borderConfig.topBorder / 2 + translateY,
      canvasLeft = config.canvasLeft + borderConfig.leftBorder / 2 + translateX,
      canvasWidth = config.canvasWidth - (borderConfig.rightBorder / 2 + borderConfig.leftBorder / 2),
      canvasHeight = config.canvasHeight - (borderConfig.bottomBorder / 2 + borderConfig.topBorder / 2),
      canvasPaths = {
        leftBorder: [M, canvasLeft, canvasTop, V, canvasTop + canvasHeight],
        rightBorder: [M, canvasLeft + canvasWidth, canvasTop, V, canvasTop + canvasHeight],
        topBorder: [M, canvasLeft, canvasTop, H, canvasLeft + canvasWidth],
        bottomBorder: [M, canvasLeft, canvasTop + canvasHeight, H, canvasLeft + canvasWidth]
      },
      border;

    // Creating the canvas background
    canvas.addGraphicalElement({
      el: 'rect',
      component: canvas,
      label: 'canvas',
      container: {
        id: 'tropo',
        label: 'group',
        isParent: false
      },
      attr: {
        y: config.canvasBGTop + translateY,
        x: config.canvasBGLeft + translateX,
        width: config.canvasBGWidth,
        height: config.canvasBGHeight,
        fill: canvasStyle.fill,
        'stroke-width': 0,
        'stroke': 'none',
        'fill-opacity': canvasStyle['fill-opacity']
      },
      id: 'background'
    }, true);

    // Creating individual canvas borders
    for (border in borderConfig) {
      canvas.addGraphicalElement({
        el: 'path',
        component: canvas,
        label: 'canvas',
        container: {
          id: 'tropo',
          label: 'group',
          isParent: false
        },
        attr: {
          path: canvasPaths[border],
          stroke: canvasStyle[border + 'Stroke'],
          'stroke-width': borderConfig[border],
          'stroke-opacity': canvasStyle['stroke-opacity'],
          'stroke-dasharray': canvasStyle['stroke-dasharray']
        },
        id: 'border'
      }, true);
    }
  }

  /**
   * Function to create group for its child components
   */
  _createGroup () {
    let canvas = this,
      config = canvas.config,
      padding = config.padding,
      translateX = config._translateX,
      translateY = config._translateY,
      // Taking the padding into consideration as the clip-rect is shifed based on the translation applied
      // on the group
      canvasLeft = config.canvasBGLeft - padding.left + translateX,
      canvasTop = config.canvasBGTop - padding.top + translateY,
      canvasWidth = config.canvasBGWidth,
      canvasHeight = config.canvasBGHeight,

      clipPath = [
        M, canvasLeft, canvasTop,
        h, canvasWidth,
        v, canvasHeight,
        h, -canvasWidth,
        Z];

    // create group for canvas
    canvas.addGraphicalElement({
      el: 'group',
      container: {
        id: 'tropo',
        label: 'group',
        isParent: true
      },
      component: canvas,
      label: 'group',
      attr: {
        name: 'canvas-tropo'
      },
      id: 'tropo'
    });
    canvas.addGraphicalElement({
      el: 'group',
      container: {
        id: 'strato',
        label: 'group',
        isParent: true
      },
      component: canvas,
      label: 'strato',
      attr: {
        name: 'canvas-strato'
      },
      id: 'strato'
    });
    canvas.addGraphicalElement({
      el: 'group',
      container: {
        id: 'meso',
        label: 'group',
        isParent: true
      },
      component: canvas,
      label: 'meso',
      attr: {
        name: 'canvas-meso',
        'clip-path': clipPath
      },
      id: 'meso'
    });
    canvas.addGraphicalElement({
      el: 'group',
      container: {
        id: 'thermo',
        label: 'group',
        isParent: true
      },
      component: canvas,
      label: 'thermo',
      attr: {
        name: 'canvas-thermo'
      },
      id: 'thermo'
    });
    canvas.addToEnv('dsGroupclipPath', clipPath);
  }
}
export default Canvas2;
