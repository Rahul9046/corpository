import { SmartRenderer } from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/component-interface';
import TimeConverter from '../vendors/fusioncharts-xt/develop/src/vendors/fc-utils/src/time-converter';
import { getMouseCoordinate, extend2 } from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/lib';

const GROUP_COMPANY = 'groupCompany',
  M = 'M',
  L = 'L',
  l = 'l',
  h = 'h',
  v = 'v',
  Z = 'Z',
  // MOUSEUP = 'fc-mouseup',
  UNDEF = undefined,
  markerWidth = 20,
  markerHeight = 14,
  START = 'start',
  MIDDLE = 'middle',
  BOTTOM = 'bottom',
  COLOR_BLACK = '#000000',
  COLOR_C5C5D4 = '#c5c5d4',
  COLOR_686980 = '#686980',
  COLOR_A2A3B1 = '#a2a3b1',
  COLOR_CFCFDD = '#cfcfdd',
  COLOR_7F8093 = '#7f8093',
  NORMAL = 'normal',
  BOLD = 'bold',
  TEN_PX = '10px',
  NINE_PX = '9px',
  NEUTRAL_COLOR = 'Neutral',
  CROSSLINE_STROKE = 1,
  DEFAULT_FONT_FAMILY = 'Source Sans Pro',
  DEFAULT_FONT_SIZE = '11px',
  laneTextHPadding = 15,
  laneTextVPadding = 5,
  FCCLICK = 'fc-click',
  CLICK = 'EventMarkerClick',
  ROLLOVER = 'EventMarkerRollOver',
  ROLLOUT = 'EventMarkerRollOut',
  getMappedColor = (colorName) => {
    switch (colorName.toLowerCase()) {
      case 'yellow':
        return '#d4a900';
      case 'green':
        return '#34a34e';
      case 'red':
        return '#d52e2e';
      default:
        return '#979797';
    }
  },
  // function  to get the dimensions of the flag marker
  getMarkerPath = (x, y, width, height) => {
    return {
      x1: x,
      y1: y - height / 2,
      x2: x + width,
      y2: y,
      x3: x,
      y3: y + height / 2
    };
  },
  // function  to get the dimensions of the abbrevation text
  getTextDim = (x, y) => {
    return {
      x: x,
      y: y - markerHeight / 2
    };
  },
  // function to get the area of a triangle
  getArea = (x1, y1, x2, y2, x3, y3) => {
    return Math.abs((x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2);
  },
  getFlagPathArr = (x, y, width, height, isGroupCompany) => {
    return isGroupCompany ? [
      M, x, y,
      l, -5, -5,
      h, -width / 2,
      v, -(height + 4),
      h, (width + 10),
      v, (height + 4),
      h, -width / 2,
      Z
    ] : [
      M, x, y,
      l, -5, 5,
      h, -width / 2,
      v, (height + 4),
      h, (width + 10),
      v, -(height + 4),
      h, -width / 2,
      Z
    ];
  },
  getLaneTextHConfiguration = (x, width, align) => {
    switch (align) {
      case 'center':
        return {
          x: x + width / 2,
          anchor: 'middle'
        };
      case 'right':
        return {
          x: x + width - laneTextHPadding,
          anchor: 'end'
        };
      default:
        return {
          x: x + laneTextHPadding,
          anchor: 'start'
        };
    }
  },
  getLaneTextVConfiguration = (y, height, align) => {
    switch (align) {
      case 'top':
        return {
          y: y + laneTextVPadding,
          vAlign: 'top'
        };
      case 'bottom':
        return {
          y: y + height - laneTextVPadding,
          vAlign: 'bottom'
        };
      default:
        return {
          y: y + height / 2,
          vAlign: 'middle'
        };
    }
  };

class EventMarker extends SmartRenderer {
  __setDefaultConfig () {
    let config = this.config;
    config.markerDetails = [];
    config.markerDimensions = [];
    config.companyMap = {};
    config.duplicateMarkerArr = [];
    // config.pressed = false;
    config.palette = ['#f3f3f9', '#ffffff'];
    config.defaultMarkerFlagCosmetics = {
      'fill-opacity': '0.65',
      'stroke-opacity': '1',
      'stroke-dashArray': 'none',
      'stroke-width': '1'
    };
    config.defaultAbbrTextStyle = {
      'text-anchor': START,
      'vertical-align': BOTTOM,
      'font-family': DEFAULT_FONT_FAMILY,
      'font-size': TEN_PX
    };
    config.defaultDuplicateTextStyle = {
      'text-anchor': MIDDLE,
      'vertical-align': MIDDLE,
      'font-family': DEFAULT_FONT_FAMILY,
      'font-size': NINE_PX,
      'fill': COLOR_BLACK,
      'font-weight': '600'
    };
    config.defaultLaneTextStyle = {
      'font-family': DEFAULT_FONT_FAMILY,
      'font-size': TEN_PX,
      fill: COLOR_CFCFDD
    };
    config.defaultLaneBucketStyle = {
      'fill-opacity': '1',
      'stroke-opacity': '1',
      'stroke': 'none',
      'stroke-width': '0',
      'stroke-dasharray': 'none'
    };
    config.messageLoggerTimeout = UNDEF;
  }
  setHoverInEffect (hoveredCategoryIndex, hoveredMarkerIndex) {
    let eventMarker = this;
    eventMarker.setData({ hoveredCategoryIndex, hoveredMarkerIndex }, true);
  }
  setHoverOutEffect () {
    let eventMarker = this;
    eventMarker.setData({
      hoveredCategoryIndex: undefined,
      hoveredMarkerIndex: undefined
    }, true);
  }
  createLanes () {
    let eventMarker = this,
      config = eventMarker.config,
      laneTextHAlign = config.laneTextHAlign,
      laneTextVAlign = config.laneTextVAlign,
      duplicateMarkerArr = config.duplicateMarkerArr,
      eventLaneTextStyle = config.eventLaneTextStyle,
      defaultLaneBucketStyle = config.defaultLaneBucketStyle,
      eventLaneBucketStyle = config.eventLaneBucketStyle,
      laneBucketStyle,
      palette = config.palette,
      paletteLen = palette.length,
      baseTextStyle = eventMarker.getFromEnv('baseTextStyle'),
      defaultLaneTextStyle = config.defaultLaneTextStyle,
      laneTextStyle,
      yScale = config.yScale,
      canvasConfig = eventMarker.getLinkedParent().config,
      x = canvasConfig.canvasBGLeft + canvasConfig._translateX,
      y,
      width = canvasConfig.canvasBGWidth,
      height,
      compName,
      i,
      textHConfig,
      textVConfig,
      len = duplicateMarkerArr.length;
    laneTextStyle = Object.assign({}, defaultLaneTextStyle, baseTextStyle, eventLaneTextStyle);
    laneBucketStyle = Object.assign({}, defaultLaneBucketStyle, eventLaneBucketStyle);
    // ignore application of line-height
    delete laneTextStyle['line-height'];
    for (i = 0; i < len; i++) {
      compName = duplicateMarkerArr[i].compName;
      y = yScale.getRangeValue(i - 0.5);
      height = yScale.getRangeValue(i + 0.5) - y;
      // lane rect
      eventMarker.addGraphicalElement({
        el: 'rect',
        attr: {
          x,
          y,
          width,
          height,
          stroke: laneBucketStyle.stroke,
          'stroke-width': laneBucketStyle['stroke-width'],
          'stroke-opacity': laneBucketStyle['stroke-opacity'],
          'fill-opacity': laneBucketStyle['fill-opacity'],
          'stroke-dasharray': laneBucketStyle['stroke-dasharray'],
          fill: palette[i % paletteLen]
        },
        container: {
          label: GROUP_COMPANY + 'eventLanes-strato-group'
        },
        label: 'eventMarkersLanePath',
        id: GROUP_COMPANY + '_' + compName + 'lane_path_' + i
      });
      textHConfig = getLaneTextHConfiguration(x, width, laneTextHAlign);
      textVConfig = getLaneTextVConfiguration(y, height, laneTextVAlign);
      // lane text
      eventMarker.addGraphicalElement({
        el: 'text',
        attr: {
          x: textHConfig.x,
          y: textVConfig.y,
          text: compName.toUpperCase(),
          'font-family': laneTextStyle['font-family'],
          'text-anchor': textHConfig.anchor,
          'vertical-align': textVConfig.vAlign,
          'font-size': laneTextStyle['font-size'],
          fill: laneTextStyle.fill,
          opacity: laneTextStyle.opacity,
          'font-weight': laneTextStyle['font-weight'],
          'font-style': laneTextStyle['font-style']
        },
        container: {
          label: GROUP_COMPANY + 'eventLanes-strato-group'
        },
        label: 'eventMarkersLaneText',
        id: GROUP_COMPANY + '_' + compName + 'lane_text_' + i
      }, true);
    }
  }
  configureAttributes (obj) {
    let eventMarker = this,
      config = eventMarker.config,
      chartConfig = eventMarker.getFromEnv('chartConfig'),
      markerDetails,
      companyMap = config.companyMap,
      markerConfigs = obj.markerConfigurationArr || [],
      dateColumnIndex = chartConfig.dateColumnIndex,
      eventCategoryIndex = chartConfig.eventCategoryIndex,
      eventDescpIndex = chartConfig.eventDescpIndex,
      eventAbbrIndex = chartConfig.eventAbbrIndex,
      eventTooltipIndex = chartConfig.eventTooltipIndex,
      eventFlagIndex = chartConfig.eventFlagIndex,
      currMarkerConf,
      type,
      i,
      len = markerConfigs.length;
    obj.type && (config.type = type = obj.type);
    obj.xScale && (config.xScale = obj.xScale);
    obj.yScale && (config.yScale = obj.yScale);
    obj.textStyle && (config.textStyle = obj.textStyle);
    obj.flagStyle && (config.flagStyle = obj.flagStyle);
    obj.eventLaneTextStyle && (config.eventLaneTextStyle = obj.eventLaneTextStyle);
    obj.eventLaneBucketStyle && (config.eventLaneBucketStyle = obj.eventLaneBucketStyle);
    obj.textColorMap && (config.textColorMap = obj.textColorMap);
    obj.flagColorMap && (config.flagColorMap = obj.flagColorMap);
    obj.laneTextHAlign && (config.laneTextHAlign = obj.laneTextHAlign);
    obj.laneTextVAlign && (config.laneTextVAlign = obj.laneTextVAlign);
    // (obj.pressed !== UNDEF) && (config.pressed = obj.pressed);
    obj.palette && typeof obj.palette === 'object' && obj.palette.length && (config.palette = obj.palette);

    config.hoveredCategoryIndex = obj.hoveredCategoryIndex;
    config.hoveredMarkerIndex = obj.hoveredMarkerIndex;
    if (len) {
      markerDetails = config.markerDetails = [];
      if (type === GROUP_COMPANY) {
        let groupCompanyMap = chartConfig.groupCompanyCatMap,
          groupCompanyIndex = chartConfig.groupCompanyIndex;
        groupCompanyMap.forEach((compName, index) => {
          companyMap[compName] = index;
          markerDetails.push({
            compName,
            markers: []
          });
        });
        for (i = 0, len = markerConfigs.length; i < len; i++) {
          currMarkerConf = markerConfigs[i];
          markerDetails[companyMap[currMarkerConf[groupCompanyIndex]]].markers.push({
            date: currMarkerConf[dateColumnIndex],
            category: currMarkerConf[eventCategoryIndex],
            description: currMarkerConf[eventDescpIndex],
            abbrevation: currMarkerConf[eventAbbrIndex],
            tooltip: currMarkerConf[eventTooltipIndex],
            flag: currMarkerConf[eventFlagIndex]
          });
        }
      } else {
        config.companyMap = {
          'OwnCompany': 0
        };
        markerDetails.push({
          compName: 'OwnCompany',
          markers: []
        });
        for (i = 0, len = markerConfigs.length; i < len; i++) {
          currMarkerConf = markerConfigs[i];
          markerDetails[0].markers.push({
            date: currMarkerConf[dateColumnIndex],
            category: currMarkerConf[eventCategoryIndex],
            description: currMarkerConf[eventDescpIndex],
            abbrevation: currMarkerConf[eventAbbrIndex],
            tooltip: currMarkerConf[eventTooltipIndex],
            flag: currMarkerConf[eventFlagIndex]
          });
        }
      }
    }
    eventMarker.createDuplicateConfigArr();
    eventMarker.createTooltext();
  }

  _firePlotEvent (eventType, e, hoveredCategoryIndex, hoveredMarkerIndex) {
    let eventMarker = this,
      config = eventMarker.config,
      duplicateMarkerArr = config.duplicateMarkerArr,
      markerDetails = duplicateMarkerArr[hoveredCategoryIndex].markers[hoveredMarkerIndex] || {},
      eventArgs = markerDetails.eventArgs,
      chart = eventMarker.getFromEnv('chart'),
      coordinate = getMouseCoordinate(chart.getFromEnv('chart-container'), e, chart),
      args = extend2(coordinate, eventArgs);
    if (!eventArgs) {
      return;
    }
    switch (eventType) {
      case 'fc-mouseover':
        chart.fireChartInstanceEvent(ROLLOVER, args);
        break;
      case 'fc-mouseout':
        chart.fireChartInstanceEvent(ROLLOUT, args);
        break;
    }
  }
  // Method to check the presence of pointer of an event marker.
  checkPointerOverMarker (e, xPos, yPos) {
    let eventMarker = this,
      config = eventMarker.config,
      chart = eventMarker.getFromEnv('chart'),
      chartConfig = chart.config,
      markerDimensions = config.markerDimensions,
      duplicateMarkerArr = config.duplicateMarkerArr,
      categoryMarkers,
      i,
      j,
      categoryLen,
      currMarkerArea,
      currMarkerDim,
      eventArgs,
      coordinate,
      args,
      eventType,
      tooltext,
      x1,
      y1,
      x2,
      y2,
      x3,
      y3;
    for (i = 0, categoryLen = markerDimensions.length; i < categoryLen; i++) {
      categoryMarkers = markerDimensions[i];
      for (j = categoryMarkers.length - 1; j >= 0; j--) {
        currMarkerDim = categoryMarkers[j];
        x1 = currMarkerDim.x1;
        y1 = currMarkerDim.y1;
        x2 = currMarkerDim.x2;
        y2 = currMarkerDim.y2;
        x3 = currMarkerDim.x3;
        y3 = currMarkerDim.y3;
        currMarkerArea = getArea(x1, y1, x2, y2, x3, y3);
        // check if the pointer is over an event marker
        if (Math.abs((getArea(x1, y1, x2, y2, xPos, yPos) + getArea(x3, y3, x2, y2, xPos, yPos) +
        getArea(x1, y1, x3, y3, xPos, yPos)) - currMarkerArea) <= 1) {
          eventType = e.type;
          // if (eventType === MOUSEDOWN) {
          //   tooltext = duplicateMarkerArr[i].markers[j].toolText;
          //   config.disableDetection = true;
          //   config.pressed = true;
          //   // eslint-disable-next-line no-loop-func
          //   config.messageLoggerTimeout = setTimeout(() => {
          //     chart.getFromEnv('toolTipController').hide();
          //     config.disableDetection = false;
          //     eventMarker.setData({
          //       pressed: false
          //     }, true);
          //     chart.displayMessageLogger({
          //       display: 'block',
          //       message: tooltext
          //     });
          //   }, 500);
          // } else if (eventType === MOUSEUP) {
          //   clearTimeout(config.messageLoggerTimeout);
          //   config.disableDetection = false;
          //   config.pressed = false;
          // }
          if (eventType === FCCLICK) {
            tooltext = duplicateMarkerArr[i].markers[j].toolText;
            chartConfig.disableDetection = true;
            chart.getFromEnv('toolTipController').hide();
            eventArgs = duplicateMarkerArr[i].markers[j].eventArgs;
            coordinate = getMouseCoordinate(chart.getFromEnv('chart-container'), e, chart);
            args = extend2(coordinate, eventArgs);
            chart.fireChartInstanceEvent(CLICK, args);
            chart.displayMessageLogger({
              display: 'block',
              message: tooltext
            });
          } else if (!chartConfig.disableDetection) {
          // return the marker details
            return {
              hovered: true,
              hoveredCategoryIndex: i,
              hoveredMarkerIndex: j,
              tooltipArr: duplicateMarkerArr[i].markers[j].tooltipArr,
              flagArr: duplicateMarkerArr[i].markers[j].flagArr,
              abbrevationArr: duplicateMarkerArr[i].markers[j].abbrevationArr,
              component: eventMarker
            };
          }
        }
      }
    }
    // not hovered on any marker
    return {
      hovered: false,
      component: eventMarker
    };
  }
  createDuplicateConfigArr () {
    let eventMarker = this,
      config = eventMarker.config,
      markerDetails = config.markerDetails,
      chartConfig = eventMarker.getFromEnv('chartConfig'),
      selectedCategories = chartConfig.eventCategories,
      selectedDescriptions = chartConfig.eventDescriptions,
      selectedFlags = chartConfig.flags,
      duplicateArr = config.duplicateMarkerArr = [],
      prevMarkerDate,
      oldMarkerConfigs,
      scaleDomain = config.xScale.getDomain(),
      newMarkerDetails = {},
      newMarkerConfig = {},
      i,
      j,
      compLen,
      markerLen;
    for (i = 0, compLen = markerDetails.length; i < compLen; i++) {
      newMarkerDetails = {
        compName: markerDetails[i].compName,
        markers: []
      };
      oldMarkerConfigs = markerDetails[i].markers;
      prevMarkerDate = UNDEF;
      for (j = 0, markerLen = oldMarkerConfigs.length; j < markerLen; j++) {
        if (+oldMarkerConfigs[j].date < +scaleDomain[0] ||
          +oldMarkerConfigs[j].date > +scaleDomain[1]) {
          continue;
        }
        if ((+prevMarkerDate !== +oldMarkerConfigs[j].date) &&
          (!selectedCategories.length || (selectedCategories.includes(oldMarkerConfigs[j].category.toLowerCase()))) &&
          (!selectedDescriptions.length || (selectedDescriptions.includes(oldMarkerConfigs[j].description.toLowerCase()))) &&
          (!selectedFlags.length || (selectedFlags.includes(oldMarkerConfigs[j].flag.toLowerCase())))) {
          newMarkerConfig = {
            date: oldMarkerConfigs[j].date,
            abbrevationArr: [oldMarkerConfigs[j].abbrevation],
            flagArr: [oldMarkerConfigs[j].flag],
            tooltipArr: [oldMarkerConfigs[j].tooltip],
            count: 1,
            eventArgs: {
              companyType: markerDetails[i].compName,
              date: oldMarkerConfigs[j].date,
              isCompoundFlag: false,
              numberOfEvents: 1,
              eventDescription: [oldMarkerConfigs[j].description],
              eventCategory: [oldMarkerConfigs[j].category],
              eventFlag: [oldMarkerConfigs[j].flag]
            }
          };
          prevMarkerDate = oldMarkerConfigs[j].date;
          newMarkerDetails.markers.push(newMarkerConfig);
        } else if ((!selectedCategories.length || (selectedCategories.includes(oldMarkerConfigs[j].category.toLowerCase()))) &&
        (!selectedDescriptions.length || (selectedDescriptions.includes(oldMarkerConfigs[j].description.toLowerCase()))) &&
        (!selectedFlags.length || (selectedFlags.includes(oldMarkerConfigs[j].flag.toLowerCase())))) {
          newMarkerConfig.abbrevationArr.push(oldMarkerConfigs[j].abbrevation);
          newMarkerConfig.flagArr.push(oldMarkerConfigs[j].flag);
          newMarkerConfig.tooltipArr.push(oldMarkerConfigs[j].tooltip);
          newMarkerConfig.count++;

          // update event arguments
          newMarkerConfig.eventArgs.isCompoundFlag = true;
          newMarkerConfig.eventArgs.numberOfEvents = newMarkerConfig.count;
          newMarkerConfig.eventArgs.eventDescription.push(oldMarkerConfigs[j].description);
          newMarkerConfig.eventArgs.eventCategory.push(oldMarkerConfigs[j].category);
          newMarkerConfig.eventArgs.eventFlag.push(oldMarkerConfigs[j].flag);
        }
      }
      duplicateArr.push(newMarkerDetails);
    }
  }
  createTooltext () {
    let eventMarker = this,
      config = eventMarker.config,
      duplicateMarkerArr = config.duplicateMarkerArr,
      chart = eventMarker.getFromEnv('chart'),
      isUTC = chart.getFromEnv('isUTC'),
      dateFormat = chart.config.dateFormat,
      tooltipStyle = eventMarker.getFromEnv('tooltipStyle'),
      allEvents,
      markerCount,
      tooltipArr,
      formatter = isUTC ? TimeConverter.utcFormatter(dateFormat) : TimeConverter.formatter(dateFormat),
      dateText,
      abbrevationArr,
      tooltext,
      flagArr,
      markers,
      currMarkerConfig,
      categoryLen,
      markerLen,
      i,
      j,
      k;
    for (i = 0, categoryLen = duplicateMarkerArr.length; i < categoryLen; i++) {
      allEvents = duplicateMarkerArr[i];
      markers = allEvents.markers;
      for (j = 0, markerLen = markers.length; j < markerLen; j++) {
        currMarkerConfig = markers[j];
        markerCount = currMarkerConfig.count;
        tooltipArr = currMarkerConfig.tooltipArr;
        dateText = formatter.format(currMarkerConfig.date);
        abbrevationArr = currMarkerConfig.abbrevationArr;
        flagArr = currMarkerConfig.flagArr;
        tooltext = `<div style="max-width:200px;white-space:normal;color:${tooltipStyle.color || COLOR_A2A3B1};font-style:${tooltipStyle['font-style'] || NORMAL}">
        <div style='margin: 5px;font-size: 14px;font-weight: ${tooltipStyle['font-weight'] || BOLD};color:${tooltipStyle.color || COLOR_7F8093}'>${dateText}</div>`;
        if (markerCount === 1) {
          tooltext += `<div style='margin: 5px;font-size: 12px;'>${tooltipArr[0]}</div>
          </div>`;
        } else {
          for (k = 0; k < markerCount; k++) {
            tooltext += `<div style='margin:5px; font-size:10px;'>
          <div style='display:inline;color:${getMappedColor(flagArr[k])}'>&#9609;</div>
          <div style='display:inline;font-weight: ${tooltipStyle['font-weight'] || BOLD};font-size: 13px;'>${abbrevationArr[k]}</div>
          <div style='display:block;font-size: 12px;'>${tooltipArr[k]}</div>
          </div>`;
          }
          tooltext += '</div>';
        }
        currMarkerConfig.toolText = tooltext;
        currMarkerConfig.dateText = dateText;
      }
    }
  }
  draw () {
    let eventMarker = this,
      config = eventMarker.config,
      textColorMap = config.textColorMap,
      flagColorMap = config.flagColorMap,
      chart = eventMarker.getFromEnv('chart'),
      axisTranslation = chart.getChildren('axis')[0].getTranslation(),
      duplicateMarkerArr = config.duplicateMarkerArr,
      markerDimensions = config.markerDimensions = [],
      textStyle = config.textStyle,
      flagStyle = config.flagStyle,
      hoveredCategoryIndex = config.hoveredCategoryIndex,
      hoveredMarkerIndex = config.hoveredMarkerIndex,
      defaultMarkerCosmetics = config.defaultMarkerFlagCosmetics,
      mergedMarkerCosmetics,
      defaultAbbrTextStyle = config.defaultAbbrTextStyle,
      defaultDuplicateTextStyle = config.defaultDuplicateTextStyle,
      smartLabel = eventMarker.getFromEnv('smartLabel'),
      baseTextStyle = eventMarker.getFromEnv('baseTextStyle'),
      abbrTextStyle,
      duplicateTextStyle,
      smartText,
      markerCount,
      type = config.type,
      isGroupCompany = (type === GROUP_COMPANY),
      currMarkerDetail,
      currFlagColor,
      markerX,
      markerY,
      markerColor,
      abbrTextColor,
      companyMap = config.companyMap,
      categoryIndex,
      compName,
      markers,
      scaleX = config.xScale,
      scaleY = config.yScale,
      i,
      j,
      compLen,
      markersLen,
      textDim,
      textYTranslation,
      markerPath,
      width,
      height,
      abbrTextDim,
      laneStartPx,
      laneEndPx;
    mergedMarkerCosmetics = Object.assign({}, defaultMarkerCosmetics, flagStyle);
    abbrTextStyle = Object.assign({}, defaultAbbrTextStyle, baseTextStyle, textStyle);
    // ignore application of line-height
    delete abbrTextStyle['line-height'];
    duplicateTextStyle = Object.assign({}, defaultDuplicateTextStyle, baseTextStyle, textStyle);
    // ignore application of line-height
    delete duplicateTextStyle['line-height'];
    // create all groups
    // for event lanes
    eventMarker.addGraphicalElement({
      el: 'group',
      container: {
        id: 'strato',
        label: 'group',
        isParent: true
      },
      component: eventMarker,
      label: type + 'eventLanes-strato-group',
      attr: {
        name: type + 'eventMarker-strato'
      },
      id: 'strato'
    }, true);
    // for event markers
    eventMarker.addGraphicalElement({
      el: 'group',
      container: {
        id: 'meso',
        label: 'group',
        isParent: true
      },
      component: eventMarker,
      label: type + 'eventMarker-meso-group',
      attr: {
        name: type + 'eventMarker-meso'
      },
      id: 'meso'
    }, true);
    // for hovered flags
    eventMarker.addGraphicalElement({
      el: 'group',
      container: {
        id: 'thermo',
        label: 'group',
        isParent: true
      },
      component: eventMarker,
      label: type + 'eventMarker-thermo-group',
      attr: {
        name: type + 'eventMarker-thermo'
      },
      id: 'thermo'
    }, true);

    // create event lanes for group company events
    isGroupCompany && eventMarker.createLanes();

    for (i = 0, compLen = duplicateMarkerArr.length; i < compLen; i++) {
      currMarkerDetail = duplicateMarkerArr[i];
      compName = currMarkerDetail.compName;
      categoryIndex = companyMap[compName];
      markers = currMarkerDetail.markers;
      markerDimensions[i] = [];
      for (j = 0, markersLen = markers.length; j < markersLen; j++) {
        markerX = scaleX.getRangeValue(markers[j].date);
        markerY = scaleY.getRangeValue(categoryIndex);
        laneStartPx = isGroupCompany ? scaleY.getRangeValue(categoryIndex - 0.5) : scaleY.getRangeValue(categoryIndex + 0.5);
        laneEndPx = isGroupCompany ? scaleY.getRangeValue(categoryIndex + 0.5) : scaleY.getRangeValue(categoryIndex - 0.5);
        height = markerHeight;
        width = height / markerHeight * markerWidth;
        markerPath = getMarkerPath(markerX, markerY, width, height);
        textDim = getTextDim(markerX, markerY);
        markerDimensions[i].push(markerPath);
        markerCount = markers[j].count;
        currFlagColor = (markerCount === 1) ? markers[j].flagArr[0] : NEUTRAL_COLOR;
        markerColor = flagColorMap[currFlagColor.toLowerCase()] || getMappedColor(currFlagColor);
        abbrTextColor = textColorMap[currFlagColor.toLowerCase()] || getMappedColor(currFlagColor);
        // marker flag
        eventMarker.addGraphicalElement({
          el: 'path',
          attr: {
            path: [
              M, markerPath.x1, markerPath.y1,
              L, markerPath.x2, markerPath.y2,
              L, markerPath.x3, markerPath.y3,
              Z
            ],
            fill: markerColor,
            'fill-opacity': mergedMarkerCosmetics['fill-opacity'],
            'stroke-opacity': mergedMarkerCosmetics['stroke-opacity'],
            stroke: markerColor,
            'stroke-width': mergedMarkerCosmetics['stroke-width'],
            'stroke-dasharray': mergedMarkerCosmetics['stroke-dasharray'],
            cursor: 'pointer'
          },
          container: {
            label: type + 'eventMarker-meso-group'
          },
          label: 'eventMarker',
          id: type + '_' + compName + '_marker' + i + j
        });
        // create line and flags on hover
        if (hoveredCategoryIndex === i && hoveredMarkerIndex === j) {
          smartLabel.setStyle({
            fontSize: DEFAULT_FONT_SIZE,
            fontFamily: DEFAULT_FONT_FAMILY
          });
          smartText = smartLabel.getOriSize(markers[j].dateText);
          // crossline
          eventMarker.addGraphicalElement({
            el: 'path',
            attr: {
              path: isGroupCompany ? [
                M, markerPath.x1 + CROSSLINE_STROKE / 2, markerPath.y1,
                L, markerPath.x1 + CROSSLINE_STROKE / 2, axisTranslation.y
              ] : [
                M, markerPath.x3 + CROSSLINE_STROKE / 2, markerPath.y3,
                L, markerPath.x3 + CROSSLINE_STROKE / 2, axisTranslation.y
              ],
              stroke: markerColor,
              'stroke-width': CROSSLINE_STROKE
            },
            container: {
              label: type + 'eventMarker-thermo-group'
            },
            label: 'eventMarkerCrossline',
            id: type + '_' + compName + '_marker_hover_line_' + i + j
          }, true);
          // flag marker
          eventMarker.addGraphicalElement({
            el: 'path',
            attr: {
              path: getFlagPathArr(markerPath.x1, axisTranslation.y, smartText.width, smartText.height, isGroupCompany),
              stroke: COLOR_686980,
              fill: COLOR_C5C5D4,
              'stroke-width': '1',
              'stroke-linejoin': 'round'
            },
            container: {
              label: type + 'eventMarker-thermo-group'
            },
            label: 'eventMarkerHoverFlag',
            id: type + '_' + compName + '_marker_hover_flag_' + i + j
          }, true);
          textYTranslation = smartText.height / 2 + 7;
          // flag marker text
          eventMarker.addGraphicalElement({
            el: 'text',
            attr: {
              x: markerPath.x1,
              y: axisTranslation.y + (isGroupCompany ? -textYTranslation : textYTranslation),
              text: markers[j].dateText,
              fill: COLOR_686980,
              'font-family': DEFAULT_FONT_FAMILY,
              'font-size': DEFAULT_FONT_SIZE,
              'text-anchor': MIDDLE,
              'vertical-align': MIDDLE
            },
            container: {
              label: type + 'eventMarker-thermo-group'
            },
            label: 'eventMarkerHoverText',
            id: type + '_' + compName + '_marker_hover_flag_text_' + i + j
          }, true);
        }
        if (markerCount > 1) {
          // duplication text
          eventMarker.addGraphicalElement({
            el: 'text',
            attr: {
              x: markerPath.x1 + markerWidth / 4,
              y: (markerPath.y1 + markerPath.y3) / 2,
              'text-anchor': duplicateTextStyle['text-anchor'],
              'vertical-align': duplicateTextStyle['vertical-align'],
              'font-family': duplicateTextStyle['font-family'],
              'font-size': duplicateTextStyle['font-size'],
              'fill': COLOR_BLACK,
              'font-weight': duplicateTextStyle['font-weight'],
              'font-style': duplicateTextStyle['font-style'],
              opacity: duplicateTextStyle.opacity,
              text: markerCount,
              cursor: 'pointer'
            },
            container: {
              label: type + 'eventMarker-meso-group'
            },
            label: 'eventMarker',
            id: type + '_' + compName + '_duplication_text' + i + j
          }, true);
        }
        smartLabel.setStyle({
          fontSize: TEN_PX,
          fontFamily: DEFAULT_FONT_FAMILY
        });
        abbrTextDim = smartLabel.getOriSize(markers[j].abbrevationArr[0]);
        abbrTextStyle.fill = markerColor;
        // abbrevation text
        if (markerCount === 1) {
          eventMarker.addGraphicalElement({
            el: 'text',
            attr: {
              x: textDim.x,
              y: textDim.y,
              text: markers[j].abbrevationArr[0],
              'text-anchor': abbrTextStyle['text-anchor'],
              'vertical-align': abbrTextStyle['vertical-align'],
              'font-family': abbrTextStyle['font-family'],
              'font-size': abbrTextStyle['font-size'],
              'font-style': abbrTextStyle['font-style'],
              'fill': abbrTextColor,
              opacity: abbrTextStyle.opacity
            },
            container: {
              label: type + 'eventMarker-meso-group'
            },
            label: 'eventMarker',
            id: type + '_' + compName + '_abbrevation_text' + i + j
          }, true);
        }
      }
    }
  }
  getType () {
    return 'eventMarker';
  }
}
export default EventMarker;
