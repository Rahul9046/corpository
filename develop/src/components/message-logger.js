import { SmartRenderer } from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/component-interface';
import {
  pluckNumber,
  convertColor
} from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/lib';

const CONTENT_WIDTH = 250;

class Messagelogger extends SmartRenderer {
  __setDefaultConfig () {
    super.__setDefaultConfig();
    let config = this.config;
    config._handlers = {};
    config.display = 'none';
    config.content = '';
    config.backgroundCss = {
      'background-color': '#000000'
    };
    config.contentContainerDisplayCss = {
      'background-color': '#ffffff',
      'border': '1px solid #000000',
      'font-family': 'Source Sans Pro',
      'position': 'relative',
      'padding': '5px',
      'box-sizing': 'unset'
    };
    config.contentDisplayCss = {
      'margin-top': '25px',
      'margin-left': '10px',
      'margin-right': '10px',
      'overflow-y': 'scroll',
      'width': '250px',
      'height': '400px'
    };
    config.closeButtonCss = {
      'font-family': 'Source Sans Pro',
      'font-size': '16px',
      'padding': '2px 4px',
      'position': 'absolute',
      'cursor': 'pointer',
      'display': 'inline-block',
      'border-radius': '3px',
      'font-weight': '600',
      'margin-right': '5px',
      'color': '#7f8093',
      'box-sizing': 'unset'
    };
  }
  configureAttributes (obj) {
    let msgLogger = this,
      config = msgLogger.config;
    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        config[key] = obj[key];
      }
    }
    msgLogger.attachEvents();
  }
  attachEvents () {
    let msgLogger = this,
      chart = msgLogger.getFromEnv('chart'),
      chartConfig = chart.config,
      handlers = msgLogger.config._handlers;
    msgLogger.addEventListener('mousedown', handlers.handleMouseDown || (handlers.handleMouseDown = () => {
      chartConfig.disableDetection = false;
      chart.displayMessageLogger({
        display: 'none'
      });
    }));
    document.addEventListener('keydown', handlers.handleKeyDown || (handlers.handleKeyDown = (e) => {
      if (msgLogger.config.display !== 'none') {
        e.preventDefault();
        if (e.keyCode === 27) {
          handlers.handleMouseDown();
        }
      }
    }));
  }
  draw () {
    let msgLogger = this,
      config = msgLogger.config,
      x = config.x,
      y = config.y,
      width = config.width,
      tooltipStyle = msgLogger.getFromEnv('tooltipStyle'),
      mergedContentContainerCSS,
      height = config.height;
    mergedContentContainerCSS = Object.assign({}, config.contentContainerDisplayCss, {
      'background-color': convertColor((tooltipStyle['background-color'] || 'FFF'), pluckNumber(tooltipStyle['background-opacity'] * 100, 100))
    });
    // main container
    msgLogger.addGraphicalElement({
      el: 'html',
      attr: {
        x,
        y,
        width,
        height,
        type: 'div'
      },
      css: {
        display: config.display,
        'user-select': 'none',
        '-ms-user-select': 'none',
        '-moz-user-select': 'none',
        '-webkit-user-select': 'none',
        '-webkit-touch-callout': 'none'
      },
      component: msgLogger,
      label: `msglogger-container_${msgLogger.getId()}`,
      id: `msglogger-container_${msgLogger.getId()}`
    }, true);
    // container background
    msgLogger.addGraphicalElement({
      el: 'html',
      attr: {
        x,
        y,
        width,
        height,
        'opacity': '0.4',
        type: 'div'
      },
      container: {
        label: `msglogger-container_${msgLogger.getId()}`,
        id: `msglogger-container_${msgLogger.getId()}`,
        isParent: false
      },
      css: config.backgroundCss,
      component: msgLogger,
      label: `msglogger-background_${msgLogger.getId()}`,
      id: `msglogger-background_${msgLogger.getId()}`
    }, true);
    // content container
    msgLogger.addGraphicalElement({
      el: 'html',
      attr: {
        x: x + (width - CONTENT_WIDTH) / 2,
        y: y + 0.15 * height,
        width: CONTENT_WIDTH,
        height: 0.7 * height,
        type: 'div'
      },
      container: {
        label: `msglogger-container_${msgLogger.getId()}`,
        id: `msglogger-container_${msgLogger.getId()}`,
        isParent: false
      },
      css: mergedContentContainerCSS,
      component: msgLogger,
      label: `msglogger-content_container_${msgLogger.getId()}`,
      id: `msglogger-content_container_${msgLogger.getId()}`
    }, true);
    // content message
    msgLogger.addGraphicalElement({
      el: 'html',
      attr: {
        type: 'div',
        innerHTML: config.message
      },
      container: {
        label: `msglogger-content_container_${msgLogger.getId()}`,
        id: `msglogger-content_container_${msgLogger.getId()}`,
        isParent: false
      },
      css: config.contentDisplayCss,
      component: msgLogger,
      label: `msglogger-content_${msgLogger.getId()}`,
      id: `msglogger-content_${msgLogger.getId()}`
    }, true);
    // close button
    msgLogger.addGraphicalElement({
      el: 'html',
      attr: {
        type: 'div',
        innerHTML: '&#10005',
        x: x + (width + CONTENT_WIDTH) / 2 - 15,
        y: y + 0.15 * height + 2
      },
      container: {
        label: `msglogger-container_${msgLogger.getId()}`,
        id: `msglogger-container_${msgLogger.getId()}`,
        isParent: false
      },
      css: config.closeButtonCss,
      component: msgLogger,
      label: `msglogger-content_close_${msgLogger.getId()}`,
      id: `msglogger-content_close_${msgLogger.getId()}`
    });
  }
}
export default Messagelogger;
