import { SmartRenderer } from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/component-interface';
import trendlineFactory from '../factories/trendlines.factory';

const MIDDLE = 'middle';
class TrendlineManager extends SmartRenderer {
  constructor () {
    super();
    this.registerFactory('trendline', trendlineFactory);
  }
  __setDefaultConfig () {
    super.__setDefaultConfig();
    let config = this.config;
    config.trendLineDatasets = [];
    config.paletteColors = ['#5AC8FA', '#50E3C2'];
    config.emptyLabelStyle = {
      'font-size': '13px',
      'font-family': 'Source Sans Pro',
      'font-weight': '600',
      fill: '#cfcfdd'
    };
    config.trendLabelStyle = {
      'font-size': '12px',
      'font-family': 'Source Sans Pro',
      'font-weight': 'normal'
    };
  }
  checkPointerOverTrendline (chartX, chartY) {
    let manager = this,
      trendlines = manager.getChildren('trendline') || [],
      currTrendline,
      hoveredInfo,
      i,
      len;
    for (i = 0, len = trendlines.length; i < len; i++) {
      currTrendline = trendlines[i];
      hoveredInfo = currTrendline.checkPointerOverAnchor(chartX, chartY);
      if (hoveredInfo.hovered) {
        hoveredInfo.component = manager;
        return hoveredInfo;
      }
    }
    return {
      hovered: false,
      component: manager
    };
  }
  createTrendLineDatasets () {
    let manager = this,
      config = manager.config,
      chartConfig = manager.getFromEnv('chartConfig'),
      allTrendLineDatas = chartConfig.measureDatas,
      selectedTrendlines = chartConfig.measures;
    config.trendLineDatasets = allTrendLineDatas.filter((measure) => {
      return selectedTrendlines.includes(measure.measureName.toLowerCase()) && measure.measureData.length;
    });
  }
  getMaxLabelSpace () {
    let manager = this,
      config = manager.config,
      chartConfig = manager.getFromEnv('chartConfig'),
      labelStyle = config.trendLabelStyle,
      trendLineDatasets = config.trendLineDatasets,
      smartLabel = manager.getFromEnv('smartLabel'),
      i,
      len,
      maxWidth = 0,
      currLabel;
    smartLabel.setStyle(labelStyle);
    if (chartConfig.showTrendLabels) {
      for (i = 0, len = trendLineDatasets.length; i < len; i++) {
        currLabel = trendLineDatasets[i].measureName;
        maxWidth = Math.max(maxWidth, smartLabel.getOriSize(currLabel).width);
      }
    }
    return maxWidth;
  }
  setScaleDomains () {
    let manager = this,
      config = manager.config,
      trendLineDatasets = config.trendLineDatasets,
      chart = manager.getFromEnv('chart'),
      i,
      len,
      maxValue = -Infinity,
      minValue = Infinity;
    for (i = 0, len = trendLineDatasets.length; i < len; i++) {
      (trendLineDatasets[i].minValue < minValue) && (minValue = trendLineDatasets[i].minValue);
      (trendLineDatasets[i].maxValue > maxValue) && (maxValue = trendLineDatasets[i].maxValue);
    }
    chart.setScaleDomain('trendlineScaleY', [minValue, maxValue]);
  }
  configureAttributes (obj) {
    let manager = this,
      config = manager.config;
    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        if (key === 'paletteColors') {
          if ((typeof obj[key] === 'object') && obj[key].length) {
            config[key] = obj[key];
          }
        } else {
          config[key] = obj[key];
        }
      }
    }
    manager.createTrendLineDatasets();
    manager.setScaleDomains();
    manager.addToEnv('trendLineManagerConfig', manager.config);
  }
  createGroup () {
    let manager = this,
      config = manager.config,
      measureConfig = config.measureConfig,
      i,
      len;

    manager.addGraphicalElement({
      el: 'group',
      container: {
        id: 'meso',
        label: 'group',
        isParent: true
      },
      component: manager,
      label: 'trendlines_meso',
      id: 'trendlines_meso',
      attr: {
        name: 'trendline-manager-meso'
      }
    }, true);

    for (i = 0, len = measureConfig.length; i < len; i++) {
      // line group
      manager.addGraphicalElement({
        el: 'group',
        container: {
          id: 'trendlines_meso',
          label: 'trendlines_meso',
          isParent: false
        },
        component: manager,
        label: 'trendline_meso_line' + i,
        id: 'trendline_meso_line' + i,
        attr: {
          name: 'trendline-measure_line' + i
        }
      }, true);
      // anchor group
      manager.addGraphicalElement({
        el: 'group',
        container: {
          id: 'trendlines_meso',
          label: 'trendlines_meso',
          isParent: false
        },
        component: manager,
        label: 'trendline_meso_anchor' + i,
        id: 'trendline_meso_anchor' + i,
        attr: {
          name: 'trendline-measure_anchor' + i
        }
      }, true);
    }
  }
  draw () {
    let manager = this,
      config = manager.config,
      emptyLabelStyle = config.emptyLabelStyle,
      canvas = manager.getLinkedParent(),
      canvasConfig = canvas.config,
      translateX = canvasConfig._translateX,
      translateY = canvasConfig._translateY,
      measures = config.measureConfig;
    manager.createGroup();
    if (!measures.length) {
      manager.addGraphicalElement({
        el: 'text',
        attr: {
          x: canvasConfig.canvasBGLeft + translateX + canvasConfig.canvasBGWidth / 2,
          y: canvasConfig.canvasBGTop + translateY + canvasConfig.canvasBGHeight / 2,
          text: config.message,
          fill: emptyLabelStyle.fill,
          'font-size': emptyLabelStyle['font-size'],
          'font-family': emptyLabelStyle['font-family'],
          'font-weight': emptyLabelStyle['font-weight'],
          'text-anchor': MIDDLE,
          'vertical-align': MIDDLE
        },
        component: manager,
        container: {
          id: 'trendlines_meso',
          label: 'trendlines_meso',
          isParent: false
        },
        id: 'trendlines_no_measures_text',
        label: 'trendlines_no_measures_text'
      });
    }
  }
  getType () {
    return 'trendlineManager';
  }
}

export default TrendlineManager;
