
import { SmartRenderer } from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/component-interface';
import SRSTool from '../vendors/fusioncharts-xt/develop/src/vendors/fc-timeseries/src/_internal/components/srs-tool';
import { Separator } from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/toolbox/tools';
import { extend2, TRACKER_FILL } from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/lib';
import {
  utcMinute,
  utcHour,
  utcDay,
  utcWeek,
  utcMonth,
  utcYear,
  utcSecond,
  utcQuarter } from '../vendors/fusioncharts-xt/develop/src/vendors/fc-utils/src/time-intervals/utc';
import {
  timeMinute,
  timeHour,
  timeDay,
  timeWeek,
  timeMonth,
  timeYear,
  timeSecond,
  timeQuarter
} from '../vendors/fusioncharts-xt/develop/src/vendors/fc-utils/src/time-intervals';
import SmartToolbar from '../vendors/fusioncharts-xt/develop/src/vendors/fc-timeseries/src/_internal/components/smart-toolbar';

const DURATION_YEAR = 31536000000,
  DURATION_MONTH = 2678400000,
  DURATION_DAY = 86400000,
  DURATION_HOUR = 3600000,
  COLOR_9E9E9E = '#9e9e9e',
  COLOR_DFDFDF = '#dfdfdf',
  DEFAULT_SEPARATOR_HEIGHT = 16,
  standardIntervals = [5 * DURATION_YEAR, 3 * DURATION_YEAR, 2 * DURATION_YEAR, DURATION_YEAR, 6 * DURATION_MONTH, 3 * DURATION_MONTH, DURATION_MONTH,
    15 * DURATION_DAY, 7 * DURATION_DAY, DURATION_DAY, 12 * DURATION_HOUR, 6 * DURATION_HOUR,
    3 * DURATION_HOUR, DURATION_HOUR, DURATION_HOUR / 2],
  intervalMap = {
    0: {
      'unit': 'year',
      'multiplier': 5,
      'intervalName': '5Y'
    },
    1: {
      'unit': 'year',
      'multiplier': 3,
      'intervalName': '3Y'
    },
    2: {
      'unit': 'year',
      'multiplier': 2,
      'intervalName': '2Y'
    },
    3: {
      'unit': 'year',
      'multiplier': 1,
      'intervalName': '1Y',
      'tilldateName': 'YTD'
    },
    4: {
      'unit': 'month',
      'multiplier': 6,
      'intervalName': '6M'
    },
    5: {
      'unit': 'month',
      'multiplier': 3,
      'intervalName': '3M',
      'tilldateName': 'QTD'
    },
    6: {
      'unit': 'month',
      'multiplier': 1,
      'intervalName': '1M',
      'tilldateName': 'MTD'
    },
    7: {
      'unit': 'day',
      'multiplier': 15,
      'intervalName': '15D'
    },
    8: {
      'unit': 'day',
      'multiplier': 7,
      'intervalName': '7D',
      'tilldateName': 'WTD'
    },
    9: {
      'unit': 'day',
      'multiplier': 1,
      'intervalName': '1D'
    },
    10: {
      'unit': 'hour',
      'multiplier': 12,
      'intervalName': '12H'
    },
    11: {
      'unit': 'hour',
      'multiplier': 6,
      'intervalName': '6H'
    },
    12: {
      'unit': 'hour',
      'multiplier': 3,
      'intervalName': '3H'
    },
    13: {
      'unit': 'hour',
      'multiplier': 1,
      'intervalName': '1H'
    },
    14: {
      'unit': 'minute',
      'multiplier': 30,
      'intervalName': '30m'
    }
  },
  // method to ckeck if current interval is valid as per current binning configurations
  isValidInterVal = function (index, isBusinessButton = false, endDomain) {
    let standardRangeSelector = this,
      interval = getInterVal(intervalMap[index].unit, standardRangeSelector.getFromEnv('isUTC')),
      minBinDuration = DURATION_DAY * 5;
    if (!isBusinessButton ? (standardIntervals[index] >= minBinDuration) :
      ((endDomain - interval.every(intervalMap[index].multiplier).floor(endDomain)) >= minBinDuration)) {
      return true;
    }
    return false;
  },
  /**
   * Function to get the time interval
   */
  getInterVal = function (unit, isUTC) {
    switch (unit) {
      case 'year':
        return isUTC ? utcYear : timeYear;
      case 'quarter':
        return isUTC ? utcQuarter : timeQuarter;
      case 'month':
        return isUTC ? utcMonth : timeMonth;
      case 'week':
        return isUTC ? utcWeek : timeWeek;
      case 'day':
        return isUTC ? utcDay : timeDay;
      case 'hour':
        return isUTC ? utcHour : timeHour;
      case 'minute':
        return isUTC ? utcMinute : timeMinute;
      case 'second':
        return isUTC ? utcSecond : timeSecond;
    }
  },
  /**
   * method to find font size considering different states as
   * normal, hover and active as of now.
   * this will return max font size required by an state
   */
  findRelaventFontSize = function (style) {
    let normalFontSize = style.activated.config.normal['font-size'] || 0,
      hoverFontSize = style.activated.config.hover['font-size'] || 0,
      activeFontSize = style.pressed.config.normal['font-size'] || 0;

    return Math.max(normalFontSize, hoverFontSize, activeFontSize);
  };

class StandardRangeSelector extends SmartRenderer {
  constructor () {
    super();
    let standardRangeSelector = this;
    // hanlder function for button click
    this._handler = function () {
      let tool = this,
        chart = tool.getFromEnv('chart'),
        chartConfig = chart.config,
        multiplier = tool.config.multiplier,
        isUTC = tool.getFromEnv('isUTC'),
        unit = tool.config.unit,
        fixedAtEnd = tool.config.fixedAtEnd,
        fixedAtStart = tool.config.fixedAtStart,
        interval,
        startDomain,
        endDomain,
        currentDomain = chartConfig.focusScaleX.getDomain(),
        totalDomain = chartConfig.contextScaleX.getDomain();
      standardRangeSelector.config.clickedButtonDetails = tool.config;

      tool.getFromEnv('animationManager').setAnimationState('selectedRange');
      // if fixed at end then set total domain's end value as end domain
      // else current domain's end value as endDomain
      if (fixedAtEnd) {
        endDomain = totalDomain[1];
      } else {
        endDomain = currentDomain[1];
      }
      // if it is standard interval button or till date button
      if (unit && multiplier) {
        interval = getInterVal(unit, isUTC);
        // if it is a standard interval button
        if (!fixedAtEnd) {
          startDomain = interval.offset(endDomain, -multiplier);
        } else {
          startDomain = interval.every(multiplier).floor(endDomain);
        }
      } else if (fixedAtStart) { // ALL button
        startDomain = totalDomain[0];
      }
      // store the clicked button details
      standardRangeSelector.config.lastSelectedButtonConfig = {
        fixedAtEnd: tool.config.fixedAtEnd,
        fixedAtStart: tool.config.fixedAtStart,
        unit: tool.config.unit,
        multiplier: tool.config.multiplier
      };
      standardRangeSelector.config.updatedThroughButton = true;
      // inform chart to validate domains and draw all respective components.
      chart.setFocusLimit([startDomain, endDomain]);
    };
    this._toolbars = {};
  }
  __setDefaultConfig () {
    super.__setDefaultConfig();
    this.config.lastSelectedButtonConfig = undefined;
    this.config.clickedButtonDetails = {};
    this.config.labelFontSize = '12px';
    // creating a state style object for buttons and background
    this.config.stateStyle = {
      activated: {
        config: {
          normal: {
            fill: COLOR_9E9E9E,
            stroke: 'none',
            'stroke-width': 2,
            cursor: 'pointer',
            'stroke-opacity': 1,
            'fill-opacity': 1,
            'opacity': '1',
            'font-weight': 400,
            'font-size': '14',
            'text-anchor': 'middle'
          },
          hover: {
            fill: '#5648D4',
            stroke: 'none',
            'stroke-width': 2,
            cursor: 'pointer',
            'stroke-opacity': 1,
            'fill-opacity': 1,
            'opacity': '1',
            'font-weight': 400,
            'font-size': '14',
            'text-anchor': 'middle'
          },
          normalBackground: {
            fill: TRACKER_FILL,
            stroke: 'none',
            cursor: 'pointer'
          },
          hoverBackground: {
            fill: TRACKER_FILL,
            stroke: 'none',
            cursor: 'pointer'
          }
        }
      },
      pressed: {
        config: {
          normal: {
            fill: '#5648D4',
            'stroke-width': 2,
            stroke: 'none',
            'symbol-stroke': '#343434',
            cursor: 'pointer',
            'fill-opacity': 1,
            'stroke-opacity': 1,
            'opacity': '1',
            'font-weight': 400,
            'font-size': '14',
            'text-anchor': 'middle'
          },
          hover: {
            fill: '#5648D4',
            'stroke-width': 2,
            stroke: 'none',
            'symbol-stroke': '#5648D4',
            cursor: 'pointer',
            'fill-opacity': 1,
            'stroke-opacity': 1,
            'opacity': '1',
            'font-weight': 400,
            'font-size': '14',
            'text-anchor': 'middle'
          },
          normalBackground: {
            fill: TRACKER_FILL,
            stroke: 'none',
            cursor: 'pointer'
          },
          hoverBackground: {
            fill: TRACKER_FILL,
            stroke: 'none',
            cursor: 'pointer'
          }
        }
      }
    };
  }
  configureAttributes (obj) {
    super.configureAttributes(obj);
    let selfConfig = this.config,
      getStyleDef = this.getFromEnv('getStyleDef'),
      baseTextStyle = this.getFromEnv('baseTextStyle'),
      textStyleNormal,
      textStyleHover,
      textStyleActive,
      backgroundStyleNormal,
      backgroundStyleHover,
      backgroundStyleActive,
      separatorStyle,
      stateStyle = selfConfig.stateStyle;
    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        selfConfig[key] = obj[key];
      }
    }
    // parse user provided styles
    // parse styles for button texts for different states as normal, hovered, active
    textStyleNormal = Object.assign({}, baseTextStyle, getStyleDef(selfConfig.style.button && selfConfig.style.button.text));
    textStyleHover = Object.assign({}, baseTextStyle, getStyleDef(selfConfig.style['button:hover'] && selfConfig.style['button:hover'].text));
    textStyleActive = Object.assign({}, baseTextStyle, getStyleDef(selfConfig.style['button:active'] && selfConfig.style['button:active'].text));

    // parse styles for button backgrounds(rects) for different states as normal, hovered, active
    backgroundStyleNormal = getStyleDef(selfConfig.style.button && selfConfig.style.button.background);
    backgroundStyleHover = getStyleDef(selfConfig.style['button:hover'] && selfConfig.style['button:hover'].background);
    backgroundStyleActive = getStyleDef(selfConfig.style['button:active'] && selfConfig.style['button:active'].background);

    // parse styles for separator line(path)
    separatorStyle = getStyleDef(selfConfig.style.separator && selfConfig.style.separator);

    // update the state style object based on user provided style
    stateStyle.activated.config.normal = extend2(stateStyle.activated.config.normal, textStyleNormal);
    stateStyle.activated.config.hover = extend2(stateStyle.activated.config.hover, textStyleHover);
    stateStyle.pressed.config.normal = extend2(stateStyle.pressed.config.normal, textStyleActive);
    stateStyle.pressed.config.hover = extend2(stateStyle.pressed.config.hover, textStyleHover);

    stateStyle.activated.config.normalBackground = extend2(stateStyle.activated.config.normalBackground, backgroundStyleNormal);
    stateStyle.activated.config.hoverBackground = extend2(stateStyle.activated.config.hoverBackground, backgroundStyleHover);
    stateStyle.pressed.config.normalBackground = extend2(stateStyle.pressed.config.normalBackground, backgroundStyleActive);
    stateStyle.pressed.config.hoverBackground = extend2(stateStyle.pressed.config.hoverBackground, backgroundStyleHover);

    selfConfig.stateStyle.separator = separatorStyle;
  }
  /**
   * Function to get all till date buttons
   * @param {Number} duration Actual data total duration.
   * @return {Array} array of till date buttons
   */
  getTillDateButtons (duration) {
    let tillDateObjArr = [],
      tillDateObj,
      endDomain = this.config.totalDomain[1],
      isUTC = this.getFromEnv('UTC'),
      dateAPI = this.getFromEnv('dateAPI'),
      availableLastDate = new Date(endDomain), // last date in given data
      currentDate = new Date(), // current date
      i,
      len;
    // create business buttons only if data for current date is present
    if ((dateAPI(availableLastDate, 'FullYear', isUTC) === dateAPI(currentDate, 'FullYear', isUTC)) &&
    (dateAPI(availableLastDate, 'Month', isUTC) === dateAPI(currentDate, 'Month', isUTC)) &&
    (dateAPI(availableLastDate, 'Date', isUTC) === dateAPI(currentDate, 'Date', isUTC))) {
      for (i = 0, len = standardIntervals.length; i < len; i++) {
        if (duration >= standardIntervals[i] && intervalMap[i].tilldateName && isValidInterVal.call(this, i, true, endDomain)) {
          tillDateObj = extend2({}, intervalMap[i]);
          tillDateObj.fixedAtEnd = true;
          tillDateObj.fixedAtStart = false;
          tillDateObjArr.push(tillDateObj);
        }
      }
    }
    return tillDateObjArr;
  }
  /**
   * API to higlight a button based on current domain's value and
   * whether the interaction was through button click or invoked by
   * any other component.
   * @return {Object} the highlighted button's configuration
   */
  getSelectionButtonConfig () {
    let standardRangeSelector = this,
      selfConfig = standardRangeSelector.config,
      timeInterValMonth,
      timeInterValYear,
      timeInterValDay,
      isUTC = standardRangeSelector.getFromEnv('isUTC'),
      currentDomain = selfConfig.currentDomain,
      returnObj = {},
      totalDomain = selfConfig.totalDomain,
      currentDomainDiff = +currentDomain[1] - +currentDomain[0];

    timeInterValYear = !isUTC ? timeYear : utcYear;
    timeInterValMonth = !isUTC ? timeMonth : utcMonth;
    timeInterValDay = !isUTC ? timeDay : utcDay;
    // if the interaction was through button click
    if (selfConfig.lastSelectedButtonConfig) {
      returnObj = {
        isTillDate: selfConfig.lastSelectedButtonConfig.fixedAtEnd && !selfConfig.lastSelectedButtonConfig.fixedAtStart,
        isAllButton: selfConfig.lastSelectedButtonConfig.fixedAtEnd && selfConfig.lastSelectedButtonConfig.fixedAtStart,
        isIntervalButton: !selfConfig.lastSelectedButtonConfig.fixedAtEnd && !selfConfig.lastSelectedButtonConfig.fixedAtStart,
        multiplier: selfConfig.lastSelectedButtonConfig.multiplier,
        unit: selfConfig.lastSelectedButtonConfig.unit
      };
      return returnObj;
    } else {
      // checking if the domain values satisfies for ALL button
      if (currentDomainDiff === (+totalDomain[1] - +totalDomain[0])) {
        return {
          isAllButton: true
        };
      }
      // checking if the domain values satisfies for till date buttons
      // YTD
      if (currentDomainDiff === (+currentDomain[1] - +timeInterValYear.floor(currentDomain[1]))) {
        return {
          isTillDate: true,
          multiplier: '1',
          unit: 'year'
        };
      }
      // MTD
      if (currentDomainDiff === (+currentDomain[1] - +timeInterValMonth.floor(currentDomain[1]))) {
        return {
          isTillDate: true,
          multiplier: '1',
          unit: 'month'
        };
      }
      // QTD
      if (currentDomainDiff === (+currentDomain[1] - +timeInterValMonth.every(3).floor(currentDomain[1]))) {
        return {
          isTillDate: true,
          multiplier: '3',
          unit: 'month'
        };
      }
      // WTD
      if (currentDomainDiff === (+currentDomain[1] - +timeInterValDay.every(7).floor(currentDomain[1]))) {
        return {
          isTillDate: true,
          multiplier: '7',
          unit: 'day'
        };
      }
      // standard interval button
      return standardRangeSelector.getHighlightedIntervalButton(currentDomainDiff);
    }
  }

  /**
   * Function to get all stanadard interval buttons
   * @param {Number} currentDomainDuration current domain difference in milliseconds
   * @param {Number} totalDomainDuration total domain difference in milliseconds
   * @return {Array} array of button configurations
   */
  getIntervalButtons (currentDomainDuration, totalDomainDuration) {
    let i, len = standardIntervals.length, intervalArr = [];
    for (i = len - 1; i > 0; i--) {
      if (standardIntervals[i] >= currentDomainDuration) {
        break;
      }
    }
    // if valid duration is given
    if (currentDomainDuration) {
    // greater than or equal to five years
      if (i <= 0) {
        (standardIntervals[0] <= totalDomainDuration) && isValidInterVal.call(this, 0) && intervalArr.push(intervalMap[0]);
        (standardIntervals[1] <= totalDomainDuration) && isValidInterVal.call(this, 1) && intervalArr.push(intervalMap[1]);
      } else if (i === len - 1) {
      // create one level higher interval button if it is less than total range
        (standardIntervals[i - 1] <= totalDomainDuration) && isValidInterVal.call(this, i - 1) && intervalArr.push(intervalMap[i - 1]);
        // equal to 30 minutes
        // consider the least button only if its less than or equal to the total range.
        (standardIntervals[i] <= totalDomainDuration) &&
       isValidInterVal.call(this, i) && intervalArr.push(intervalMap[i]);
      } else { // within five years and 30 minutes
        if (standardIntervals[i - 1] <= totalDomainDuration) {
        // consider the higher button only if its less than or equal to the total range.
          isValidInterVal.call(this, i - 1) && intervalArr.push(intervalMap[i - 1]);
        }
        // add the rest of the two buttons
        (standardIntervals[i] <= totalDomainDuration) && isValidInterVal.call(this, i) && intervalArr.push(intervalMap[i]);
        (standardIntervals[i + 1] <= totalDomainDuration) && isValidInterVal.call(this, i + 1) && intervalArr.push(intervalMap[i + 1]);
      }
    }

    return intervalArr;
  }
  /**
   * Function to get the current highlighted standard button's configuration based on duration
   * @return {Obj} button configuration
   */
  getHighlightedIntervalButton (duration) {
    let srs = this,
      selfConfig = srs.config,
      i,
      intervalButtons = selfConfig.intervalButtons,
      currentDomain = selfConfig.currentDomain,
      isUTC = srs.getFromEnv('isUTC'),
      interval,
      len = intervalButtons.length,
      returnObj = {};
    for (i = len - 1; i >= 0; i--) {
      interval = getInterVal(intervalButtons[i].unit, isUTC);
      if ((+currentDomain[0] === +interval.offset(currentDomain[1], -intervalButtons[i].multiplier)) ||
      (+currentDomain[1] === +interval.offset(currentDomain[0], intervalButtons[i].multiplier))) {
        break;
      }
    }
    if (i !== -1) {
      returnObj = extend2({ isIntervalButton: true }, intervalMap[i]);
      returnObj = {
        isIntervalButton: true,
        unit: intervalButtons[i].unit,
        multiplier: intervalButtons[i].multiplier
      };
      selfConfig.lastSelectedButtonConfig = returnObj;
    }
    return returnObj;
  }
  /**
   * returns the tool requirements of this component
   *
   * @returns {Object} requirements
   * @memberof StandardRangeSelector
   */
  getToolInfo () {
    let srs = this,
      srsConfig = srs.config,
      chart = srs.getFromEnv('chart'),
      toolBar = srs.getFromEnv('selectorToolbar'),
      retObj = {},
      i,
      len,
      state,
      smartLabel = srs.getFromEnv('smartLabel'),
      smartText,
      stateStyleObj = srsConfig.stateStyle,
      labelFontSize = findRelaventFontSize(stateStyleObj) || srsConfig.labelFontSize,
      labelFontFamily = stateStyleObj.activated.config.normal['font-family'],
      labelFontWeight = stateStyleObj.activated.config.normal['font-weight'],
      currentDomain = srsConfig.currentDomain,
      totalDomain = srsConfig.totalDomain,
      selectedButtonConfig,
      currentDomainDiff = +currentDomain[1] - +currentDomain[0],
      totalDomainDiff = +totalDomain[1] - +totalDomain[0],
      intervalToolbarId = `intervalToolBar-${toolBar.getId()}-${chart.getId()}`,
      businessToolbarId = `businessToolBar-${toolBar.getId()}-${chart.getId()}`,
      allToolbarId = `allToolBar-${toolBar.getId()}-${chart.getId()}`,
      intervalButtons = srsConfig.intervalButtons = srs.getIntervalButtons(currentDomainDiff, totalDomainDiff),
      tillDateButtons = srsConfig.tillDateButtons = srs.getTillDateButtons(totalDomainDiff);

    smartLabel.setStyle({
      fontSize: labelFontSize,
      fontFamily: labelFontFamily,
      fontWeight: labelFontWeight
    });

    selectedButtonConfig = srs.getSelectionButtonConfig();

    // create configurations of interval button toolbar
    retObj[intervalToolbarId] = {
      type: 'tool',
      def: SmartToolbar,
      configuration: {
        hAlign: 'left',
        toolbarhdirection: 1,
        child: {}
      }
    };

    // create configurations of interval buttons
    for (i = 0, len = intervalButtons.length; i < len; i++) {
      if (selectedButtonConfig.isIntervalButton && (selectedButtonConfig.unit === intervalButtons[i].unit) &&
        (selectedButtonConfig.multiplier === intervalButtons[i].multiplier)) {
        state = 'pressed';
      } else {
        state = 'activated';
      }
      smartText = smartLabel.getOriSize(intervalButtons[i].intervalName, false);
      retObj[intervalToolbarId].configuration.child[`intervalButton-${toolBar.getId()}-${chart.getId()}-${i}`] = {
        type: 'tool',
        def: SRSTool,
        configuration: {
          text: intervalButtons[i].intervalName,
          name: 'interval',
          width: smartText.width,
          height: smartText.height,
          scale: 1,
          'marginLeft': i ? 5 : 0,
          'marginRight': (i === len - 1) ? 0 : 5,
          'hAlign': 'left',
          'symbolStrokeWidth': '2',
          'hoveredState': 'normal',
          state,
          'multiplier': intervalButtons[i].multiplier,
          'unit': intervalButtons[i].unit,
          'strokeWidth': 0,
          listener: {
            'fc-click': srs._handler
          },
          css: stateStyleObj
        }
      };
    }

    // first separator, create configurations only when interval buttons are created
    if (len) {
      retObj[`separator-${toolBar.getId()}-${chart.getId()}-0`] = {
        type: 'tool',
        def: Separator,
        configuration: {
          marginLeft: 0,
          marginRight: 0,
          scale: 1,
          height: (smartText.height < DEFAULT_SEPARATOR_HEIGHT) ? DEFAULT_SEPARATOR_HEIGHT : smartText.height, // default height for separator as per design is 16px, so if size is less convert it to default value
          width: 10,
          hAlign: 'left',
          stroke: stateStyleObj.separator.stroke || COLOR_DFDFDF,
          css: stateStyleObj.separator
        }
      };
    }

    // create configurations of business toolbar
    retObj[businessToolbarId] = {
      type: 'tool',
      def: SmartToolbar,
      configuration: {
        hAlign: 'left',
        toolbarhdirection: 1,
        child: {}
      }
    };

    // create configurations of business tool buttons
    for (i = 0, len = tillDateButtons.length; i < len; i++) {
    // decide the state of button
      if (selectedButtonConfig.isTillDate && (selectedButtonConfig.unit === tillDateButtons[i].unit) &&
      (selectedButtonConfig.multiplier === tillDateButtons[i].multiplier) && +currentDomain[1] === +totalDomain[1]) { // till date button is not in pressed state if active window end is not same as time nav end.
        state = 'pressed';
      } else {
        state = 'activated';
      }

      smartText = smartLabel.getOriSize(tillDateButtons[i].tilldateName, false);
      retObj[businessToolbarId].configuration.child[`tillDateButton-${toolBar.getId()}-${chart.getId()}-${i}`] = {
        type: 'tool',
        def: SRSTool,
        configuration: {
          text: tillDateButtons[i].tilldateName,
          name: 'interval',
          scale: 1,
          width: smartText.width,
          height: smartText.height,
          state,
          'multiplier': tillDateButtons[i].multiplier,
          'unit': tillDateButtons[i].unit,
          'symbolStrokeWidth': '2',
          'hoveredState': 'normal',
          'fixedAtStart': tillDateButtons[i].fixedAtStart,
          'fixedAtEnd': tillDateButtons[i].fixedAtEnd,
          'marginLeft': i ? 5 : 0,
          'marginRight': (i === len - 1) ? 0 : 5,
          'hAlign': 'left',
          'strokeWidth': 0,
          listener: {
            'fc-click': srs._handler
          },
          css: stateStyleObj
        }
      };
    }

    // create second separator configurations only when business buttons are also shown
    if (len) {
      retObj[`separator-${toolBar.getId()}-${chart.getId()}-1`] = {
        type: 'tool',
        def: Separator,
        configuration: {
          'marginLeft': 0,
          'marginRight': 0,
          scale: 1,
          height: (smartText.height < DEFAULT_SEPARATOR_HEIGHT) ? DEFAULT_SEPARATOR_HEIGHT : smartText.height, // default height for separator as per design is 16px, so if size is less convert it to default value
          classIndex: 3,
          itemIndex: 0,
          width: 10,
          'hAlign': 'left',
          'stroke': stateStyleObj.separator.stroke || COLOR_DFDFDF,
          css: stateStyleObj.separator
        }
      };
    }

    // check for the state of all button
    if (selectedButtonConfig.isAllButton) {
      state = 'pressed';
    } else {
      state = 'activated';
    }

    // create configurations all button toolbar
    retObj[allToolbarId] = {
      type: 'tool',
      def: SmartToolbar,
      configuration: {
        'hAlign': 'left',
        toolbarhdirection: 1,
        child: {}
      }
    };

    smartText = smartLabel.getOriSize('All', false);
    // create 'all' button configuration
    retObj[allToolbarId].configuration.child[`allButton-${toolBar.getId()}-${chart.getId()}-0`] = {
      type: 'tool',
      def: SRSTool,
      configuration: {
        state,
        width: smartText.width,
        height: smartText.height,
        scale: 1,
        text: 'All',
        name: 'interval',
        'marginLeft': 0,
        'marginRight': 0,
        'hAlign': 'left',
        'hoveredState': 'normal',
        'symbolStrokeWidth': '2',
        'strokeWidth': 0,
        'fixedAtStart': true,
        'fixedAtEnd': true,
        fill: '#00ff00',
        labelFill: '#00ff00',
        symbolFill: '#00ff00',
        listener: {
          'fc-click': srs._handler
        },
        css: stateStyleObj
      }
    };

    return retObj;
  }

  /**
   * Api is called when focusLimitChanged event is triggered
   *
   * @memberof StandardRangeSelector
   */
  updateOnLimitChange () {
    let standardRangeSelector = this,
      chartConfig = standardRangeSelector.getFromEnv('chartConfig'),
      currentDomainDiff;
    standardRangeSelector.config.currentDomain = chartConfig.focusScaleX.getDomain();
    currentDomainDiff = standardRangeSelector.config.currentDomain[1] - standardRangeSelector.config.currentDomain[0];
    // if current range is not the same as the last selected range and the update has not happened through
    // button click then reset the last clicked button details
    if (standardRangeSelector.config.lastSelectedRange && (standardRangeSelector.config.lastSelectedRange !== currentDomainDiff) &&
      !standardRangeSelector.config.updatedThroughButton) {
      standardRangeSelector.config.lastSelectedButtonConfig = undefined;
    }
    standardRangeSelector.config.lastSelectedRange = currentDomainDiff;
    standardRangeSelector.config.updatedThroughButton = false;
    // register draw job
    standardRangeSelector.setData({}, true);
  }
}
export default StandardRangeSelector;
