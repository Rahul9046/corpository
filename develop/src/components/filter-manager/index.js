import { SmartRenderer } from '../../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/component-interface';
import dropDownFactory from '../../factories/dropdown.factory';
import { pluckNumber } from '../../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/lib';
// import { TRACKER_FILL } from '../../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/lib';

const PADDING = 10,
  // TOLERANCE = 5,
  COLOR_WHITE = '#ffffff';
  // mathCos = Math.cos,
  // mathSin = Math.sin,
  // PI = Math.PI,
  // resetIconPaths = (x, y, radius) => {
  //   var r = radius,
  //     startX = x - r, startY = y,
  //     endAngle = 3 * PI / 4,
  //     endX = x + r * mathCos(endAngle),
  //     endY = y - r * mathSin(endAngle),
  //     arrowLength = r * 2 / 3;
  //   return {
  //     arcPath: ['M', startX, startY, 'A',
  //       r, r, 0, 1, 0, endX, endY],
  //     _arrowHead: ['M', endX, endY, 'L', endX + 2,
  //       endY - arrowLength - 2, 'M', endX, endY, 'L', endX + arrowLength - 0.5, endY + 4],
  //     arrowHead: ['M', endX + 2, endY - arrowLength - 2, 'L', endX, endY, endX + arrowLength - 0.5, endY + 4]
  //   };
  // };
class FilterManager extends SmartRenderer {
  constructor () {
    super();
    this.registerFactory('dropdownFactory', dropDownFactory);
    this._handlers = {};
  }
  __setDefaultConfig () {
    super.__setDefaultConfig();
    let config = this.config;
    // config.radius = 8;
    config.iconWidth = 52;
    config.iconHeight = 25;
    config._translation = {
      x: 0,
      y: 0
    };
    config._dimension = {
      width: 0,
      height: 0
    };
  }
  attachEvents () {
    let manager = this,
      chartInstance = manager.getFromEnv('chartInstance'),
      jsonData,
      handlers = manager._handlers;
    manager.addEventListener('fc-click', handlers.clickHandler || (handlers.clickHandler = () => {
      jsonData = Object.assign({}, chartInstance.getJSONData());
      chartInstance.setJSONData(jsonData);
    }));
  };
  configureAttributes (obj) {
    let manager = this,
      config = manager.config;
    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        config[key] = obj[key];
      }
    }
    manager.addToEnv('manager', manager);
    manager.attachEvents();
  }
  setTranslation (obj) {
    let config = this.config;
    config._translation.x = obj.x;
    config._translation.y = obj.y;
  }
  setDimension (obj) {
    let config = this.config;
    config._dimension.width = obj.width;
    config._dimension.height = obj.height;
  }
  getTranslation () {
    return this.config._translation;
  }
  getDimension () {
    return this.config._dimension;
  }
  draw () {
    let manager = this,
      config = manager.config,
      buttonStyle = config.buttonStyle,
      textStyle = config.textStyle,
      chartConfig = manager.getFromEnv('chartConfig'),
      translation = manager.getTranslation(),
      // iconRadius = config.radius,
      width = config.iconWidth,
      height = config.iconHeight,
      // resetIconCenterX = (chartConfig.allCanvasLeft + chartConfig.allCanvasWidth) + (width + PADDING) / 2,
      resetIconCenterX = (chartConfig.allCanvasLeft + chartConfig.allCanvasWidth) - (3 * width + 2 * PADDING) / 2,
      resetIconCenterY = translation.y + (height + PADDING) / 2;
      // iconPaths = resetIconPaths(resetIconCenterX, resetIconCenterY, iconRadius);
    // main container
    manager.addGraphicalElement({
      el: 'html',
      attr: {
        x: translation.x,
        y: translation.y,
        type: 'div'
      },
      css: {
        'user-select': 'none',
        '-ms-user-select': 'none',
        '-moz-user-select': 'none',
        '-webkit-user-select': 'none',
        '-webkit-touch-callout': 'none',
        'z-index': '0'
      },
      component: manager,
      label: 'dropdown-container',
      id: 'dropdown-container'
    }, true);
    // reset icon rect
    manager.addGraphicalElement({
      el: 'rect',
      attr: {
        x: resetIconCenterX - width / 2,
        y: resetIconCenterY - height / 2,
        width,
        height,
        fill: buttonStyle.fill || '#686980',
        r: 2,
        stroke: buttonStyle.stroke || 'none',
        'stroke-width': pluckNumber(buttonStyle['stroke-width'], 1),
        'stroke-opacity': pluckNumber(buttonStyle['stroke-opacity'], 1),
        'fill-opacity': pluckNumber(buttonStyle['fill-opacity'], 1),
        cursor: 'pointer'
      },
      component: manager,
      container: {
        id: 'meso',
        label: 'meso',
        isParent: true
      },
      id: 'reset_icon_background',
      label: 'reset_icon_background'
    });
    // reset icon text
    manager.addGraphicalElement({
      el: 'text',
      attr: {
        x: resetIconCenterX,
        y: resetIconCenterY,
        text: 'Reset',
        'fill': textStyle.fill || COLOR_WHITE,
        'font-family': textStyle['font-family'] || 'Source Sans Pro',
        'font-size': '11px',
        'text-anchor': 'middle',
        'font-weight': textStyle['font-weight'] || 'normal',
        'font-style': textStyle['font-style'] || 'normal',
        'fill-opacity': pluckNumber(textStyle['fill-opacity'], 1),
        'vertical-align': 'middle',
        cursor: 'pointer'
      },
      component: manager,
      container: {
        id: 'meso',
        label: 'meso',
        isParent: true
      },
      id: 'reset_icon_text',
      label: 'reset_icon_text'
    });
    // reset icon arrow path
    // manager.addGraphicalElement({
    //   el: 'path',
    //   attr: {
    //     path: iconPaths.arrowHead,
    //     'stroke-width': 3,
    //     'stroke': '#9e9e9e',
    //     fill: 'none'
    //   },
    //   component: manager,
    //   container: {
    //     id: 'meso',
    //     label: 'meso',
    //     isParent: true
    //   },
    //   id: 'reset_icon_arrow',
    //   label: 'reset_icon_arrow'
    // }, true);
    // reset icon background
  }
}
export default FilterManager;
