import { SmartRenderer } from '../../../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/component-interface';
import headerFactory from '../../../factories/dropdown-header.factory';
import contentboxFactory from '../../../factories/dropdown-content-box.factory';
import applyFactory from '../../../factories/dropdown-apply.factory';

const UNDEF = undefined,
  APPLY_BUTTON_CLICK = 'ApplyButtonClick';
class Dropdown extends SmartRenderer {
  constructor () {
    super();
    this.registerFactory('dropdownHeaderFactory', headerFactory);
    this.registerFactory('dropdownContentBoxFactory', contentboxFactory);
    this.registerFactory('dropdownApplyFactory', applyFactory);
  }
  __setDefaultConfig () {
    super.__setDefaultConfig();
    let config = this.config;
    config.contentVisibility = 'none';
    config.currSelection = UNDEF;
    config.currHovered = UNDEF;
    config.hideContent = true;
    config.selectCount = 0;
    config.redrawSelection = UNDEF;
    config.selectionArr = [];
    config.defaultHeaderContainerCss = {
      'display': 'inline-block',
      'font-family': 'Source Sans Pro',
      'position': 'relative',
      'user-select': 'none',
      'background-color': '#ffffff',
      'float': 'left',
      'margin': '0px 8px 0px 0px',
      'border': 'none',
      'box-sizing': 'border-box',
      'box-shadow': 'none',
      'z-index': '0'
    };
  }
  handleUpdate () {
    let dropDown = this,
      config = dropDown.config,
      filterType = config.dropDownInfo.type,
      selectedArr = config.selectionArr,
      chart = dropDown.getFromEnv('chart');
    chart.config[filterType] = selectedArr;
    chart.fireChartInstanceEvent(APPLY_BUTTON_CLICK, {
      filterName: config.dropDownInfo.header,
      selectedItems: config.dropDownInfo.items.filter((item) => {
        return selectedArr.includes(item.contentName.toLowerCase());
      }).map(item => item.contentName)
    });
    chart.redrawCanvasComponents();
  }
  manageSelection () {
    let dropDown = this,
      config = dropDown.config,
      selectedArr = config.selectionArr,
      currSelection = config.currSelection,
      maxSelect = config.dropDownInfo.maxSelect;
    if (currSelection) {
      // selected for the second time
      if (selectedArr.includes(currSelection)) {
        selectedArr = config.selectionArr = selectedArr.filter(item => item !== currSelection);
        config.selectCount--;
      } else if (config.selectCount < maxSelect) { // new selection
        selectedArr.push(currSelection);
        config.selectCount++;
      } else { // new selection over max selection
        // store content's name for its redraw
        config.redrawSelection = selectedArr.shift();
        selectedArr.push(currSelection);
      }
    }
    config.currSelection = UNDEF;
  }
  configureAttributes (obj) {
    let dropDown = this,
      config = dropDown.config;
    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        config[key] = obj[key];
      }
    }
    dropDown.manageSelection();
    dropDown.addToEnv('dropDown', dropDown);
  }
  draw () {
    let dropDown = this,
      config = dropDown.config,
      visibleCss = {};
    if (config.contentVisibility === 'block') {
      visibleCss = {
        border: '1px solid #cfcfdd',
        'border-radius': '4px',
        'box-shadow': '2px 2px 3px rgba(64,64,64,0.1)'
      };
    }
    // main container
    dropDown.addGraphicalElement({
      el: 'html',
      attr: {
        type: 'div',
        width: config.width
      },
      container: {
        label: 'dropdown-container',
        id: 'dropdown-container',
        isParent: true
      },
      css: Object.assign({}, config.defaultHeaderContainerCss, visibleCss),
      component: dropDown,
      label: `main_container_${dropDown.getId()}`,
      id: `main_container_${dropDown.getId()}`
    });
  }
}
export default Dropdown;
