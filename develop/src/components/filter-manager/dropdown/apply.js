import { SmartRenderer } from '../../../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/component-interface';

class Apply extends SmartRenderer {
  constructor () {
    super();
    this._handlers = {};
  }
  __setDefaultConfig () {
    super.__setDefaultConfig();
    let config = this.config;
    config.defaultApplyButtonCss = {
      'background-color': '#686980',
      'color': '#ffffff',
      'font-weight': 'bold',
      'cursor': 'pointer',
      'text-align': 'center',
      'position': 'relative',
      'font-size': '11px',
      'padding': '3.5px 0px',
      'display': 'none'
    };
  }
  configureAttributes (obj = {}) {
    let config = this.config;
    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        config[key] = obj[key];
      }
    }
    this.attachHandlers();
  }
  attachHandlers () {
    let apply = this,
      handlers = apply._handlers,
      dropDown = apply.getFromEnv('dropDown');

    apply.addEventListener('mousedown', handlers.clickHandler || (handlers.clickHandler = () => {
      dropDown.handleUpdate();
    }));
  }
  draw () {
    let apply = this,
      config = apply.config,
      display = config.display,
      dropDown = apply.getFromEnv('dropDown');
    // apply button
    apply.addGraphicalElement({
      el: 'html',
      attr: {
        type: 'div',
        innerHTML: 'APPLY'
      },
      container: {
        label: `main_container_${dropDown.getId()}`,
        id: `main_container_${dropDown.getId()}`,
        isParent: true
      },
      css: Object.assign({}, config.defaultApplyButtonCss, {display}),
      component: apply,
      label: `dropdown_apply_${apply.getId()}`,
      id: `dropdown_apply_${apply.getId()}`
    });
  }
}
export default Apply;
