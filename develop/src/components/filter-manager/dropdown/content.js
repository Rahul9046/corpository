import { SmartRenderer } from '../../../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/component-interface';
const supportedStyles = ['background-color', 'font-family', 'font-weight', 'font-style', 'color'],
  UNDEF = undefined;
class Content extends SmartRenderer {
  constructor () {
    super();
    this._handlers = {};
  }
  __setDefaultConfig () {
    super.__setDefaultConfig();
    let config = this.config;
    config.hovered = false;
    config.selected = false;
    config.defaultDropdownContentBoxCss = {
      'border-bottom': '1px solid #f2f2f2',
      'border-top': 'none',
      'cursor': 'pointer',
      'color': '#8D8686',
      'padding': '7px',
      'position': 'relative',
      'font-size': '12px',
      'white-space': 'pre-wrap',
      'word-wrap': 'break-word',
      'font-weight': 'normal',
      'background-color': '#ffffff',
      'box-sizing': 'border-box'
    };
    config.hoverInCss = {
      'background-color': '#f2f2f2'
    };
    config.hoverOutCss = {
      'background-color': '#ffffff'
    };
    config.selectCss = {
      'font-weight': '600',
      'color': '#0d0d0d',
      'background-color': '#f2f2f2'
    };
  }
  attachHandlers () {
    let dropDownContent = this,
      config = dropDownContent.config,
      handlers = dropDownContent._handlers,
      dropDown = dropDownContent.getFromEnv('dropDown'),
      contentBox = dropDownContent.getLinkedParent(),
      redrawContent,
      dropDownConfig = dropDown.config;

    dropDownContent.addEventListener('mousedown', handlers.clickHandler || (handlers.clickHandler = () => {
      // set the flag to stop document's handler from hiding the contents.
      dropDownConfig.hideContent = false;
      dropDownConfig.currSelection = config.contentName.toLowerCase();
      dropDown.manageSelection();
      // check if any redraw is needed for other contents in case max selection is needed
      if (dropDownConfig.redrawSelection) {
        redrawContent = contentBox.getChildren('content')
          .find(content => content.config.contentName.toLowerCase() === dropDownConfig.redrawSelection);
        redrawContent && redrawContent.setData({}, true);
        dropDownConfig.redrawSelection = undefined;
      }
      dropDownContent.setData({}, true);
    }));
    dropDownContent.addEventListener('mouseover', handlers.hoverInHandler || (handlers.hoverInHandler = () => {
      dropDownConfig.currHovered = config.contentName;
      dropDownContent.setData({
        hovered: true
      }, true);
    }));
    dropDownContent.addEventListener('mouseout', handlers.hoverOutHandler || (handlers.hoverOutHandler = () => {
      dropDownConfig.currHovered = UNDEF;
      dropDownContent.setData({
        hovered: false
      }, true);
    }));
  }
  parseStyle () {
    let dropDownContent = this,
      config = dropDownContent.config,
      extContentStyle = config.extContentStyle,
      contentStyle;
    contentStyle = config.contentStyle = {};
    for (let key in extContentStyle) {
      if (extContentStyle.hasOwnProperty(key) && extContentStyle[key] && supportedStyles.includes(key)) {
        contentStyle[key] = extContentStyle[key];
      }
    }
  }
  configureAttributes (obj) {
    let config = this.config;
    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        config[key] = obj[key];
      }
    }
    this.parseStyle();
    this.attachHandlers();
  }
  draw () {
    let dropDownContent = this,
      config = dropDownContent.config,
      contentName = config.contentName,
      selectBoxCss,
      hoverCss,
      contentStyle = config.contentStyle,
      dropDown = dropDownContent.getFromEnv('dropDown'),
      baseTextStyle = dropDownContent.getFromEnv('baseTextStyle'),
      dropDownConfig = dropDown.config,
      textStyle,
      selectionArr = dropDownConfig.selectionArr;
    selectBoxCss = selectionArr.includes(contentName.toLowerCase()) ? config.selectCss : {};
    hoverCss = config.hovered ? config.hoverInCss : {};
    textStyle = Object.assign({}, config.defaultDropdownContentBoxCss, baseTextStyle, contentStyle, hoverCss, selectBoxCss);
    // ignore application of line-height
    delete textStyle['line-height'];
    // dropdown content div
    dropDownContent.addGraphicalElement({
      el: 'html',
      attr: {
        type: 'div',
        innerHTML: contentName
      },
      container: {
        label: `dropdown_sub_content_box_${dropDownContent.getId()}`,
        id: `dropdown_sub_content_box_${dropDownContent.getId()}`,
        isParent: true
      },
      css: textStyle,
      component: dropDownContent,
      label: `dropdown_content_div_${dropDownContent.getId()}`,
      id: `dropdown_content_div_${dropDownContent.getId()}`
    });
  }
}
export default Content;
