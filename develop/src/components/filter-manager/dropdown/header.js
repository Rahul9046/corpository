import { SmartRenderer } from '../../../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/component-interface';
const supportedStyles = ['background-color', 'font-family', 'font-weight', 'font-style', 'color'];
class Header extends SmartRenderer {
  constructor () {
    super();
    this._handlers = {};
  }
  __setDefaultConfig () {
    super.__setDefaultConfig();
    let config = this.config;
    config.defaultHeaderNameContainerCss = {
      border: '1px solid #cfcfdd',
      'border-radius': '4px',
      'position': 'relative',
      'cursor': 'pointer',
      'z-index': '0'
    };
    config.defaultHeaderNameCss = {
      'display': 'inline-block',
      'margin': '8px 10px',
      'color': '#686980',
      'font-size': '12px',
      'position': 'relative',
      'white-space': 'nowrap',
      'overflow': 'hidden',
      'text-overflow': 'ellipsis',
      'max-width': '70%',
      'z-index': '0'
    };
    config.defaultDownArrowCss = {
      'background-image': 'url(data:image/svg+xml;base64,ICAgICAgICAgICA8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IgoJIHdpZHRoPSIxNXB4IiBoZWlnaHQ9IjE1cHgiPgo8Zz4KCTxwYXRoIGQ9Ik00LDUgTDExLDUgTDcuNSw5IFoiIGZpbGw9IiM0ODQ4NTQiLz4KPC9nPgo8L3N2Zz4=)',
      'background-repeat': 'no-repeat',
      'background-position': 'center',
      'width': '15px',
      'height': '15px',
      'top': '8px',
      'right': '10px',
      'left': 'auto',
      'position': 'absolute',
      'z-index': '0'
    };
  }
  attachHandlers () {
    let header = this,
      handlers = header._handlers,
      parent = header.getLinkedParent(),
      parentConfig = parent.config,
      manager = header.getFromEnv('manager');
    header.addEventListener('mousedown', handlers.clickHandler || (handlers.clickHandler = () => {
      // set the flag to stop document's handler from hiding the contents only if its not initially visible.
      if (parentConfig.contentVisibility === 'none') {
        parentConfig.hideContent = false;
        parentConfig.contentVisibility = 'block';
        manager.setData({}, true);
      }
    }));
    document.addEventListener('mousedown', handlers.documentClickHandler || (handlers.documentClickHandler = (e) => {
      // check if the event bubbled via header or contents.
      if (parentConfig.hideContent && parentConfig.contentVisibility === 'block' &&
      (e.target.id !== 'dropdownContainer')) {
        parentConfig.contentVisibility = 'none';
        manager.setData({}, true);
      }
      // reset the flag
      parentConfig.hideContent = true;
    }));
  }
  parseStyle () {
    let header = this,
      config = header.config,
      extHeaderStyle = config.extHeaderStyle,
      headerStyle;
    headerStyle = config.headerStyle = {
      text: {},
      background: {}
    };
    for (let key in extHeaderStyle) {
      if (extHeaderStyle.hasOwnProperty(key) && extHeaderStyle[key] && supportedStyles.includes(key)) {
        if (key !== 'background-color') {
          headerStyle.text[key] = extHeaderStyle[key];
        } else {
          headerStyle.background[key] = extHeaderStyle[key];
        }
      }
    }
  }
  configureAttributes (obj) {
    let config = this.config;
    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        config[key] = obj[key];
      }
    }
    this.parseStyle();
    this.attachHandlers();
  }
  draw () {
    let header = this,
      config = header.config,
      headerStyle = config.headerStyle,
      dropDown = header.getFromEnv('dropDown'),
      showContentCss = {},
      baseTextStyle = header.getFromEnv('baseTextStyle'),
      textStyle = Object.assign({}, config.defaultHeaderNameCss, baseTextStyle, headerStyle.text),
      headerName = config.headerName;
    // ignore application of line-height
    delete textStyle['line-height'];
    if (dropDown.config.contentVisibility === 'block') {
      showContentCss = {
        'border': 'none',
        'border-bottom': '1px solid #f2f2f2',
        'border-radius': '0px'
      };
    }
    // header name container
    header.addGraphicalElement({
      el: 'html',
      attr: {
        type: 'div'
      },
      container: {
        label: `main_container_${dropDown.getId()}`,
        id: `main_container_${dropDown.getId()}`,
        isParent: true
      },
      css: Object.assign({}, config.defaultHeaderNameContainerCss, showContentCss, headerStyle.background),
      component: header,
      label: `dropdown_header_name_container_${dropDown.getId()}`,
      id: `dropdown_header_name_container_${dropDown.getId()}`
    });

    // header name
    header.addGraphicalElement({
      el: 'html',
      attr: {
        type: 'span',
        'innerHTML': headerName,
        opacity: textStyle.opacity
      },
      container: {
        label: `dropdown_header_name_container_${dropDown.getId()}`,
        id: `dropdown_header_name_container_${dropDown.getId()}`
      },
      css: textStyle,
      component: header,
      label: `dropdown_header_name_${dropDown.getId()}`,
      id: `dropdown_header_name_${dropDown.getId()}`
    }, true);

    // down arrow
    header.addGraphicalElement({
      el: 'html',
      attr: {
        type: 'div',
        'visibility': config.contentVisibility
      },
      container: {
        label: `dropdown_header_name_container_${dropDown.getId()}`,
        id: `dropdown_header_name_container_${dropDown.getId()}`
      },
      css: config.defaultDownArrowCss,
      component: header,
      label: `dropdown_header_name_arrow_${dropDown.getId()}`,
      id: `dropdown_header_name_arrow_${dropDown.getId()}`
    }, true);
  }
}
export default Header;
