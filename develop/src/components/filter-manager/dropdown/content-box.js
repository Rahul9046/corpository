import { SmartRenderer } from '../../../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/component-interface';
import contentFactory from '../../../factories/dropdown-content.factory';

class ContentBox extends SmartRenderer {
  constructor () {
    super();
    this.registerFactory('contentFactory', contentFactory);
  }
  __setDefaultConfig () {
    super.__setDefaultConfig();
    let config = this.config;
    config.defaultDropdownMainContentBoxCss = {
      'max-height': '120px',
      'overflow-y': 'scroll',
      'position': 'relative',
      'display': 'none',
      'width': '100%'
    };
    config.defaultDropdownSubContentBoxCss = {
      'position': 'relative',
      'width': '100%'
    };
  }
  configureAttributes (obj) {
    let config = this.config;
    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        config[key] = obj[key];
      }
    }
  }
  draw () {
    let dropDownContentBox = this,
      config = dropDownContentBox.config,
      dropDown = dropDownContentBox.getFromEnv('dropDown'),
      contents = dropDownContentBox.getChildren('content'),
      i,
      len,
      contentVisibility = config.contentVisibility;
    // dropdown main content box
    dropDownContentBox.addGraphicalElement({
      el: 'html',
      attr: {
        type: 'div',
        id: 'dropdownContainer'
      },
      container: {
        label: `main_container_${dropDown.getId()}`,
        id: `main_container_${dropDown.getId()}`,
        isParent: true
      },
      css: Object.assign({}, config.defaultDropdownMainContentBoxCss, { display: contentVisibility }),
      component: dropDownContentBox,
      label: `dropdown_main_content_box_${dropDown.getId()}`,
      id: `dropdown_main_ content_box_${dropDown.getId()}`
    });
    for (i = 0, len = contents.length; i < len; i++) {
      // dropdown sub content box
      dropDownContentBox.addGraphicalElement({
        el: 'html',
        attr: {
          type: 'div'
        },
        container: {
          label: `dropdown_main_content_box_${dropDown.getId()}`,
          id: `dropdown_main_ content_box_${dropDown.getId()}`
        },
        component: dropDownContentBox,
        css: config.defaultDropdownSubContentBoxCss,
        label: `dropdown_sub_content_box_${contents[i].getId()}`,
        id: `dropdown_sub_content_box_${contents[i].getId()}`
      });
    }
  }
}
export default ContentBox;
