import { SmartRenderer } from '../../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/component-interface';
import CustomRangeSelectorTool from './selector-tool';

class CustomRangeSelector extends SmartRenderer {
  /**
   * configures the component
   *
   * @param {any} [inputConfig={}] configuration
   * @memberof CustomRangeSelector
   */
  configureAttributes (inputConfig = {}) {
    let selector = this,
      selectorConfig = selector.config,
      key;

    for (key in inputConfig) {
      selectorConfig[key] = inputConfig[key];
    }
  }

  /**
   * Called when the event 'focusLimitChanged' is triggered
   *
   * @memberof CustomRangeSelector
   */
  updateOnLimitChange () {
    this.setData({
      domain: this.getFromEnv('chartConfig').focusScaleX.getDomain()
    }, true);
  }

  /**
   * returns the tool requirements of this components
   *
   * @returns {Object} requirements
   * @memberof CustomRangeSelector
   */
  getToolInfo () {
    let styleObj = Object.assign({}, this.config.style);

    styleObj['title:hoverout'] = styleObj.title;
    return {
      'selector-0': {
        type: 'tool',
        def: CustomRangeSelectorTool,
        configuration: {
          domain: this.config.domain,
          hAlign: 'right',
          scale: 1,
          marginTop: 2,
          marginBottom: 2,
          marginLeft: 2,
          marginRight: 2,
          extStyle: styleObj
        }
      }
    };
  }
}

export default CustomRangeSelector;
