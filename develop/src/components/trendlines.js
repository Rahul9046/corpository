import { SmartRenderer } from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/component-interface';
import TimeConverter from '../vendors/fusioncharts-xt/develop/src/vendors/fc-utils/src/time-converter';

const COLOR_WHITE = '#ffffff',
  COLOR_A2A3B1 = '#a2a3b1',
  COLOR_7F8093 = '#7f8093',
  M = 'M',
  L = 'L',
  MIDDLE = 'middle',
  LABELHPADDING = 10,
  START = 'start',
  UNDEF = undefined,
  createDataCoordinates = (dataArr, xScale, yScale) => {
    let coordArr = [],
      i,
      len;
    for (i = 0, len = dataArr.length; i < len; i++) {
      coordArr.push({
        x: xScale.getRangeValue(dataArr[i].date),
        y: yScale.getRangeValue(dataArr[i].value)
      });
    }
    return coordArr;
  },
  getPathArr = (coordArr) => {
    let i,
      pathArr = [],
      currCoord,
      len;
    for (i = 0, len = coordArr.length; i < len; i++) {
      currCoord = coordArr[i];
      if (!i) {
        pathArr.push(M, currCoord.x, currCoord.y);
      } else {
        pathArr.push(L, currCoord.x, currCoord.y);
      }
    }
    return pathArr;
  },
  isWithinAnchor = (chartX, chartY, anchorX, anchorY, radius) => {
    let dx, dy, diff;
    dx = (chartX - anchorX);
    dy = (chartY - anchorY);
    // Mathematical function to find a point inside a cirlce of given radius
    diff = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
    return diff <= radius;
  };

class Trendline extends SmartRenderer {
  __setDefaultConfig () {
    super.__setDefaultConfig();
    let config = this.config;
    config.anchorRadius = 3;
  }
  configureAttributes (obj) {
    let trendline = this,
      config = trendline.config;
    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        config[key] = obj[key];
      }
    }
    trendline.createTooltipConfiguration();
  }
  createTooltipConfiguration () {
    let trendline = this,
      config = trendline.config,
      numberFormatter = trendline.getFromEnv('number-formatter'),
      datasetInfo = config.datasetInfo,
      managerConfig = trendline.getFromEnv('trendLineManagerConfig'),
      measureFormat = managerConfig.measureFormat,
      measureData = datasetInfo.measureData,
      measureName,
      data,
      chart = trendline.getFromEnv('chart'),
      isUTC = chart.getFromEnv('isUTC'),
      dateFormat = chart.config.dateFormat,
      formatter = isUTC ? TimeConverter.utcFormatter(dateFormat) : TimeConverter.formatter(dateFormat),
      i,
      tooltext,
      dateText,
      formattedDataValue,
      extFormat,
      len;
    for (i = 0, len = measureData.length; i < len; i++) {
      data = measureData[i];
      measureName = datasetInfo.measureName;

      dateText = formatter.format(data.date);
      if ((extFormat = measureFormat[measureName.toLowerCase()]) !== UNDEF) {
        if (typeof extFormat === 'function') {
          formattedDataValue = extFormat(data.value);
        } else if (typeof extFormat === 'string') {
          formattedDataValue = data.value + extFormat;
        } else {
          formattedDataValue = '';
        }
      } else {
        formattedDataValue = numberFormatter.formatValue(data.value);
      }
      tooltext = `<div style="max-width:200px;white-space:normal;color:${COLOR_A2A3B1}">
        <div style='margin: 5px;font-size: 14px;font-weight: bold;color:${COLOR_7F8093}'>${dateText}</div>`;
      tooltext += `<div style='margin: 5px;font-size: 12px;'>${datasetInfo.measureName}</div>
          </div>`;
      tooltext += `<div style='margin: 5px;font-size: 12px;'>Value: ${formattedDataValue}</div>
      </div>`;
      data.tooltext = tooltext;
    }
  }
  checkPointerOverAnchor (chartX, chartY) {
    let trendline = this,
      config = trendline.config,
      xScale = config.xScale,
      yScale = config.yScale,
      datasetInfo = config.datasetInfo,
      measureData = datasetInfo.measureData,
      anchorX,
      anchorY,
      currData,
      hovered,
      i,
      len = measureData.length;
    for (i = len - 1; i >= 0; i--) {
      currData = measureData[i];
      anchorX = xScale.getRangeValue(currData.date);
      anchorY = yScale.getRangeValue(currData.value);
      hovered = isWithinAnchor(chartX, chartY, anchorX, anchorY, config.anchorRadius);
      if (hovered) {
        return {
          hovered: true,
          hoveredData: currData,
          measureName: datasetInfo.measureName
        };
      }
    }
    return {
      hovered: false
    };
  }
  draw () {
    let trendline = this,
      config = trendline.config,
      datasetInfo = config.datasetInfo,
      trendLabelStyle = config.trendLabelStyle,
      showTrendLabel = config.showTrendLabel,
      dataArr = datasetInfo.measureData,
      anchorRadius = config.anchorRadius,
      index = config.index,
      xScale = config.xScale,
      yScale = config.yScale,
      managerConfig = trendline.getFromEnv('trendLineManagerConfig'),
      paletteColors = managerConfig.paletteColors,
      paletteColorsLen = paletteColors.length,
      coordArr,
      dataLength = dataArr.length,
      pathArr,
      i,
      len;
    coordArr = createDataCoordinates(dataArr, xScale, yScale);
    pathArr = getPathArr(coordArr);

    // create trendline path
    trendline.addGraphicalElement({
      el: 'path',
      attr: {
        path: pathArr
      },
      container: {
        id: 'trendline_meso_line' + index,
        label: 'trendline_meso_line' + index,
        isParent: true
      },
      css: {
        fill: 'none',
        stroke: paletteColors[index % paletteColorsLen],
        'stroke-width': 2
      },
      component: trendline,
      id: `trendline_path_${index}`,
      label: 'trendlinePath'
    });
    // create trendline label
    if (showTrendLabel) {
      trendline.addGraphicalElement({
        el: 'text',
        attr: {
          x: coordArr[dataLength - 1].x + LABELHPADDING,
          y: coordArr[dataLength - 1].y,
          text: datasetInfo.measureName,
          'text-anchor': START,
          'vertical-align': MIDDLE,
          fill: paletteColors[index % paletteColorsLen]
        },
        container: {
          id: 'trendline_meso_line' + index,
          label: 'trendline_meso_line' + index,
          isParent: true
        },
        css: trendLabelStyle,
        component: trendline,
        id: `trendline_label_${index}`,
        label: 'trendlineLabel'
      });
    }

    //  create anchors
    for (i = 0, len = dataArr.length; i < len; i++) {
      trendline.addGraphicalElement({
        el: 'circle',
        attr: {
          cx: coordArr[i].x,
          cy: coordArr[i].y,
          r: anchorRadius
        },
        container: {
          id: 'trendline_meso_anchor' + index,
          label: 'trendline_meso_anchor' + index,
          isParent: true
        },
        css: {
          fill: COLOR_WHITE,
          stroke: paletteColors[index % paletteColorsLen],
          'stroke-width': 1
        },
        component: trendline,
        id: `trendline_anchor_${index}_${i}`,
        label: 'trendlineAnchor'
      });
    }
  }
}

export default Trendline;
