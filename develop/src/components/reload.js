import { SmartRenderer } from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/component-interface';
import { pluckNumber } from '../vendors/fusioncharts-xt/develop/src/vendors/fc-core/src/lib';
import 'whatwg-fetch';

// const mathCos = Math.cos,
//   mathSin = Math.sin,
//   PI = Math.PI,
const COLOR_BLACK = '#000000',
  COLOR_WHITE = '#ffffff',
  RELOAD_BUTTON_CLICK = 'ReloadButtonClick',
  DATALOAD_START = 'DataLoadStart',
  DATALOAD_SUCCESS = 'DataLoadSuccess',
  DATALOAD_FAIL = 'DataLoadFail',
  MIDDLE = 'middle';
  // reloadIcon = (x, y, radius) => {
  //   var r = radius,
  //     startX = x - r, startY = y,
  //     endAngle = (PI / 2 + PI) / 2,
  //     endX = x + r * mathCos(endAngle),
  //     endY = y + r * mathSin(endAngle),
  //     arrowLength = r * 2 / 3,

//     paths = ['M', startX, startY, 'A',
//       r, r, 0, 1, 1, endX, endY, 'L', endX + arrowLength,
//       endY - 1, endX + 2, endY + arrowLength - 0.5, endX, endY];

//   return paths;
// };
class Reload extends SmartRenderer {
  constructor () {
    super();
    this._translation = {};
    this._handlers = {};
  }
  __setDefaultConfig () {
    super.__setDefaultConfig();
    let config = this.config;
    config.width = 56;
    config.height = 25;
    config.showLoader = 0;
  }
  attachHandlers () {
    let reload = this,
      config = reload.config,
      dataURL = config.dataURL,
      chart = reload.getFromEnv('chart'),
      dataTable = chart.config.dataTable,
      schema = dataTable.getData().schema,
      dataStore = dataTable.getDataStore(),
      DataStoreDef,
      newDataStore,
      chartInstance = chart.getFromEnv('chartInstance'),
      jsonData = Object.assign({}, chartInstance.getJSONData()),
      handlers = reload._handlers;
    reload.addEventListener('fc-click', handlers.clickHandler || (handlers.clickHandler = () => {
      chart.fireChartInstanceEvent(RELOAD_BUTTON_CLICK, {});
      if (dataURL) {
        chart.fireChartInstanceEvent(DATALOAD_START, {});
        reload.setData({
          showLoader: 1
        }, true);
        fetch(dataURL)
          .then(function (response) {
            if (response.ok) {
              return response.json();
            }
          })
          .then(function (dataJSON) {
            if (dataJSON) {
              chart.fireChartInstanceEvent(DATALOAD_SUCCESS, {});
              DataStoreDef = dataStore.constructor;
              newDataStore = new DataStoreDef(dataJSON, schema);
              jsonData.data = newDataStore.getDataTable();
              chartInstance.setJSONData(jsonData);
            } else {
              chart.fireChartInstanceEvent(DATALOAD_FAIL, {});
              chartInstance.setJSONData(jsonData);
            }
          })
          // eslint-disable-next-line
          .catch(function(error) {
            chart.fireChartInstanceEvent(DATALOAD_FAIL, {});
            chartInstance.setJSONData(jsonData);
          });
      }
    }));
  }
  configureAttributes (obj) {
    let reload = this,
      config = reload.config;
    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        config[key] = obj[key];
      }
    }
    reload.attachHandlers();
  }
  setTranslation (obj) {
    this._translation.x = obj.left;
    this._translation.y = obj.top;
  }
  getTranslation () {
    return this._translation;
  }
  getDimension () {
    let config = this.config;
    return {
      width: config.width,
      height: config.height
    };
  }
  draw () {
    let reload = this,
      config = reload.config,
      drawLoader = config.drawLoader,
      chartConfig = reload.getFromEnv('chartConfig'),
      buttonStyle = config.buttonStyle,
      textStyle = config.textStyle,
      translation = reload.getTranslation(),
      x = translation.x,
      y = translation.y,
      width = config.width,
      height = config.height;
      // iconPath;
    // iconPath = reloadIcon(x + width / 2, y + height / 2, 0.3 * height);
    reload.addGraphicalElement({
      el: 'rect',
      attr: {
        x: x - width / 2,
        y: y - height / 2,
        width,
        height,
        fill: buttonStyle.fill || '#686980',
        r: 2,
        stroke: buttonStyle.stroke || 'none',
        'fill-opacity': pluckNumber(buttonStyle['fill-opacity'], 1),
        'stroke-opacity': pluckNumber(buttonStyle['stroke-opacity'], 1),
        'stroke-width': pluckNumber(buttonStyle['stroke-width'], 1),
        cursor: 'pointer'
      },
      container: {
        id: 'meso',
        label: 'meso',
        isParent: true
      },
      id: 'reload_rect',
      label: 'reload_rect'
    });
    reload.addGraphicalElement({
      el: 'text',
      attr: {
        x: x,
        y: y,
        text: 'Reload',
        'font-family': textStyle['font-family'] || 'Source Sans Pro',
        'font-weight': textStyle['font-weight'] || 'normal',
        'font-style': textStyle['font-style'] || 'normal',
        'font-size': '11px',
        'fill': textStyle.fill || COLOR_WHITE,
        'fill-opacity': pluckNumber(textStyle['fill-opacity'], 1),
        'text-anchor': 'middle',
        'vertical-align': 'middle',
        'cursor': 'pointer'
      },
      container: {
        id: 'meso',
        label: 'meso',
        isParent: true
      },
      id: 'reload_icon',
      label: 'reload_icon'
    });
    if (drawLoader) {
      reload.addGraphicalElement({
        el: 'rect',
        attr: {
          x: chartConfig.allCanvasLeft,
          y: chartConfig.allCanvasTop,
          width: chartConfig.allCanvasWidth,
          height: chartConfig.allCanvasHeight,
          fill: config.showLoader ? COLOR_BLACK : 'none',
          opacity: config.showLoader ? 0.4 : 0
        },
        container: {
          id: 'meso',
          label: 'meso',
          isParent: true
        },
        id: 'reload_background',
        label: 'reload_background'
      }, true);
      reload.addGraphicalElement({
        el: 'text',
        attr: {
          x: chartConfig.allCanvasLeft + chartConfig.allCanvasWidth / 2,
          y: chartConfig.allCanvasTop + chartConfig.allCanvasHeight / 2,
          text: 'Loading Data Please Wait...',
          'vertical-align': MIDDLE,
          'text-anchor': MIDDLE,
          'font-family': 'Source Sans Pro',
          'font-weight': '600',
          'fill': COLOR_WHITE,
          'font-size': '16px',
          'stroke': 'none',
          'opacity': config.showLoader ? 1 : 0
        },
        container: {
          id: 'meso',
          label: 'meso',
          isParent: true
        },
        id: 'reload_text',
        label: 'reload_text'
      }, true);
    }
  }
}

export default Reload;
